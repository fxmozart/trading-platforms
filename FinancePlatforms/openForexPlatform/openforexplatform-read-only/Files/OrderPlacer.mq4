//+------------------------------------------------------------------+
//|                                                  OrderPlacer.mq4 |
//|                      Copyright � 2010, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2010, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net"

extern double volume = 1;
extern int placeInterval = 5;
extern int closeInterval = 9;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   if (Minute() % placeInterval == 0)
   {
       int ticket=OrderSend(Symbol(),OP_BUY,volume,Ask,3,Ask-25*Point,Ask+25*Point,"My order #2",16384,0,Green);
   }
   else if (Minute() % closeInterval == 0)
   {
      HandleCloseAllOrdersRequest();
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+
double GetCurrentOrderClosingPrice()
{
   RefreshRates();

   int orderType = OrderType();
   if (orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP)
   {
      return (MarketInfo(OrderSymbol(), MODE_BID));
      //return (Bid);
   }
   else
   {
      return (MarketInfo(OrderSymbol(), MODE_ASK));
      //return (Ask);
   }
}


void HandleCloseAllOrdersRequest()
{
   int closedOrdersCount = 0;
   
   int totalOrders = OrdersTotal();
   
   for(int j=0; j<5 && totalOrders > 0; j++)
   {// Retry a few times to close all orders.
      totalOrders = OrdersTotal();  
      for(int pos=0; pos < totalOrders; pos++)
      {
        if(OrderSelect(pos, SELECT_BY_POS)) 
        {
          if (OrderSymbol() != Symbol())
          {// This order belongs to different integration - do not touch it.
            continue;
          }
          
          
          double orderPrice = GetCurrentOrderClosingPrice();

          if ( OrderClose(OrderTicket(), OrderLots(), orderPrice, 12, Yellow) == false)
          {
             int errorCode = GetLastError();
             //ReportError("OrderClose has failed with error #" + errorCode + ":" + PrintErrorDescription(errorCode), operationID);
          }
          else
          {
            closedOrdersCount++;
          }
        }
        
        Sleep(250);
      }
   }

}

