// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Diagnostics;
using Matrix.Common.Diagnostics;

namespace Matrix.Framework.ServiceApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application in service mode.
        /// If it is run as a win application or console, will automatically switch
        /// to corresponding mode, so it can assist debugging.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                if (Environment.UserInteractive)
                {// Start in test mode, with console control and output.
                    Console.WriteLine("Starting service...");

                    ServiceMain serviceBase = new ServiceMain();
                    serviceBase.ConsoleStart(args);

                    Console.WriteLine("Service running in console mode, press any key to stop.");
                    Console.ReadKey();

                    serviceBase.ConsoleStop();
                }
                else
                {// Start service is service mode.
                    ServiceBase.Run(new ServiceBase[] { new ServiceMain() });
                }
            }
            catch (Exception ex)
            {// Main thread exception occured.
                Console.WriteLine("*Exception* " + ex.Message);
                SystemMonitor.Error("Core thread error", ex);
                Console.ReadKey();
            }

            Console.WriteLine("Service stopped.");
        }
    }
}
