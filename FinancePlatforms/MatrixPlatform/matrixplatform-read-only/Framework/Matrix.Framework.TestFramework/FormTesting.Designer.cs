﻿using Matrix.Common.Diagnostics.FrontEnd.TracerControls;
using Matrix.Common.Diagnostics.TracerCore;

namespace Matrix.Framework.TestFramework
{
    partial class FormTesting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTesting));
            Tracer tracer1 = new Tracer();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.timerUI = new System.Windows.Forms.Timer(this.components);
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.toolStripComboBoxSpeedTests = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonRun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonProperties = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxCount = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonAbort = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10K = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton100K = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1Mil = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10Mil = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton100Mil = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tracerControl1 = new TracerControl();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxMultiThread = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonMultiThreadRun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMultiThreadAbort = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMultiThreadProperties = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxMultiThreads = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxMultiThreadSec = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                        this.columnHeader1});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView1.Location = new System.Drawing.Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(778, 266);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 600;
            // 
            // timerUI
            // 
            this.timerUI.Enabled = true;
            this.timerUI.Interval = 500;
            this.timerUI.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                        this.columnHeader2});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Location = new System.Drawing.Point(3, 279);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(778, 186);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 600;
            // 
            // toolStripComboBoxSpeedTests
            // 
            this.toolStripComboBoxSpeedTests.DropDownHeight = 400;
            this.toolStripComboBoxSpeedTests.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxSpeedTests.IntegralHeight = false;
            this.toolStripComboBoxSpeedTests.Name = "toolStripComboBoxSpeedTests";
            this.toolStripComboBoxSpeedTests.Size = new System.Drawing.Size(181, 25);
            // 
            // toolStripButtonRun
            // 
            this.toolStripButtonRun.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRun.Image")));
            this.toolStripButtonRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRun.Name = "toolStripButtonRun";
            this.toolStripButtonRun.Size = new System.Drawing.Size(46, 22);
            this.toolStripButtonRun.Text = "Run";
            this.toolStripButtonRun.Click += new System.EventHandler(this.toolStripButtonStart_Click);
            // 
            // toolStripButtonProperties
            // 
            this.toolStripButtonProperties.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonProperties.Image")));
            this.toolStripButtonProperties.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProperties.Name = "toolStripButtonProperties";
            this.toolStripButtonProperties.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonProperties.Text = "Properties";
            this.toolStripButtonProperties.Click += new System.EventHandler(this.toolStripButtonProperties_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(36, 22);
            this.toolStripLabel1.Text = "Count";
            // 
            // toolStripTextBoxCount
            // 
            this.toolStripTextBoxCount.Name = "toolStripTextBoxCount";
            this.toolStripTextBoxCount.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBoxCount.Text = "100000";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                                                        this.toolStripLabel2,
                                                                                        this.toolStripComboBoxSpeedTests,
                                                                                        this.toolStripButtonRun,
                                                                                        this.toolStripButtonAbort,
                                                                                        this.toolStripButtonProperties,
                                                                                        this.toolStripSeparator1,
                                                                                        this.toolStripLabel1,
                                                                                        this.toolStripTextBoxCount,
                                                                                        this.toolStripButton10K,
                                                                                        this.toolStripButton100K,
                                                                                        this.toolStripButton1Mil,
                                                                                        this.toolStripButton10Mil,
                                                                                        this.toolStripButton100Mil});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(792, 25);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel2.Text = "Speed";
            // 
            // toolStripButtonAbort
            // 
            this.toolStripButtonAbort.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAbort.Image")));
            this.toolStripButtonAbort.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbort.Name = "toolStripButtonAbort";
            this.toolStripButtonAbort.Size = new System.Drawing.Size(54, 22);
            this.toolStripButtonAbort.Text = "Abort";
            this.toolStripButtonAbort.Click += new System.EventHandler(this.toolStripButtonAbort_Click);
            // 
            // toolStripButton10K
            // 
            this.toolStripButton10K.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton10K.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10K.Image")));
            this.toolStripButton10K.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10K.Name = "toolStripButton10K";
            this.toolStripButton10K.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton10K.Text = "10K";
            this.toolStripButton10K.Click += new System.EventHandler(this.toolStripButton10K_Click);
            // 
            // toolStripButton100K
            // 
            this.toolStripButton100K.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton100K.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton100K.Image")));
            this.toolStripButton100K.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton100K.Name = "toolStripButton100K";
            this.toolStripButton100K.Size = new System.Drawing.Size(35, 22);
            this.toolStripButton100K.Text = "100K";
            this.toolStripButton100K.Click += new System.EventHandler(this.toolStripButton100K_Click);
            // 
            // toolStripButton1Mil
            // 
            this.toolStripButton1Mil.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1Mil.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1Mil.Image")));
            this.toolStripButton1Mil.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1Mil.Name = "toolStripButton1Mil";
            this.toolStripButton1Mil.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton1Mil.Text = "1Mil";
            this.toolStripButton1Mil.Click += new System.EventHandler(this.toolStripButton1Mil_Click);
            // 
            // toolStripButton10Mil
            // 
            this.toolStripButton10Mil.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton10Mil.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10Mil.Image")));
            this.toolStripButton10Mil.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10Mil.Name = "toolStripButton10Mil";
            this.toolStripButton10Mil.Size = new System.Drawing.Size(35, 22);
            this.toolStripButton10Mil.Text = "10Mil";
            this.toolStripButton10Mil.Click += new System.EventHandler(this.toolStripButton10Mil_Click);
            // 
            // toolStripButton100Mil
            // 
            this.toolStripButton100Mil.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton100Mil.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton100Mil.Image")));
            this.toolStripButton100Mil.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton100Mil.Name = "toolStripButton100Mil";
            this.toolStripButton100Mil.Size = new System.Drawing.Size(41, 22);
            this.toolStripButton100Mil.Text = "100Mil";
            this.toolStripButton100Mil.Click += new System.EventHandler(this.toolStripButton100Mil_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 50);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(792, 494);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listView2);
            this.tabPage1.Controls.Add(this.splitter1);
            this.tabPage1.Controls.Add(this.listView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(784, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Status";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(3, 269);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(778, 10);
            this.splitter1.TabIndex = 10;
            this.splitter1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tracerControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(784, 468);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Diag";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tracerControl1
            // 
            this.tracerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracerControl1.Location = new System.Drawing.Point(3, 3);
            this.tracerControl1.MarkingMatch = "";
            this.tracerControl1.Name = "tracerControl1";
            this.tracerControl1.ShowMethodFilter = false;
            this.tracerControl1.Size = new System.Drawing.Size(778, 462);
            this.tracerControl1.TabIndex = 0;
            tracer1.Enabled = true;
            tracer1.TimeDisplayFormat = Tracer.TimeDisplayFormatEnum.ApplicationTicks;
            this.tracerControl1.Tracer = tracer1;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                                                        this.toolStripLabel3,
                                                                                        this.toolStripComboBoxMultiThread,
                                                                                        this.toolStripButtonMultiThreadRun,
                                                                                        this.toolStripButtonMultiThreadAbort,
                                                                                        this.toolStripButtonMultiThreadProperties,
                                                                                        this.toolStripSeparator2,
                                                                                        this.toolStripLabel4,
                                                                                        this.toolStripTextBoxMultiThreads,
                                                                                        this.toolStripLabel5,
                                                                                        this.toolStripTextBoxMultiThreadSec,
                                                                                        this.toolStripLabel6});
            this.toolStrip2.Location = new System.Drawing.Point(0, 25);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(792, 25);
            this.toolStrip2.TabIndex = 11;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(77, 22);
            this.toolStripLabel3.Text = "MultiThreading";
            // 
            // toolStripComboBoxMultiThread
            // 
            this.toolStripComboBoxMultiThread.DropDownHeight = 400;
            this.toolStripComboBoxMultiThread.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxMultiThread.IntegralHeight = false;
            this.toolStripComboBoxMultiThread.Name = "toolStripComboBoxMultiThread";
            this.toolStripComboBoxMultiThread.Size = new System.Drawing.Size(181, 25);
            // 
            // toolStripButtonMultiThreadRun
            // 
            this.toolStripButtonMultiThreadRun.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMultiThreadRun.Image")));
            this.toolStripButtonMultiThreadRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMultiThreadRun.Name = "toolStripButtonMultiThreadRun";
            this.toolStripButtonMultiThreadRun.Size = new System.Drawing.Size(46, 22);
            this.toolStripButtonMultiThreadRun.Text = "Run";
            this.toolStripButtonMultiThreadRun.Click += new System.EventHandler(this.toolStripButtonMultiThreadRun_Click);
            // 
            // toolStripButtonMultiThreadAbort
            // 
            this.toolStripButtonMultiThreadAbort.Enabled = false;
            this.toolStripButtonMultiThreadAbort.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMultiThreadAbort.Image")));
            this.toolStripButtonMultiThreadAbort.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMultiThreadAbort.Name = "toolStripButtonMultiThreadAbort";
            this.toolStripButtonMultiThreadAbort.Size = new System.Drawing.Size(54, 22);
            this.toolStripButtonMultiThreadAbort.Text = "Abort";
            this.toolStripButtonMultiThreadAbort.Click += new System.EventHandler(this.toolStripButtonMultiThreadAbort_Click);
            // 
            // toolStripButtonMultiThreadProperties
            // 
            this.toolStripButtonMultiThreadProperties.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMultiThreadProperties.Image")));
            this.toolStripButtonMultiThreadProperties.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMultiThreadProperties.Name = "toolStripButtonMultiThreadProperties";
            this.toolStripButtonMultiThreadProperties.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonMultiThreadProperties.Text = "Properties";
            this.toolStripButtonMultiThreadProperties.Click += new System.EventHandler(this.toolStripButtonMultiThreadProperties_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(46, 22);
            this.toolStripLabel4.Text = "Threads";
            // 
            // toolStripTextBoxMultiThreads
            // 
            this.toolStripTextBoxMultiThreads.Name = "toolStripTextBoxMultiThreads";
            this.toolStripTextBoxMultiThreads.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBoxMultiThreads.Text = "5";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(29, 22);
            this.toolStripLabel5.Text = "Time";
            // 
            // toolStripTextBoxMultiThreadSec
            // 
            this.toolStripTextBoxMultiThreadSec.Name = "toolStripTextBoxMultiThreadSec";
            this.toolStripTextBoxMultiThreadSec.Size = new System.Drawing.Size(40, 25);
            this.toolStripTextBoxMultiThreadSec.Text = "12";
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(23, 22);
            this.toolStripLabel6.Text = "sec";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                                                          this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 544);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(792, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(109, 17);
            this.toolStripStatusLabel.Text = "toolStripStatusLabel1";
            // 
            // FormTesting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "FormTesting";
            this.Text = "Testing Environment";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Timer timerUI;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxSpeedTests;
        private System.Windows.Forms.ToolStripButton toolStripButtonRun;
        private System.Windows.Forms.ToolStripButton toolStripButtonProperties;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxCount;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton100K;
        private System.Windows.Forms.ToolStripButton toolStripButton1Mil;
        private System.Windows.Forms.ToolStripButton toolStripButton10Mil;
        private System.Windows.Forms.ToolStripButton toolStripButton10K;
        private System.Windows.Forms.ToolStripButton toolStripButtonAbort;
        private System.Windows.Forms.ToolStripButton toolStripButton100Mil;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Splitter splitter1;
        private TracerControl tracerControl1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxMultiThread;
        private System.Windows.Forms.ToolStripButton toolStripButtonMultiThreadRun;
        private System.Windows.Forms.ToolStripButton toolStripButtonMultiThreadAbort;
        private System.Windows.Forms.ToolStripButton toolStripButtonMultiThreadProperties;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxMultiThreads;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxMultiThreadSec;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
    }
}