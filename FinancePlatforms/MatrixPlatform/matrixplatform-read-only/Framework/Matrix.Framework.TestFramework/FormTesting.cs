// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Matrix.Common.Diagnostics;
using Matrix.Common.FrontEnd;
using Matrix.Common.FrontEnd.UI.Controls;

namespace Matrix.Framework.TestFramework
{
    public partial class FormTesting : Form
    {
        int? TestCount
        {
            get
            {
                int result;
                if (int.TryParse(toolStripTextBoxCount.Text, out result))
                {
                    return result;
                }

                return null;
            }
        }

        public SpeedTest SelectedSpeedTest
        {
            get
            {
                if (toolStripComboBoxSpeedTests.SelectedIndex > -1)
                {
                    return _testManager.SpeedTests[toolStripComboBoxSpeedTests.SelectedIndex];
                }

                return null;
            }
        }

        public MultiThreadTest SelectedMultiThreadTest
        {
            get
            {
                if (toolStripComboBoxMultiThread.SelectedIndex > -1)
                {
                    return _testManager.MutliThreadTests[toolStripComboBoxMultiThread.SelectedIndex];
                }

                return null;
            }
        }

        bool IsSpeedTestRunning
        {
            get
            {
                return !toolStripButtonRun.Enabled;
            }
        }

        bool IsMultiThreadTestRunning
        {
            get
            {
                return !toolStripButtonMultiThreadRun.Enabled;
            }
        }

        TestManager _testManager = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public FormTesting()
        {
            InitializeComponent();

            _testManager = new TestManager(this);
            _testManager.TestFinishedEvent += new TestManager.TestUpdateDelegate(_testManager_TestFinishedEvent);

            this.Text += " [" + Assembly.GetEntryAssembly().GetName().Name + "]";
        }

        void _testManager_TestFinishedEvent(TestManager manager)
        {
            WinFormsHelper.BeginManagedInvoke(this, delegate() {
                                                                   toolStripButtonMultiThreadRun.Enabled = true;
                                                                   toolStripButtonRun.Enabled = true;
            });
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            foreach (SpeedTest test in _testManager.SpeedTests)
            {
                toolStripComboBoxSpeedTests.Items.Add(test.GetType().Name);
            }

            foreach (MultiThreadTest test in _testManager.MutliThreadTests)
            {
                toolStripComboBoxMultiThread.Items.Add(test.GetType().Name);
            }

            tracerControl1.Tracer = SystemMonitor.Tracer;

            if (toolStripComboBoxSpeedTests.Items.Count > 0)
            {
                toolStripComboBoxSpeedTests.SelectedIndex = 0;
            }

            if (toolStripComboBoxMultiThread.Items.Count > 0)
            {
                toolStripComboBoxMultiThread.SelectedIndex = 0;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            toolStripButtonAbort_Click(sender, EventArgs.Empty);
            toolStripButtonMultiThreadAbort_Click(sender, EventArgs.Empty);
        }

        public void ReportMain(string message)
        {
            WinFormsHelper.BeginManagedInvoke(this, delegate() { listView1.Items.Insert(0, message); });
        }

        public void ReportSecondary(string message)
        {
            WinFormsHelper.BeginManagedInvoke(this, delegate() { listView2.Items.Insert(0, message); });
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string status = toolStripStatusLabel.Text;

            if (IsSpeedTestRunning && SelectedSpeedTest != null)
            {// Speed test.
                if (SelectedSpeedTest.Executed.ToString() != status)
                {
                    status = SelectedSpeedTest.Executed.ToString("### ### ### ###.");
                }

                SelectedSpeedTest.Update(this);
            }
            else if (IsMultiThreadTestRunning && SelectedMultiThreadTest != null)
            {// Multi thread test.
                if (SelectedMultiThreadTest.StepsPerformed.ToString() != status)
                {
                    status = SelectedMultiThreadTest.StepsPerformed.ToString("### ### ### ###.");
                }

                SelectedMultiThreadTest.Update(this);
            }
            else
            {
                status = string.Empty;
            }

            if (status != toolStripStatusLabel.Text)
            {
                toolStripStatusLabel.Text = status;
            }
        }



        private void toolStripButtonStart_Click(object sender, EventArgs e)
        {
            if (SelectedSpeedTest != null)
            {
                toolStripButtonRun.Enabled = false;
                _testManager.RunSpeedTest(SelectedSpeedTest, TestCount.Value);
            }
        }

        private void toolStripButton10K_Click(object sender, EventArgs e)
        {
            toolStripTextBoxCount.Text = "10000";
        }

        private void toolStripButton100K_Click(object sender, EventArgs e)
        {
            toolStripTextBoxCount.Text = "100000";
        }

        private void toolStripButton1Mil_Click(object sender, EventArgs e)
        {
            toolStripTextBoxCount.Text = "1000000";
        }

        private void toolStripButton10Mil_Click(object sender, EventArgs e)
        {
            toolStripTextBoxCount.Text = "10000000";
        }

        private void toolStripButton100Mil_Click(object sender, EventArgs e)
        {
            toolStripTextBoxCount.Text = "100000000";
        }

        private void toolStripButtonProperties_Click(object sender, EventArgs e)
        {
            if (SelectedSpeedTest != null)
            {
                PropertiesForm form = new PropertiesForm("Test Properties", SelectedSpeedTest);
                form.ShowDialog();
            }
        }

        private void toolStripButtonAbort_Click(object sender, EventArgs e)
        {
            _testManager.Stop();
            toolStripButtonRun.Enabled = true;
        }

        private void toolStripButtonMultiThreadAbort_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButtonMultiThreadRun_Click(object sender, EventArgs e)
        {
            if (SelectedSpeedTest != null)
            {
                TimeSpan timespan = TimeSpan.FromSeconds(int.Parse(toolStripTextBoxMultiThreadSec.Text));
                int threadsCount = int.Parse(toolStripTextBoxMultiThreads.Text);
                
                toolStripButtonMultiThreadRun.Enabled = false;

                _testManager.RunMultiThreadTest(SelectedMultiThreadTest, timespan, threadsCount);
            }
        }

        private void toolStripButtonMultiThreadProperties_Click(object sender, EventArgs e)
        {
            if (SelectedMultiThreadTest != null)
            {
                PropertiesForm form = new PropertiesForm("Test Properties", SelectedMultiThreadTest);
                form.ShowDialog();
            }
        }

    }
}
