// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.MessageBus.Core;
using System.Collections.ObjectModel;
using System.Reflection;

namespace Matrix.Framework.SuperPool.Subscription
{
    /// <summary>
    /// Instruct the way an event of a proxy must be interpreted.
    /// A request for perform subscription for given event.
    /// </summary>
    [Serializable]
    public class EventSubscriptionRequest
    {
        public ClientId SenderId { get; set; }

        public string ExtendedEventName { get; set; }

        /// <summary>
        /// Add or remove operation.
        /// </summary>
        public bool IsAdd { get; set; } 
        
        /// <summary>
        /// 
        /// </summary>
        //public MethodInfo EventAddMethodInfo { get; set; }
        
        /// <summary>
        /// The method that handles the event on the receiver object.
        /// </summary>
        public MethodInfo DelegateInstanceMethodInfo { get; set; }

        /// <summary>
        /// Assigned only when a remote synchronization is done.
        /// </summary>
        public int? SpecificCountOptional { get; set; }

        List<ClientId> _eventsSources = null;
        /// <summary>
        /// If the value is null, we consider it to be a subscription for all sources in general.
        /// </summary>
        public ReadOnlyCollection<ClientId> EventsSources
        {
            get
            {
                List<ClientId> eventsSources = _eventsSources;
                if (eventsSources == null)
                {
                    return null;
                }

                return eventsSources.AsReadOnly(); 
            }
        }

        /// <summary>
        /// Constructor, instruct registration for ANY source that raises the event.
        /// </summary>
        public EventSubscriptionRequest()
        {
        }

        /// <summary>
        /// Constructor, instruct registration for a specific source.
        /// </summary>
        public EventSubscriptionRequest(ClientId eventSourceId)
        {
            _eventsSources = new List<ClientId>();
            _eventsSources.Add(eventSourceId);
        }

        /// <summary>
        /// Constructor, instruct registration for a specific set of sources.
        /// </summary>
        public EventSubscriptionRequest(IEnumerable<ClientId> eventSourceId)
        {
            _eventsSources = new List<ClientId>(eventSourceId);
        }

        public override string ToString()
        {
            string sourcesMessage = _eventsSources != null ? _eventsSources.Count.ToString() : string.Empty;
            return base.ToString() + ", senderId[" + SenderId.ToString() + "], sources count [" +
                   sourcesMessage + "]";
        }
    }
}
