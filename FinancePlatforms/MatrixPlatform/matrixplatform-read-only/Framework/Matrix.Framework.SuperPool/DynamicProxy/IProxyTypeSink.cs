// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Matrix.Framework.SuperPool.DynamicProxy
{
    /// <summary>
    /// Interface serves as a sink for proxied calls.
    /// 
    /// *IMPORTANT* renaming any of these methods *must* be matched in the ProxyTypeBuilder.
    /// </summary>
    public interface IProxyTypeSink
    {
        void ReceiveMethodCall(int methodId, object[] parameters);
        object ReceiveMethodCallAndReturn(int methodId, Type returnType, object[] parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodId"></param>
        /// <param name="returnType">Usefull to return a default value when failed to establish a critical component.</param>
        /// <returns></returns>
        object ReceivePropertyGet(int methodId, Type returnType);
        void ReceivePropertySet(int methodId, object value);

        void ReceiveEventSubscribed(int methodId, Delegate subscribedDelegate);
        void ReceiveEventUnSubscribed(int methodId, Delegate subscribedDelegate);
    }
}
