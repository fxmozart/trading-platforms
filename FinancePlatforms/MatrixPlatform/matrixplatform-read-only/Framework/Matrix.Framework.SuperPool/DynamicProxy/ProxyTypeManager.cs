// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Matrix.Common.Core.Collections;

namespace Matrix.Framework.SuperPool.DynamicProxy
{
    /// <summary>
    /// Class manages the operation of dynamically built proxy type instances and types.
    /// Operation is separated as follows:
    /// - proxy class, implemetns a proxy instance of a client instance.
    /// </summary>
    public class ProxyTypeManager : IDisposable
    {
        private ProxyTypeBuilder _builder = new ProxyTypeBuilder("ProxyTypeManager.Proxies");

        Dictionary<Type, object> _proxyObjects = new Dictionary<Type, object>();

        /// <summary>
        /// The builder we used to create the proxy types.
        /// </summary>
        public ProxyTypeBuilder Builder
        {
            get { return _builder; }
        }

        /// <summary>
        /// All reference types are automatically defaulted to null, so this only handles the value types.
        /// </summary>
        static HotSwapDictionary<Type, object> _defaultReturnValues = new HotSwapDictionary<Type, object>();

        /// <summary>
        /// Static constructor.
        /// </summary>
        static ProxyTypeManager()
        {
            _defaultReturnValues.AddRange(
                new Type[] { typeof(Int16), typeof(Int32), typeof(Int64), typeof(uint), typeof(double), typeof(string) },
                new object[] { 0, 0, 0, 0, 0, string.Empty });
        }

        IProxyTypeSink _sink;

        public IProxyTypeSink Sink
        {
            get { return _sink; }
            set { _sink = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ProxyTypeManager()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
        }

        /// <summary>
        /// Helper.
        /// </summary>
        protected void Abort(string message)
        {
            throw new Exception(message);
        }

        #region Public 

        ///// <summary>
        ///// Obtain a proxy method delegate for a dynamic method.
        ///// Obtain only once, when we attach it to the event.
        ///// </summary>
        //public GeneratedDynamicMethodInfo ObtainDynamicMethodProxyDelegate(Type delegateType)
        //{
        //    // Establish delegate parameters.
        //    MethodInfo delegateMethodInfo = delegateType.GetMethod("Invoke");

        //    // Generate a new dynamic method; we can *NOT REUSE THEM*, since they work on handling events
        //    // and each event handler must be traceable to the instance that we subscribed it for.
        //    return _builder.GenerateDynamicProxyMethod(ProxyTypeBuilder.GetMethodParametersTypes(delegateMethodInfo), 
        //        delegateMethodInfo.ReturnType);

        //    //lock (_dynamicMethods)
        //    //{
        //    //    if (_dynamicMethods.ContainsKey(delegateMethodInfo))
        //    //    {
        //    //        return _dynamicMethods[delegateMethodInfo];
        //    //    }

        //    //    Delegate delegateInstance = dynamicMethod.Method.CreateDelegate(delegateType, _dynamicMethodSink);
                
        //    //    //object result = delegateInstance2.DynamicInvoke(0);
        //    //    //delegateStatic.DynamicInvoke(_dynamicMethodSink, 0);
        //    //    //delegateInstance.DynamicInvoke(null, EventArgs.Empty);

        //    //    //return delegateInstance;

        //    //    //// Lock it, since this way we shall only create what we actually use.
        //    //    //object proxy = Activator.CreateInstance(dynamicMethod.GeneratedType, _sink);

        //    //    //Delegate delegateInstance = Delegate.CreateDelegate(delegateType, proxy,
        //    //    //    dynamicMethod.GeneratedType.GetMethod(delegateMethodInfo.Name));

        //    //    //_dynamicMethods[delegateMethodInfo] = delegateInstance;

        //    //    //return delegateInstance;
        //    //    return delegateInstance;
        //    //}

        //}

        /// <summary>
        /// Invoked on each call.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public object ObtainInterfaceProxy(Type type)
        {
            lock (_proxyObjects)
            {
                if (_proxyObjects.ContainsKey(type))
                {
                    return _proxyObjects[type];
                }
            }

            if (type.IsInterface == false)
            {
                Abort("Type [" + type.Name + "] not an interface.");
            }

            Type proxyType = _builder.GenerateInterfaceProxyImplementation(type);
            if (proxyType == null)
            {// Failed to create proxy type for this interface.
                return null;
            }

            ConstructorInfo info = proxyType.GetConstructor(new Type[] { typeof(IProxyTypeSink) });
            object proxyInstance = info.Invoke(new object[] { _sink });

            lock (_proxyObjects)
            {
                if (_proxyObjects.ContainsKey(type) == false)
                {
                    _proxyObjects.Add(type, proxyInstance);
                }
            }

            return proxyInstance;
        }

        #endregion

        /// <summary>
        /// Implementation, separate in order to allow calls from child classes.
        /// </summary>
        public static object GetTypeDefaultValue(Type returnType)
        {
            if (returnType == null || returnType.IsByRef
                || returnType.IsClass)
            {
                return null;
            }

            object result;
            if (_defaultReturnValues.TryGetValue(returnType, out result))
            {
                return result;
            }

            result = Activator.CreateInstance(returnType);
            _defaultReturnValues.Add(returnType, result);

            return result;
        }

    }
}
