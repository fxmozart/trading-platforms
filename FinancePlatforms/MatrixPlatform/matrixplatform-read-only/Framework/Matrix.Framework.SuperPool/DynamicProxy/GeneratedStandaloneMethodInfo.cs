// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Reflection.Emit;
using System.Reflection;

#if Matrix_Diagnostics
using Matrix.Common.Diagnostics;
#endif

namespace Matrix.Framework.SuperPool.DynamicProxy
{
    /// <summary>
    /// Stores info on a single standalone method, or a method generated as a part of proxy class.
    /// The method is what we need, and the class stores it
    /// and provides it with member instances.
    /// </summary>
    public class GeneratedMethodInfo
    {
        #region Common

        public int Id { get; protected set; }

        /// <summary>
        /// Assigned trough usage by a Super pool component.
        /// </summary>
        public string EventName { get; set; }

        #endregion

        #region Standalone specific

        public bool IsStandalone
        {
            get
            {
                return StandaloneDynamicMethod != null;
            }
        }

        /// <summary>
        /// The dynamic method we generated
        /// </summary>
        public DynamicMethod StandaloneDynamicMethod { get; protected set; }

        #endregion
        
        #region Part of proxy specific

        public bool IsProxyClassMethod
        {
            get
            {
                return ProxyOwnerType != null;
            }   
        }


        /// <summary>
        /// The owner proxy type (if available).
        /// </summary>
        public Type ProxyOwnerType { get; protected set; }

        /// <summary>
        /// The method info we implement upon.
        /// </summary>
        public MethodInfo ProxyMethodInfo { get; protected set; }

        #endregion

        /// <summary>
        /// Construct proxy class method.
        /// </summary>
        public GeneratedMethodInfo(int id, Type proxyType, MethodInfo methodInfo)
        {
            Id = id;
            ProxyMethodInfo = methodInfo;
            ProxyOwnerType = proxyType;
        }

        /// <summary>
        /// Construct standalone.
        /// </summary>
        public GeneratedMethodInfo(int id, DynamicMethod dynamicMethod, Type delegateType)
        {
            this.Id = id;

            //this.StandaloneDelegateType = delegateType;
            this.StandaloneDynamicMethod = dynamicMethod;
        }

        /// <summary>
        /// This operates on valid object in both a Standalone and Proxy method modes.
        /// </summary>
        public MethodInfo GetMethodInfo()
        {
            if (IsProxyClassMethod)
            {
                return ProxyMethodInfo;
            }
            else if (IsStandalone)
            {
                return StandaloneDynamicMethod;
            }

            return null;
        }

        /// <summary>
        /// Only works for ProxyTypes, since stand alone methods do not have it.
        /// </summary>
        /// <returns></returns>
        public Type GetBaseInterfaceType()
        {
            Type proxyType = ProxyOwnerType;
            Type[] interfaceTypes = proxyType.GetInterfaces();

            if (interfaceTypes.Length != 1)
            {
#if Matrix_Diagnostics
                SystemMonitor.Error(string.Format("Proxy class [{0}] does not provide clear interface specification.", proxyType.ToString()));
#endif
                return null;
            }

            return interfaceTypes[0];
        }

        public override string ToString()
        {
            MethodInfo methodInfo = GetMethodInfo();
            if (methodInfo == null)
            {// Instance not initialized properly yet.
                return base.ToString();
            }

            if (IsStandalone)
            {
                return "Standalone method [" + methodInfo.ToString() + "]";
            }
            else
            {
                Type baseInterfaceType = GetBaseInterfaceType();
                return "Generated method info for [" + baseInterfaceType.Name + "." + methodInfo.ToString() + "]";
            }
        }
    }
}
