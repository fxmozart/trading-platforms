// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.MessageBus.Core;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.SuperPool.DynamicProxy;
using Matrix.Framework.SuperPool.Subscription;

namespace Matrix.Framework.SuperPool.Call
{
    /// <summary>
    /// Stores information related to a single call to a super pool proxy.
    /// </summary>
    public class SuperPoolProxyCall
    {
        public enum ModeEnum : int
        {
            Default,
            DirectCall,
            CallFirst
        }

        public ModeEnum Mode { get; set; }

        public bool Processed { get; set; }

        public TimeSpan? RequestConfirmTimeout = null;

        public int? MethodId { get; set; }
        public Type ReturnType { get; set; }
        public object[] Parameters { get; set; }

        public GeneratedMethodInfo MethodInfo { get; set; }

        public List<ClientId> ReceiversIds { get; set; }

        public SuperPoolClient Sender { get; set; }

        public TimeSpan? Timeout { get; set; }

        public bool IsSynchronous
        {
            get { return Timeout.HasValue; }
        }

        public bool IsAsyncResultExpecting
        {
            get { return AsyncResultDelegate != null; }
        }

        internal EventSubscriptionRequest SubscriptionRequest { get; set; }

        internal CallOutcome Outcome { get; set; }

        public AsyncCallResultDelegate AsyncResultDelegate { get; set; }
        
        public object AsyncResultState { get; set; }

        /// <summary>
        /// Used when a call was made to any implementor, and results are collection in 
        /// async fashion, with this max. timeout allowed for a result.
        /// </summary>
        public TimeSpan? AsyncResultTimeout { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SuperPoolProxyCall()
        {
            Clear();
        }

        /// <summary>
        /// Clear instance.
        /// </summary>
        public void Clear()
        {
            Mode = ModeEnum.Default;

            Processed = false;
            RequestConfirmTimeout = null;

            SubscriptionRequest = null;

            MethodId = null;
            ReturnType = null;
            Parameters = null;
            MethodInfo = null;
            ReceiversIds = null;
            Sender = null;
            Timeout = null;
            
            AsyncResultDelegate = null;
            AsyncResultState = null;
            AsyncResultTimeout = null;
        }

        public override string ToString()
        {
            if (IsSynchronous)
            {
                return string.Format(base.ToString() + ", from [{0}, {1}].", Sender.Id.Name, MethodInfo != null ? MethodInfo.ToString() : string.Empty);
            }
            else
            {
                return string.Format(base.ToString() + ", from [{0}, {1}].", Sender.Id.Name, MethodInfo != null ? MethodInfo.ToString() : string.Empty);
            }
        }

    }
}
