// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Matrix.Framework.SuperPool.Call
{
    /// <summary>
    /// Present per thread info for the current super pool call on a per thread base.
    /// Operation of this class is configurable, and can be stopped alltogether to speed
    /// up execution (controlled trough MessageSuperPoolInvocation.ProvideCallContextData).
    /// </summary>
    public static class SuperPoolCallContext
    {
        static Dictionary<int, SuperPoolCall> _calls = new Dictionary<int, SuperPoolCall>();

        /// <summary>
        /// Obtain (or set) the current call for the current thread.
        /// </summary>
        public static SuperPoolCall CurrentCall
        {
            get
            {
                SuperPoolCall result;
                lock (_calls)
                {
                    if (_calls.TryGetValue(Thread.CurrentThread.ManagedThreadId, out result))
                    {
                        return result;
                    }
                }

                return null;
            }

            internal set 
            {
                lock (_calls)
                {
                    _calls[Thread.CurrentThread.ManagedThreadId] = value;
                }
            }
        }
    }
}
