// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Framework.DataStorage.DataSeries
{
    /// <summary>
    /// Contains unified series information, for a given series (file).
    /// </summary>
    [Serializable]
    public class DataSeriesInfo
    {
        /// <summary>
        /// The Guid of this series.
        /// </summary>
        public Guid Guid { get; protected set; }

        /// <summary>
        /// Name of this series.
        /// </summary>
        public string Name { get; protected set; }

        public string ItemTypeName { get; protected set; }

        public string FilePath { get; set; }

        /// <summary>
        /// Available if established.
        /// </summary>
        public int? ItemsCount { get; protected set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public DataSeriesInfo(string name, string itemTypeName)
        {
            Name = name;
            Guid = Guid.NewGuid();
            ItemTypeName = itemTypeName;
        }
    }
}
