// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.IO;
//using System.Collections.ObjectModel;

//namespace Framework.DataStorage.DataSeries
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class DataItemSeriesManager
//    {
//        const string DefaultSeriesFileExtension = "srsx";

//        List<DataSeriesInfo> _series = new List<DataSeriesInfo>();

//        /// <summary>
//        /// 
//        /// </summary>
//        public ReadOnlyCollection<DataSeriesInfo> SeriesUnsafe
//        {
//            get { return _series.AsReadOnly(); }
//        }

//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public DataItemSeriesManager(string folder)
//        {
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public List<DataSeriesInfo> LoadSeriesFromFolder(string folderName)
//        {
//            List<DataSeriesInfo> result = new List<DataSeriesInfo>();
//            string[] files = Directory.GetFiles(folderName, "*." + DefaultSeriesFileExtension);

//            foreach (string file in files)
//            {
//                DataSeriesInfo serie = LoadSeriesFromFile(file);
//                if (serie != null)
//                {
//                    result.Add(serie);
//                }
//            }

//            return result;
//        }

//        public IItemSerializer GetSeriesSerializer(DataSeriesInfo info)
//        {
//            throw new NotImplementedException("Ex");
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public DataSeriesInfo LoadSeriesFromFile(string fileName)
//        {
//            throw new NotImplementedException("Ex");
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public DataSeriesInfo CreateSeries(string fileName)
//        {
//            throw new NotImplementedException("Ex");

//            if (File.Exists(fileName))
//            {
//                return null;
//            }
//        }

//        //public DataSeriesWriter GetSeriesReaderWriter(DataSeriesInfo info)
//        //{
//        //    return null;
//        //}

//        //public DataSeriesWriter CreateGenerator()
//        //{
//        //    return null;
//        //}

//    }
//}
