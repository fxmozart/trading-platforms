// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Framework.DataStorage.Database
{
    /// <summary>
    /// Indicate with this attribute that persistance is desired on any child class of IDBPersistent
    /// - if applied on class indicates the default persistency for classes properties.
    /// - if applied to property indicates specific instructions how to map property to DB
    /// </summary>
    public class DBPersistenceAttribute : Attribute
    {
        /// <summary>
        /// The type of persistance to apply.
        /// </summary>
        public enum PersistenceTypeEnum
        {
            None,
            Default,
            Binary
        }

        /// <summary>
        /// The mode of the persistence.
        /// </summary>
        public enum PersistenceModeEnum
        {
            Default, // Both read and write access.
            ReadOnly, // Read only access (get param).
        }

        PersistenceTypeEnum _persistenceType = PersistenceTypeEnum.Default;
        /// <summary>
        /// Type of the persistence.
        /// </summary>
        public PersistenceTypeEnum PersistenceType
        {
            get { return _persistenceType; }
        }

        PersistenceModeEnum _persistenceMode = PersistenceModeEnum.Default;
        /// <summary>
        /// Mode of the persistence.
        /// </summary>
        public PersistenceModeEnum PersistenceMode
        {
            get { return _persistenceMode; }
        }

        /// <summary>
        /// Default persistance constructor.
        /// </summary>
        public DBPersistenceAttribute(bool persist)
        {
            if (persist == false)
            {
                _persistenceType = PersistenceTypeEnum.None;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public DBPersistenceAttribute(PersistenceTypeEnum persistType)
        {
            _persistenceType = persistType;
        }

        /// <summary>
        /// 
        /// </summary>
        public DBPersistenceAttribute(PersistenceModeEnum persistenceMode)
        {
            _persistenceMode = persistenceMode;
        }

        /// <summary>
        /// 
        /// </summary>
        public DBPersistenceAttribute(PersistenceTypeEnum persistType, PersistenceModeEnum persistenceMode)
        {
            _persistenceType = persistType;
            _persistenceMode = persistenceMode;
        }

    }
}
