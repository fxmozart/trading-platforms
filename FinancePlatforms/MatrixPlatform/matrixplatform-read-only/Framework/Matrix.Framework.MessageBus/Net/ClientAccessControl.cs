// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.MessageBus.Net.Messages;

namespace Matrix.Framework.MessageBus.Net
{
    /// <summary>
    /// Client side access control structure, stores information on client login.
    /// </summary>
    [Serializable]
    public sealed class ClientAccessControl
    {
        public string Username { get; set; }
        public string Password { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ClientAccessControl()
        {
        }

        /// <summary>
        /// Obtain message containing the info of this control instance.
        /// This is used from the client side to send
        /// </summary>
        /// <returns></returns>
        public AccessMessage ObtainClientSideMessage()
        {
            return new AccessMessage() { Username = this.Username, Password = this.Password };
        }

        /// <summary>
        /// Update control with incoming message data.
        /// </summary>
        public void Update(AccessMessage message)
        {
            Username = message.Username;
            Password = message.Password;
        }
    }
}
