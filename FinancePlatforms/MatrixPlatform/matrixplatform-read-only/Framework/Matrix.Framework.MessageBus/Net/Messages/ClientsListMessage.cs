// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using Matrix.Common.Core;
using Matrix.Framework.MessageBus.Core;

namespace Matrix.Framework.MessageBus.Net.Messages
{
    /// <summary>
    /// Message contains info on a set of message bus clients.
    /// </summary>
    [Serializable]
    internal class ClientsListMessage : Message, IDeserializationCallback
    {
        public List<ClientId> Ids = new List<ClientId>();

        /// <summary>
        /// Over the network - transport only types names, since otherwise an 
        /// uknown types causes exceptions on the very message.
        /// </summary>
        List<string> _types = new List<string>();
        List<List<string>> _sourcesTypesNames = new List<List<string>>();

        [NonSerialized]
        List<Type> _typesLocal = new List<Type>();
        public ReadOnlyCollection<Type> Types
        {
            get
            {
                return _typesLocal.AsReadOnly();
            }
        }

        public ReadOnlyCollection<List<string>> SourcesTypes
        {
            get
            {
                return _sourcesTypesNames.AsReadOnly();
            }
        }

        public void AddType(Type type, Type sourceType)
        {
            lock (this)
            {
                _types.Add(type.AssemblyQualifiedName);
                _sourcesTypesNames.Add(ReflectionHelper.GetTypeNameAndRelatedTypes(sourceType));
            }
        }

        #region IDeserializationCallback Members

        public void OnDeserialization(object sender)
        {
            _typesLocal = new List<Type>();

            for (int i = 0; i < _types.Count; i++)
            {
                Type type = Type.GetType(_types[i]);

                if (type != null)
                {
                    lock (this)
                    {
                        _typesLocal.Add(type);
                        //_sourcesTypesLocal.Add(sourceType);
                    }
                }
            }
        }

        #endregion
    }
}
