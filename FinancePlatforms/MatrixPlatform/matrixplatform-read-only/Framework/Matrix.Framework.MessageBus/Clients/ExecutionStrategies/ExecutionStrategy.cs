// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
#if Matrix_Diagnostics
using Matrix.Common.Diagnostics;
#endif

using System;
using Matrix.Framework.MessageBus.Core;

namespace Matrix.Framework.MessageBus.Clients.ExecutionStrategies
{
    /// <summary>
    /// Base class for thread execution strategies, for the Message bus client framework.
    /// </summary>
    public abstract class ExecutionStrategy : IDisposable
    {
        protected object _syncRoot = new object();

        ActiveClient _client;
        /// <summary>
        /// Instance of the client stub, this strategy serves on.
        /// </summary>
        protected ActiveClient Client
        {
            get { return _client; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ExecutionStrategy()
        {
        }

        /// <summary>
        /// Initialize the strategy.
        /// </summary>
        public bool Initialize(ActiveClient client)
        {
            lock (_syncRoot)
            {
                if (_client != null)
                {
#if Matrix_Diagnostics
                    SystemMonitor.Warning("Client already assigned to execution strategy.");
#endif
                    return false;
                }

                //SystemMonitor.ThrowIf(client != null, "Client not assigned to execution strategy.");
                _client = client;
                return true;
            }
        }

        public virtual void Dispose()
        {
            lock (_syncRoot)
            {
                _client = null;
            }
        }

        /// <summary>
        /// Enlist item for execution.
        /// </summary>
        public virtual void Execute(Envelope envelope)
        {
            ActiveClient client = _client;

            if (envelope.ExecutionModel == Envelope.ExecutionModelEnum.Direct)
            {
                if (client != null)
                {
                    client.PerformExecution(envelope);
                }
            }
            else
            {
                OnExecute(envelope);
            }
        }

        /// <summary>
        /// Implementations handle this to do the actual execution.
        /// </summary>
        protected abstract void OnExecute(Envelope envelope);

    }
}
