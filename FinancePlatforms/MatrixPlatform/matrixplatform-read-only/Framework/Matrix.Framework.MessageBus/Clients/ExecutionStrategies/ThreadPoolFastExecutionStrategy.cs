// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Core;
using Matrix.Common.Extended;
using Matrix.Common.Extended.ThreadPools;
using Matrix.Framework.MessageBus.Core;

namespace Matrix.Framework.MessageBus.Clients.ExecutionStrategies
{
    /// <summary>
    /// Execution strategy implemented using the custom fast thread pool implementation.
    /// </summary>
    public class ThreadPoolFastExecutionStrategy : ExecutionStrategy
    {
        bool _useCommonMessageBusPool;

        volatile ThreadPoolFast _pool;

        FastInvokeHelper.FastInvokeHandlerDelegate _performExecutionDelegate = null;

        public ThreadPoolFast ThreadPool
        {
            get
            {
                if (_pool == null)
                {
                    lock (this)
                    {
                        if (_pool == null)
                        {
                            if (_useCommonMessageBusPool)
                            {
                                // Refactor caution!
                                if (Client.MessageBus != null)
                                {
                                    _pool = Client.MessageBus.DefaultThreadPool;
                                }
                            }
                            else
                            {
                                _pool = new ThreadPoolFast(Client != null  && Client.Id != null ? Client.Id.Name + ".ThreadPoolFast" : "NA.2.ThreadPoolFast");
                            }
                        }
                    }
                }

                return _pool;
            }
        }

        /// <summary>
        /// This only applicable when a custom local thread pool used.
        /// </summary>
        public int MinimumThreadsCount
        {
            get
            {
                ThreadPoolFast pool = ThreadPool;
                if (pool != null)
                {
                    return pool.MinimumThreadsCount;
                }
                return 0;
            }

            set
            {
                if (_useCommonMessageBusPool == false)
                {
                    ThreadPoolFast pool = ThreadPool;
                    if (pool != null)
                    {
                        pool.MinimumThreadsCount = value;
                    }
                }
            }
        }

        /// <summary>
        /// This only applicable when a custom local thread pool used.
        /// </summary>
        public int MaximumThreadsCount
        {
            get 
            { 
                ThreadPoolFast pool = ThreadPool;
                if (pool != null)
                {
                    return pool.MaximumThreadsCount;
                }
                return 0;
            }
            
            set
            {
                if (_useCommonMessageBusPool == false)
                {
                    ThreadPoolFast pool = ThreadPool;
                    if (pool != null)
                    {
                        pool.MaximumThreadsCount = value;
                    }
                }
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ThreadPoolFastExecutionStrategy(bool useCommonMessageBusPool)
        {
            _useCommonMessageBusPool = useCommonMessageBusPool;

            GeneralHelper.GenericDelegate<Envelope> delegateInstance = new GeneralHelper.GenericDelegate<Envelope>(PerformExecution);
            _performExecutionDelegate = FastInvokeHelper.GetMethodInvoker(delegateInstance.Method, true, false);
        }

        public override void Dispose()
        {
            lock (_syncRoot)
            {
                if (_pool != null)
                {
                    _pool.Dispose();
                }
                _pool = null;
            }

            base.Dispose();
        }

        /// <summary>
        /// Actually perform the exection.
        /// </summary>
        internal void PerformExecution(object envelope)
        {
            ActiveClient client = Client;
            if (client != null)
            {
                client.PerformExecution(envelope as Envelope);
            }
        }

        protected override void OnExecute(Envelope envelope)
        {
            ThreadPoolFast pool = ThreadPool;
            if (pool != null)
            {
                pool.QueueFastDelegate(this, _performExecutionDelegate, envelope);
                
                // Other invocation options.
                //ThreadPoolFastEx.TargetInfo targetInfo = new ThreadPoolFastEx.TargetInfo(string.Empty, this,
                //    _singleFastInvokeDelegate, threadPool, envelope);
                //threadPool.QueueTargetInfo(targetInfo);
            }
        }
    }
}
