// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using Matrix.Framework.MessageBus.Clients.ExecutionStrategies;
using Matrix.Framework.MessageBus.Core;
using Matrix.Common.Core.Identification;

namespace Matrix.Framework.MessageBus.Clients
{
    /// <summary>
    /// Default stub standalone implementation of an Message bus Slim client.
    /// </summary>
    public class ActiveClient : MessageBusClient
    {
        volatile IMessageBus _messageBus = null;
        /// <summary>
        /// The instance of the message bus this client belongs to.
        /// </summary>
        public override IMessageBus MessageBus
        {
            get { return _messageBus; }
        }

        ExecutionStrategy _executionStrategy;
        /// <summary>
        /// Execution strategy defines how threads are managed on executing the tasks in the client.
        /// </summary>
        public override ExecutionStrategy ExecutionStrategy
        {
            get { return _executionStrategy; }
        }

        ClientId _id;
        /// <summary>
        /// The Id of this client as provided by the currently assigned
        /// message bus.
        /// </summary>
        public override ClientId Id
        {
            get { return _id; }
        }

        public override Type OptionalSourceType
        {
            get { return null; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ActiveClient(ClientId id)
        {
            _id = id;

            _executionStrategy = null;
            _messageBus = null;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ActiveClient(string name)
            : this(new ClientId(name))
        {
        }

        public override void Dispose()
        {
            _executionStrategy = null;
            _messageBus = null;

            base.Dispose();
        }

        /// <summary>
        /// Setup execution strategy for the 
        /// execution of class on this client.
        /// </summary>
        public override bool SetupExecutionStrategy(ExecutionStrategy executionStrategy)
        {
            lock (this)
            {
                if (_executionStrategy != null)
                {
                    _executionStrategy.Dispose();
                    _executionStrategy = null;
                }

                _executionStrategy = executionStrategy;
            }

            if (executionStrategy != null)
            {
                return executionStrategy.Initialize(this);
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        internal override bool AssignMessageBus(IMessageBus messageBus, int indexId)
        {
            if (_messageBus != null)
            {
                return false;
            }

            _id.MessageBus = messageBus;
            _id.LocalMessageBusIndex = indexId;

            //lock (this)
            {// Lock needed to assure operations go together.
                _messageBus = messageBus;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        internal override void ReleaseMessageBus()
        {
            lock (this)
            {// Lock needed to assure operations go together.
                _messageBus = null;
            }
        }

        /// <summary>
        /// Receive an envelope for executing.
        /// </summary>
        internal override bool Receive(Envelope envelope)
        {
            ExecutionStrategy executionStrategy = _executionStrategy;
            if (executionStrategy != null)
            {
                executionStrategy.Execute(envelope);
            }

            RaiseEnvelopeReceivedEvent(envelope);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void PerformExecution(Envelope envelope)
        {
            RaiseEnvelopeExecutingEvent(envelope);

            OnPerformExecution(envelope);

            RaiseEnvelopeExecutedEvent(envelope);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnPerformExecution(Envelope envelope)
        {

        }

    }
}
