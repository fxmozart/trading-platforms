// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;

//namespace MessageBus.Core
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class JSonMethodInfoConverter : Newtonsoft.Json.JsonConverter
//    {
//        const char Separator = '|';

//        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
//        {
//            if (value == null)
//            {
//                writer.WriteNull();
//                return;
//            }

//            MethodBase method = (MethodBase)value;

//            StringBuilder valueBuilder = new StringBuilder();
//            valueBuilder.Append(method.DeclaringType.AssemblyQualifiedName);
//            valueBuilder.Append(Separator);
//            valueBuilder.Append(method.Name);

//            foreach (ParameterInfo parameter in method.GetParameters())
//            {
//                valueBuilder.Append(Separator);
//                valueBuilder.Append(parameter.ParameterType.AssemblyQualifiedName);
//            }

//            // Write the value to the writer.
//            writer.WriteValue(valueBuilder.ToString());
//        }

//        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, Newtonsoft.Json.JsonSerializer serializer)
//        {
//            string[] values = reader.Value.ToString().Split(Separator);

//            Type declaringType = Type.GetType(values[0]);
//            string methodName = values[1];

            
//            Type[] argumentsTypes = new Type[values.Length - 2];
//            for (int i = 2; i < values.Length; i++)
//            {
//                argumentsTypes[i - 2] = Type.GetType(values[i]);
//            }

//            MethodInfo mi = declaringType.GetMethod(methodName, argumentsTypes);


//         //Type t = (ReflectionUtils.IsNullableType(objectType)) ? Nullable.GetUnderlyingType(objectType) : objectType;

//  //          if (reader.TokenType == JsonToken.Null)
//  //          {
//  //              if (!ReflectionUtils.IsNullable(objectType))
//  //                  throw new Exception("Cannot convert null value to {0}.".FormatWith(CultureInfo.InvariantCulture, objectType));

//  //              return null;
//  //          }

//  //          if (reader.TokenType != JsonToken.String)
//  //              throw new Exception("Unexpected token parsing binary. Expected String, got {0}.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));

//  //          // current token is already at base64 string
//  //          // unable to call ReadAsBytes so do it the old fashion way
//  //          string encodedData = reader.Value.ToString();
//  //          byte[] data = Convert.FromBase64String(encodedData);
//            return null;
//        }

//        public override bool CanConvert(Type objectType)
//        {
//            return objectType.IsSubclassOf(typeof(MethodBase))
//                || objectType == typeof(MethodBase);
//        }
//    }
//}
