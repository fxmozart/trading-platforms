// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;
//using Common.Extended;
//using Newtonsoft.Json;
//using System.IO;
//using Newtonsoft.Json.MatrixPlatformSpecific;

//namespace MessageBus.Core
//{
//    /// <summary>
//    /// Serializer using the Json.NET framework.
//    /// </summary>
//    public class JSonSerializer : SerializerBase
//    {
//        JsonSerializerSettings Settings { get; set; }
        
//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public JSonSerializer()
//        {
//            // Assign settings to include the type data into the stream
//            // so that deserialization can be done successfully.
//            Settings = new JsonSerializerSettings();
//            Settings.TypeNameHandling = TypeNameHandling.All;
//            Settings.Converters.Add(new JSonMethodInfoConverter());
//            Settings.
//        }

//        protected override bool SerializeData(System.IO.Stream stream, object message)
//        {
//            string jsonText = JsonConvert.SerializeObject(message, Formatting.None, Settings);
            
//            //byte[] data = Convert.FromBase64String(jsonText);
//            //byte[] dataLength = System.BitConverter.GetBytes(data.Length);
//            //stream.Write(dataLength, 0, dataLength.Length);
//            //stream.Write(data, 0, data.Length);

//            using (StreamWriter sw = new StreamWriter(stream))
//            {
//                sw.Write(jsonText);
//            }

//            return true;
//        }

//        protected override object DeserializeData(System.IO.Stream stream)
//        {
//            using (StreamReader reader = new StreamReader(stream))
//            {
//                object deserialized = JsonConvert.DeserializeObject(reader.ReadToEnd(), null, Settings);
//                return deserialized;
//            }

//            //TextReader tr;
//            //tr.ReadBlock(a, 0, 0);

//            //byte[] dataLengthBuffer = new byte[sizeof(int)];
//            //if (stream.Read(dataLengthBuffer, 0, dataLengthBuffer.Length) != sizeof(int))
//            //{
//            //    throw new InvalidDataException("Not enough data in stream.");
//            //}

//            //int stringLength = BitConverter.ToInt32(dataLengthBuffer, 0);
//            //if (stringLength > MaxMessageSize * 10)
//            //{
//            //    throw new InvalidDataException("Invalid data size.");
//            //}

//            //byte[] data = new byte[stringLength];
//            //stream.Read(

            
//        }

//    }
//}
