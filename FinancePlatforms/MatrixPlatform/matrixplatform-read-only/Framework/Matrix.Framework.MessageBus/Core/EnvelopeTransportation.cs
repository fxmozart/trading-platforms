// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using Matrix.Common.Extended.FastSerialization;
using Wintellect.PowerCollections;

namespace Matrix.Framework.MessageBus.Core
{
    /// <summary>
    /// The stamp contains transporting information regarding the path of the envelope.
    /// </summary>
    [Serializable]
    public class EnvelopeTransportation : ISerializable, ICloneable
    {
        Deque<EnvelopeStamp> _stamps;

        /// <summary>
        /// Count of stamps inside this transport info.
        /// </summary>
        public int StampsCount
        {
            get { return _stamps.Count; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public EnvelopeTransportation()
        {
            _stamps = new Deque<EnvelopeStamp>();
        }

        #region ISerializable Members

        /// <summary>
        /// Implementing the ISerializable to provide a faster, more optimized
        /// serialization for the class.
        /// </summary>
        public EnvelopeTransportation(SerializationInfo info, StreamingContext context)
        {
            // Get from the info.
            SerializationReader reader = new SerializationReader((byte[])info.GetValue("data", typeof(byte[])));

            object[] stamps = reader.ReadObjectArray();
            if (stamps.Length == 0)
            {
                _stamps = new Deque<EnvelopeStamp>();
            }
            else
            {
                _stamps = new Deque<EnvelopeStamp>((EnvelopeStamp[])stamps);
            }
        }

        /// <summary>
        /// Implementing the ISerializable to provide a faster, more optimized
        /// serialization for the class.
        /// </summary>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            SerializationWriter writer = new SerializationWriter();

            lock (_stamps)
            {
                writer.Write((object[])_stamps.ToArray());
            }

            // Put to the info.
            info.AddValue("data", writer.ToArray());
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void PushStamp(EnvelopeStamp stamp)
        {
            lock (_stamps)
            {
                _stamps.Add(stamp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EnvelopeStamp PopStamp()
        {
            if (_stamps.Count > 0)
            {
                lock (_stamps)
                {
                    if (_stamps.Count > 0)
                    {
                        return _stamps.RemoveFromFront();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Create a responding envelope transport.
        /// </summary>
        public static EnvelopeTransportation CreateResponseTransport(EnvelopeTransportation transport)
        {
            EnvelopeTransportation result = new EnvelopeTransportation();
            List<EnvelopeStamp> responseStamps = new List<EnvelopeStamp>();
            
            lock (transport._stamps)
            {
                for (int i = transport._stamps.Count - 1; i >= 0; i--)
                {
                    EnvelopeStamp sourceStamp = transport._stamps[i];
                    EnvelopeStamp newStamp = new EnvelopeStamp(sourceStamp.MessageBusStampId, sourceStamp.SenderIndex, sourceStamp.ReceiverIndex);
                    result._stamps.Add(newStamp);
                }
            }

            return result;
        }

        /// <summary>
        /// Clone the envelope transport.
        /// </summary>
        public EnvelopeTransportation Duplicate()
        {
            lock (_stamps)
            {
                return new EnvelopeTransportation() { _stamps = new Deque<EnvelopeStamp>(this._stamps) };
            }
        }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return Duplicate();
        }

        #endregion
    }
}
