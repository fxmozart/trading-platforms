// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using Matrix.Common.Extended.Operationals;
using Matrix.Framework.MessageBus.Clients.ExecutionStrategies;

namespace Matrix.Framework.MessageBus.Core
{
    /// <summary>
    /// Base interface for client implementations of the message bus client.
    /// </summary>
    public abstract class MessageBusClient : Operational, IDisposable
    {
        public abstract IMessageBus MessageBus { get; }

        public abstract ClientId Id { get; }

        public abstract ExecutionStrategy ExecutionStrategy { get; }

        /// <summary>
        /// The type of the source (optional, may be null in case client is used in standalone mode).
        /// </summary>
        public abstract Type OptionalSourceType { get; }

        public delegate void EnvelopeUpdateDelegate(MessageBusClient stub, Envelope envelope);
        public delegate void ClientUpdateDelegate(MessageBusClient client);

        /// <summary>
        /// *IMPORTANT* this event is executed on an incoming thread from the message bus, and must never be blocked, since it will compomise the model.
        /// This is NOT executed on the new execution thread, use EnvelopeExecuteEvent for this. Process calls on this event as fast as possible.
        /// </summary>
        public event EnvelopeUpdateDelegate EnvelopeReceivedEvent;

        public event EnvelopeUpdateDelegate EnvelopeExecutingEvent;
        public event EnvelopeUpdateDelegate EnvelopeExecutedEvent;

        /// <summary>
        /// General update of some of the properties of the client.
        /// </summary>
        public event ClientUpdateDelegate UpdateEvent;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MessageBusClient()
        {
        }

        /// <summary>
        /// Dispose of any associations or resources.
        /// Will also free any of the events, although it 
        /// is any subscriber responcibility to release the events as well.
        /// </summary>
        public virtual void Dispose()
        {
            EnvelopeReceivedEvent = null;
            EnvelopeExecutingEvent = null;
            EnvelopeExecutedEvent = null;
            UpdateEvent = null;
        }

        internal abstract bool Receive(Envelope envelope);

        internal abstract bool AssignMessageBus(IMessageBus messageBus, int indexId);

        internal abstract void ReleaseMessageBus();

        public abstract bool SetupExecutionStrategy(ExecutionStrategy executionStrategy);
        
        protected void RaiseEnvelopeReceivedEvent(Envelope envelope)
        {
            EnvelopeUpdateDelegate envelopeReceivedDelegate = EnvelopeReceivedEvent;
            if (envelopeReceivedDelegate != null)
            {
                envelopeReceivedDelegate(this, envelope);
            }
        }

        protected void RaiseEnvelopeExecutingEvent(Envelope envelope)
        {
            EnvelopeUpdateDelegate envelopeReceivedDelegate = EnvelopeExecutingEvent;
            if (envelopeReceivedDelegate != null)
            {
                envelopeReceivedDelegate(this, envelope);
            }
        }

        protected void RaiseEnvelopeExecutedEvent(Envelope envelope)
        {
            EnvelopeUpdateDelegate envelopeReceivedDelegate = EnvelopeExecutedEvent;
            if (envelopeReceivedDelegate != null)
            {
                envelopeReceivedDelegate(this, envelope);
            }
        }

        /// <summary>
        /// Notify all interested parties, that the client has undergone some update.
        /// </summary>
        public void RaiseUpdateEvent()
        {
            ClientUpdateDelegate del = UpdateEvent;
            if (del != null)
            {
                del(this);
            }
        }
    }
}
