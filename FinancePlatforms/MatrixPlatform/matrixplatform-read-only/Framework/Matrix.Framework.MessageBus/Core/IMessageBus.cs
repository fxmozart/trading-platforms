// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Extended.ThreadPools;
using Matrix.Framework.MessageBus.Net;
using Matrix.Framework.MessageBus.Clients.ExecutionStrategies;

namespace Matrix.Framework.MessageBus.Core
{
    /// <summary>
    /// Helper delegate.
    /// </summary>
    public delegate void MessageBusClientUpdateDelegate(IMessageBus messageBus, ClientId clientId);

    /// <summary>
    /// Helper delegate.
    /// </summary>
    /// <param name="permanentRemove">Clients may be temprorarily removed (for ex. when temporarily offline) or permanently. Permanent means destroy all possible and pending connections to client.</param>
    public delegate void MessageBusClientRemovedDelegate(IMessageBus messageBus, ClientId clientId, bool permanentRemove);

    /// <summary>
    /// Helper delegate.
    /// </summary>
    /// <param name="permanentRemove">Clients may be temprorarily removed (for ex. when temporarily offline) or permanently. Permanent means destroy all possible and pending connections to client.</param>
    public delegate void MessageBusCounterPartyUpdateDelegate(IMessageBus messageBus, string counterPartyId, string state);

    /// <summary>
    /// Provides information on the outcome of a send operation.
    /// </summary>
    public enum OutcomeEnum
    {
        // There is no information if the call succeeded or not.
        Unknown,
        // Call succeeded.
        Success,
        // Call failed, reason not specified.
        Failure,
        // Call failed due to time out.
        TimeoutFailure,
        // System is not able to process the operation.
        SystemFailture
    }

    /// <summary>
    /// Interface defines the appearance of a message bus.
    /// </summary>
    public interface IMessageBus : IDisposable
    {
        /// <summary>
        /// Raised when a client has been added to the bus.
        /// </summary>
        event MessageBusClientUpdateDelegate ClientAddedEvent;
        
        /// <summary>
        /// Raised when the client has been removed from the bus.
        /// Clients may be temprorarily removed (for ex. when temporarily offline) or permanently. Permanent means destroy all possible and pending connections to client.
        /// </summary>
        event MessageBusClientRemovedDelegate ClientRemovedEvent;
        
        /// <summary>
        /// Raise when a client has updated it self, and wishes to share this knowledge.
        /// This is not message bus specific, just a helper path to allow items to
        /// minitor each other for common changes withough a complex message mechanism.
        /// </summary>
        event MessageBusClientUpdateDelegate ClientUpdateEvent;

        /// <summary>
        /// If the message bus is connected to some other counter party (for ex. a 
        /// server or a client mesasge bus) and this party changes state (for ex.
        /// gets shutdown), this is the notification.
        /// </summary>
        event MessageBusCounterPartyUpdateDelegate CounterPartyUpdateEvent;

        /// <summary>
        /// Name of the message bus (for ex. assign something descriptive of 
        /// the module and operations the bus is to be performing).
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The deafault thread pool instance that is used to execute items on the bus.
        /// </summary>
        ThreadPoolFastEx DefaultThreadPool { get; }

        /// <summary>
        /// Add a client to the message bus.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        bool AddClient(MessageBusClient client);
        
        /// <summary>
        /// Remove a client from the message bus.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="isPermanent"></param>
        /// <returns></returns>
        bool RemoveClient(MessageBusClient client, bool isPermanent);
        
        /// <summary>
        /// Check if a client with this ID is part of this message bus.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        bool ContainsClient(ClientId client);

        /// <summary>
        /// Obtain the Ids of all clients.
        /// </summary>
        /// <returns></returns>
        List<ClientId> GetAllClientsIds();

        /// <summary>
        /// Directly obtain the instance of a local client, use with caution, 
        /// since this brakes the decoupling model, and must be utilized only
        /// when absolutely necessary.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        MessageBusClient GetLocalClientInstance(ClientId clientId);

        /// <summary>
        /// Obtain the type of the client instance. This is NOT 
        /// the type of the source, only the type of the client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Type GetClientType(ClientId clientId);
        
        /// <summary>
        /// Get the (assembly qualified) names of the client source;
        /// this provides not only the actual source type, but also 
        /// all the classes and interfaces it inherits, since the
        /// discovery process needs that to consume common interfaces
        /// with separate implementations.
        /// </summary>
        List<string> GetClientSourceTypes(ClientId clientId);

        /// <summary>
        /// Send a message.
        /// </summary>
        /// <param name="senderId">The identified of the sending party.</param>
        /// <param name="receiversIds">The identifier of the receiving party.</param>
        /// <param name="envelope">The envelope containing the message sent.</param>
        /// <param name="requestConfirmTimeout">Timeout assigned for the "confirmation of receival" of the envelope. Not related in any way to the execution of the message.</param>
        /// <param name="showErrorsDiagnostics">Show errors in diagnostics.</param>
        /// <returns>True if the call was a success, otherwise false.</returns>
        OutcomeEnum Send(ClientId senderId, ClientId receiverId, Envelope envelope, TimeSpan? requestConfirmTimeout, bool showErrorsDiagnostics);
        
        /// <summary>
        /// Send a message.
        /// </summary>
        /// <param name="senderId">The identified of the sending party.</param>
        /// <param name="receiversIds">The identifiers of the receiving parties.</param>
        /// <param name="envelope">The envelope containing the message sent.</param>
        /// <param name="requestConfirmTimeout">Timeout assigned for the "confirmation of receival" of the envelope. Not related in any way to the execution of the message.</param>
        /// <param name="showErrorsDiagnostics">Show errors in diagnostics.</param>
        /// <returns>True if the call was a success, otherwise false.</returns>
        OutcomeEnum Send(ClientId senderId, IEnumerable<ClientId> receiversIds, Envelope envelope, TimeSpan? requestConfirmTimeout, bool showErrorsDiagnostics);
        
        /// <summary>
        /// Respond to a received message.
        /// </summary>
        /// <param name="receivedEnvelope">The envelope containing the message received.</param>
        /// <param name="responseEnvelope">The responding envelope.</param>
        /// <returns>True if the call was a success, otherwise false.</returns>
        OutcomeEnum Respond(Envelope receivedEnvelope, Envelope responseEnvelope);
    }
}
