// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using Matrix.Common.Extended.FastSerialization;

namespace Matrix.Framework.MessageBus.Core
{
    /// <summary>
    /// A stamp is placed by passing trough one message bus.
    /// It can be used both as history, and as indication where 
    /// to send a respoding envelope.
    /// </summary>
    [Serializable]
    public class EnvelopeStamp : ICloneable
    {
        long _stampId = 0;

        /// <summary>
        /// Number of the transport
        /// </summary>
        public long MessageBusStampId
        {
            get { return _stampId; }
            set { _stampId = value; }
        }

        volatile ClientId _receiverId = null;
        /// <summary>
        /// Index of the receiving entity.
        /// </summary>
        public ClientId ReceiverIndex
        {
            get { return _receiverId; }
            set { _receiverId = value; }
        }

        volatile ClientId _senderId = null;
        /// <summary>
        /// Index of the sender entity.
        /// </summary>
        public ClientId SenderIndex
        {
            get { return _senderId; }
            set { _senderId = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public EnvelopeStamp(long stampId, ClientId receiverId, ClientId senderId)
        {
            _stampId = stampId;
            _receiverId = receiverId;
            _senderId = senderId;
        }

        #region ISerializable Members

        /// <summary>
        /// Implementing the ISerializable to provide a faster, more optimized
        /// serialization for the class.
        /// </summary>
        public EnvelopeStamp(SerializationInfo info, StreamingContext context)
        {
            // Get from the info.
            SerializationReader reader = new SerializationReader((byte[])info.GetValue("data", typeof(byte[])));

            _stampId = reader.ReadInt64();
            _receiverId = (ClientId)reader.ReadObject();
            _senderId = (ClientId)reader.ReadObject();
        }

        /// <summary>
        /// Implementing the ISerializable to provide a faster, more optimized
        /// serialization for the class.
        /// </summary>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            SerializationWriter writer = new SerializationWriter();

            writer.Write(_stampId);
            writer.WriteObject(_receiverId);
            writer.WriteObject(_senderId);

            // Put to the info.
            info.AddValue("data", writer.ToArray());
        }

        #endregion

        public object Clone()
        {
            return Duplicate();
        }

        public EnvelopeStamp Duplicate()
        {
            ClientId senderId = this._senderId;
            ClientId receiverId = this._receiverId;

            if (senderId != null)
            {
                senderId = senderId.Duplicate();
            }

            if (receiverId != null)
            {
                receiverId = receiverId.Duplicate();
            }

            return new EnvelopeStamp(_stampId, senderId, receiverId);
        }

    }
}
