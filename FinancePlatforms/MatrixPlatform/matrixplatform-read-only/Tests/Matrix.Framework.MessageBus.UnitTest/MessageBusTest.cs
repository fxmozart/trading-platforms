// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Matrix.Framework.MessageBus.Clients;
using Matrix.Framework.MessageBus.Core;
using NUnit.Framework;

namespace Matrix.Framework.MessageBus.UnitTest
{
    /// <summary>
    /// Test common functionality of the message bus framework.
    /// Current performace of simple message transfer is around 500-600K.
    /// </summary>
    [TestFixture]
    public class MessageBusTest
    {
        MessageBus.Core.MessageBus _bus = new Matrix.Framework.MessageBus.Core.MessageBus("Bus");
        ActiveInvocatorClient _client1, _client2;

        int _targetCount = 0;

        ManualResetEvent _readyEvent = new ManualResetEvent(false);

        public MessageBusTest()
        {
            _client1 = new ActiveInvocatorClient("Client1");
            _client2 = new ActiveInvocatorClient("Client2");

            _client2.EnvelopeReceivedEvent += new MessageBusClient.EnvelopeUpdateDelegate(_client2_EnvelopeReceivedEvent);
            _client2.EnvelopeExecutedEvent += new MessageBusClient.EnvelopeUpdateDelegate(_client2_EnvelopeExecutedEvent);
            _bus.AddClient(_client1);
            _bus.AddClient(_client2);
        }

        void _client2_EnvelopeExecutedEvent(MessageBusClient stub, Envelope envelope)
        {
            if (Interlocked.Decrement(ref _targetCount) == 0)
            {
                _readyEvent.Set();
            }
        }

        void _client2_EnvelopeReceivedEvent(MessageBusClient stub, Envelope envelope)
        {
            //if (Interlocked.Decrement(ref _targetCount) == 0)
            //{
            //    _readyEvent.Set();
            //}
        }

        [Test]
        public void SimpleTest([Values(10000, 35000, 100000, 1000000)] int length)
        {
            _targetCount = length;
            for (int i = 0; i < length; i++)
            {
                Envelope envelope = new Envelope() { Message = i };
                _bus.Send(_client1.Id, _client2.Id, envelope, null, false);
            }

            _readyEvent.WaitOne();
        }

        [Test]
        public void ActiveClientEnvelopeReceiverTest([Values(10000, 35000, 100000, 1000000)] int length)
        {

        }
    }
}
