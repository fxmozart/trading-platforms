// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using Matrix.Common.Diagnostics;
using Matrix.Common.Diagnostics.TracerCore;
using Matrix.Framework.MessageBus.Net;
using Matrix.Framework.SuperPool.Core;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.SuperPool.Test.SpeedTests
{
    /// <summary>
    /// Test the operation of the super pool over network connection.
    /// The current setup is 2-3K per second.
    /// </summary>
    public class NetworkEventsTest : SpeedTest
    {
        Matrix.Framework.SuperPool.Core.SuperPool _poolServer;
        Matrix.Framework.SuperPool.Core.SuperPool _poolProxy;
        Matrix.Framework.SuperPool.Core.SuperPool _poolProxy2;

        InterfaceImplementor _implementorServer;
        InterfaceImplementor _implementorClient;
        InterfaceImplementor _implementorClient2;

        public bool TestAccessControl { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public NetworkEventsTest()
            : base(true)
        {
            SystemMonitor.AssignTracer(new Tracer());
        }

        public override void Dispose()
        {
            if (_poolServer != null)
            {
                _poolServer.Dispose();
            }

            if (_poolProxy != null)
            {
                _poolProxy.Dispose();
            }

            if (_implementorServer != null)
            {
                _implementorServer.Dispose();
            }

            if (_implementorClient != null)
            {
                _implementorClient.Dispose();
            }

            base.Dispose();
        }

        void Initialize()
        {
            if (_poolServer != null)
            {
                return;
            }


            if (TestAccessControl)
            {
                _poolServer = new Matrix.Framework.SuperPool.Core.SuperPool(new ServerMessageBus("Server.MessageBus", ServerMessageBus.DefaultPort,
                                                                        new ServerAccessControl() { Password = "ribspa" }));
                _poolProxy = new Matrix.Framework.SuperPool.Core.SuperPool(new ClientMessageBus(new IPEndPoint(IPAddress.Loopback, ServerMessageBus.DefaultPort), "One",
                                                                       new ClientAccessControl() { Password = "ribspa" }));
                _poolProxy2 = new Matrix.Framework.SuperPool.Core.SuperPool(new ClientMessageBus(new IPEndPoint(IPAddress.Loopback, ServerMessageBus.DefaultPort), "Two",
                                                                        new ClientAccessControl() { Password = "ribspa" }));
            }
            else
            {
                _poolServer = new Matrix.Framework.SuperPool.Core.SuperPool(new ServerMessageBus("Server.MessageBus", ServerMessageBus.DefaultPort, null));
                _poolProxy = new Matrix.Framework.SuperPool.Core.SuperPool(new ClientMessageBus(new IPEndPoint(IPAddress.Loopback, ServerMessageBus.DefaultPort), "One", null));
                _poolProxy2 = new Matrix.Framework.SuperPool.Core.SuperPool(new ClientMessageBus(new IPEndPoint(IPAddress.Loopback, ServerMessageBus.DefaultPort), "Two", null));
            }


            _implementorServer = new InterfaceImplementor("Client.Server");
            _poolServer.AddClient(_implementorServer.Client);

            _implementorClient = new InterfaceImplementor("Client.Proxy");
            _poolProxy.AddClient(_implementorClient.Client);

            _implementorClient2 = new InterfaceImplementor("Client.Proxy.2");
            _poolProxy2.AddClient(_implementorClient2.Client);
        }

        public override void Update(FormTesting form)
        {
            
        }

        public override bool OnRun(FormTesting form, int count)
        {
            Initialize();

            //_implementorServer.SubscribeTo(_implementorClient.Client.Id);
            //_implementorServer.SubscribeTo(_implementorClient.Client.Id);
            _implementorServer.SubscribeTo(_implementorClient2.Client.Id);

            for (int i = 0; i < count; i++)
            {
                try
                {
                    if (i == 10)
                    {
                        _implementorServer.UnSubscribeTo(_implementorClient.Client.Id);
                        _implementorServer.UnSubscribeTo(_implementorClient.Client.Id);
                        SystemMonitor.Info("*UNSUBSCRIBING*");
                    }

                    //Thread.Sleep(2000);
                    _implementorClient.RaiseEvent("one");
                    _implementorClient2.RaiseEvent("two");
                }
                catch (Exception ex)
                {
                    string m = ex.Message;
                }
            }

            return true;
        }

        void SuperPoolNetworkTest_Event(string parameter1)
        {
            throw new NotImplementedException();
        }
    }
}
