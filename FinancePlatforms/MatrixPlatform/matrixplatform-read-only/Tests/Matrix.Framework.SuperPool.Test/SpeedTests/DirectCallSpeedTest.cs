// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Matrix.Common.Diagnostics;
using Matrix.Framework.TestFramework;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.SuperPool.Core;

namespace Matrix.Framework.SuperPool.Test.SpeedTests
{
    public interface DirectCallSpeedTest_Interface
    {
        int Run();
    }

    /// <summary>
    /// 
    /// </summary>
    public class DirectCallSpeedTest : SpeedTest, DirectCallSpeedTest_Interface
    {
        Matrix.Framework.SuperPool.Core.SuperPool pool;
        SuperPoolClient client1;
        SuperPoolClient client2;

        bool _testEventHandling = true;
        /// <summary>
        /// 
        /// </summary>
        public bool TestEventHandling
        {
            get { return _testEventHandling; }
            set { _testEventHandling = value; }
        }

        public interface Interface
        {
            int Run2();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public DirectCallSpeedTest()
            : base(true)
        {
            pool = new Matrix.Framework.SuperPool.Core.SuperPool();

            client1 = new SuperPoolClient("c1", this);
            client2 = new SuperPoolClient("c2", this);

            bool result = pool.AddClient(client1);
            result = pool.AddClient(client2);
        }

        public override void Update(FormTesting form)
        {
            
        }

        public override bool OnRun(FormTesting form, int count)
        {
            SystemMonitor.Info("Start...");

            for (int i = 0; i < count; i++)
            {
                client1.CallDirectLocal<DirectCallSpeedTest_Interface>(client2.Id).Run();
                //if (client1.DirectLocalCall<DirectCallSpeedTest_Interface>(client2.Id).Run() != 12)
                //{
                //    throw new Exception("Failure");
                //}
            }

            base.SignalTestComplete();
            return true;
        }

        public int Run()
        {
            base.IncrementExecuted();
            return 12;
        }
    }
}
