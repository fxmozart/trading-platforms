// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------

using Matrix.Common.Core.Collections;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.MultiThread
{
    /// <summary>
    /// Current config runs at around 7.6Mil per second.
    /// With locks placed, the rate is around 3.5Mil.
    /// </summary>
    public class HotSwapListTest : MultiThreadTest
    {
        HotSwapList<int> _list = new HotSwapList<int>();

        int _desiredItemsCount = 10;
        /// <summary>
        /// 
        /// </summary>
        public int DesiredItemsCount
        {
            get { return _desiredItemsCount; }
            set { _desiredItemsCount = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public HotSwapListTest()
        {
        }

        protected override void OnTestStart()
        {
            _list.Clear();
        }

        protected override void OnTestEnd()
        {
            _list.Clear();
        }

        public override void Update(FormTesting form)
        {
            form.ReportSecondary(_list.Count + " items in list");
        }

        protected override void OnExecuteStep(FormTesting form, long stepId)
        {
            double x = 0;
            //lock (this)
            {
                foreach (int value in _list)
                {
                    x += value;
                }
            }

            //// This test a clone iteration.
            //// CORRECT 
            //List<int> clone = new List<int>(_list.AsReadOnly());

            //// IN-CORRECT 
            //List<int> cloneWrong = new List<int>(_list);

            ////// CORRECT 
            //ReadOnlyCollection<int> clone2 = _list.AsReadOnly();
            //for (int i = 0; i < clone2.Count; i++)
            //{
            //    int p = clone2[i];
            //}

            //// IN-CORRECT 
            //for (int i = 0; i < _list.Count; i++)
            //{
            //    int p = _list[i];
            //}


            if (stepId % 1200 == 0)
            {
                if (_list.Count <= _desiredItemsCount)
                {
                    //lock (_list)
                    {
                        _list.Add(76);
                    }
                }
            }
            else if (stepId % 1200 == 60)
            {
                if (_list.Count >= _desiredItemsCount)
                {
                    //lock (_list)
                    {
                        _list.RemoveAt(0);
                    }
                }
            }
        }
    }
}
