// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Reflection;
using Matrix.Common.Extended;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.Speed
{
    public class GetMethodExtendedNameSpeedTest : SpeedTest
    {
        MethodInfo method;
        /// <summary>
        /// 
        /// </summary>
        public GetMethodExtendedNameSpeedTest()
            : base(false)
        {
            method = typeof(GetMethodExtendedNameSpeedTest).GetMethod("OnRun");
        }

        public override bool OnRun(FormTesting form, int count)
        {
            string name = string.Empty;
            for (_executed = 0; _executed < count; _executed++)
            {
                name = GeneralHelper.GetMethodExtendedName(method, true);
            }

            return string.IsNullOrEmpty(name);
        }

        public override void Update(FormTesting form)
        {
            
        }
    }
}
