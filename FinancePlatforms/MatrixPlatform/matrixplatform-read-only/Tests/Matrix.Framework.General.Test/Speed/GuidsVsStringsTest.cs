// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.Speed
{
    /// <summary>
    /// Guids is around 20-21K operations per second, at the 10K test.
    /// Strings is 14-15K operations per seconds, at the 10K test, even with simple strings (integers) the performance is very similar.
    /// </summary>
    public class GuidsVsStringsTest : SpeedTest
    {
        bool _storeAsStrings = true;
        /// <summary>
        /// 
        /// </summary>
        public bool StoreAsStrings
        {
            get { return _storeAsStrings; }
            set { _storeAsStrings = value; }
        }

        List<Guid> _guids = new List<Guid>();
        
        List<string> _guidsStrings = new List<string>();

        /// <summary>
        /// 
        /// </summary>
        public GuidsVsStringsTest()
            : base(false)
        {
        }

        public override void Update(FormTesting form)
        {
        }

        public override bool OnRun(FormTesting form, int count)
        {
            _guids.Clear();
            _guidsStrings.Clear();

            for (_executed = 0; _executed < count; _executed++)
            {
                if (StoreAsStrings)
                {
                    string value = _executed.ToString();
                    //string value = guid.ToString();
                    if (_guidsStrings.Contains(value) == false)
                    {
                        _guidsStrings.Add(value);
                    }
                }
                else
                {
                    Guid guid = Guid.NewGuid();
                    if (_guids.Contains(guid) == false)
                    {
                        _guids.Add(guid);
                    }
                }
            }

            return true;
        }
    }
}
