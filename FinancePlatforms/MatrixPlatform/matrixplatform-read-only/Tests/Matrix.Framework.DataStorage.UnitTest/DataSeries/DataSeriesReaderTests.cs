// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Matrix.Framework.DataStorage.DataSeries;
using NUnit.Framework;

namespace Matrix.Framework.DataStorage.UnitTest.DataSeries
{
    /// <summary>
    /// Test the data series reader.
    /// </summary>
    [TestFixture]
    public class DataSeriesReaderTests
    {
        DataSeriesReader _reader = null;

        Random _random = new Random();

        /// <summary>
        /// 
        /// </summary>
        public DataSeriesReaderTests()
        {
        }

        [TestFixtureSetUp]
        public void Init()
        {
            _reader = new DataSeriesReader();
            _reader.Initialize(DataSeriesWriterTest.CreateSerializer(), DataSeriesWriterTest.FilePath);
        }

        [TestFixtureTearDown]
        public void UnInit()
        {
            _reader.Dispose();
        }

        /// <summary>
        /// Verifies.
        /// </summary>
        /// <param name="iterations"></param>
        [Test]
        public void GetItemOffset([Values(25, 100, 250, 1000, 5000)] int iterations)
        {
            object item = _reader.ReadLast(0);
            for (int i = 0; i < iterations; i++)
            {
                int seekPos = _random.Next((int)item);
                long? position = _reader.GetIndexOffset(seekPos);
                
                // Cause exception if no value.
                if (position.HasValue == false)
                {
                    throw new Exception(string.Format("Failed to retrieve positions info for [{0}].", seekPos));
                }

                long dummy = position.Value;
                List<object> itemRead = _reader.Read<object>(ref dummy, 1, false);

                // Verify we have correctly established the position.
                if ((int)itemRead[0] != seekPos)
                {
                    throw new Exception(string.Format("Retrieved invalid position info for [{0}].", seekPos));
                }
            }
        }

        /// <summary>
        /// Verifies.
        /// </summary>
        [Test]
        public void Read([Values(1000, 5000, 10000, 50000)] int maxItemsThousands)
        {
            long offset = 0;
            List<object> items = _reader.Read<object>(ref offset, maxItemsThousands * 1000, false);
            for (int i = 0; i < items.Count; i++)
            {
                if (i != (int)items[i])
                {
                    throw new Exception("Item read consecutivity error.");
                }
            }
        }

        /// <summary>
        /// Verifies.
        /// </summary>
        /// <param name="countPerRead"></param>
        [Test]
        public void ReadAndContinue([Values(1, 10, 10000)] int countPerReadThousands)
        {
            long offset = 0;

            List<object> items;
            int itemsRead = 0;
            
            do
            {
                items = _reader.Read<object>(ref offset, countPerReadThousands * 1000, false);
                
                for (int i = 0; i < items.Count; i++)
                {
                    if ((int)items[i] != i + itemsRead)
                    {
                        throw new Exception("Item reading consecutivity error.");
                    }
                }

                itemsRead += items.Count;
            }
            while (items != null && items.Count > 0);

        }

        /// <summary>
        /// Verifies.
        /// </summary>
        /// <param name="countPerPage"></param>
        [Test]
        public void ReadPaged([Values(1, 100)]int countPerPageThousands)
        {
            int itemIndex = 0;
            long offset = 0;
            DataSeriesReader.PagedReadDelegate delegateInstance = delegate(DataSeriesReader reader, List<object> data)
                                                                      {
                                                                          for (int i = 0; i < data.Count; i++)
                                                                          {
                                                                              if ((int)data[i] != itemIndex)
                                                                              {
                                                                                  throw new Exception("Item reading consecutivity error.");
                                                                              }

                                                                              itemIndex++;
                                                                          }
                                                                          return true;
                                                                      };
            
            int totalItemsRead = _reader.ReadPaged(ref offset, 1000 * countPerPageThousands, int.MaxValue, false, delegateInstance);
            Console.WriteLine(totalItemsRead.ToString());
        }


    }
}
