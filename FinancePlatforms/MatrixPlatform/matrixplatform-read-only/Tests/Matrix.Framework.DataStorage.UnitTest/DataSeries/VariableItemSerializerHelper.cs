// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.DataStorage.DataSeries;

namespace Matrix.Framework.DataStorage.UnitTest.DataSeries
{
    /// <summary>
    /// Serializer also inserts escape sequences in data, every now and then, to check if they get stored
    /// and restored properly.
    /// </summary>
    public class VariableSerializerHelper : IItemSerializer
    {
        Random _random = new Random();

        #region IItemSerializer Members

        public bool WriteItem(System.IO.BinaryWriter writer, uint index, object item)
        {
            writer.Write(index);

            int itemCount = _random.Next(15) + 1;
            writer.Write((int)itemCount);
            for (int i = 0; i < itemCount; i++)
            {
                writer.Write((int)item);
            }
            
            writer.Write(111);
            if (index % 97 == 0)
            {
                writer.Write(DataSeriesSequenceHelper.DefaultEscapeSequence);
            }
            return true;
        }

        public bool ReadItem(System.IO.BinaryReader reader, uint index, out object item)
        {
            item = (int)reader.ReadUInt32();
            if ((int)item != index)
            {
                throw new Exception("Test failed.");
            }

            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                if (reader.ReadInt32() != index)
                {
                    throw new Exception("Test failed.");
                }
            }

            int oneeleven = reader.ReadInt32();
            if ((int)item % 97 == 0)
            {
                byte[] escape = reader.ReadBytes(DataSeriesSequenceHelper.DefaultEscapeSequence.Length);

                if (DataSeriesSequenceHelper.CompareBuffers(DataSeriesSequenceHelper.DefaultEscapeSequence, DataSeriesSequenceHelper.DefaultEscapeSequence.Length,
                                                            escape, escape.Length) != 0)
                {
                    throw new Exception("Test failed.");
                }
            }
            
            return true;
        }

        #endregion

    }
}
