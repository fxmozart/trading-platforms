// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Windows.Forms;
using System.Net;
using Matrix.Common.Diagnostics;
using Matrix.Common.Sockets.Common;
using Matrix.Common.Sockets.Core;
using Matrix.Common.Core.Serialization;

namespace Matrix.Common.Sockets.BasicClient
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormClient : Form
    {
        IPEndPoint EndPoint
        {
            get
            {
                IPHostEntry entry = Dns.GetHostEntry(/*"jeromev2"*/"localhost");
                if (entry != null && entry.AddressList.Length > 0)
                {
                    return new IPEndPoint(entry.AddressList[0], int.Parse(toolStripTextBoxPort.Text));
                }

                return null;
            }
        }

        SocketMessageClient _messageClient;
        /// <summary>
        /// 
        /// </summary>
        public FormClient()
        {
            InitializeComponent();

        }

        static void Helper_ConnectedEvent(SocketCommunicator helper)
        {
            SystemMonitor.Info("Client connected event raised.");
        }

        static void Helper_DisconnectedEvent(SocketCommunicator helper)
        {
            SystemMonitor.Info("Client disconnected event raised.");
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.tracerControl1.Tracer = SystemMonitor.Tracer;
            toolStripTextBoxPort.Text = SocketMessageServer.DefaultPort.ToString();
        }

        private void toolStripButtonConnect_Click(object sender, EventArgs e)
        {
            if (_messageClient != null)
            {
                MessageBox.Show("Alrady created.");
                return;
            }

            _messageClient = new SocketMessageClient(EndPoint, new BinarySerializer());
            _messageClient.ConnectedEvent += new SocketCommunicator.HelperUpdateDelegate(Helper_ConnectedEvent);
            _messageClient.DisconnectedEvent += new SocketCommunicator.HelperUpdateDelegate(Helper_DisconnectedEvent);

            _messageClient.AutoReconnect = toolStripButtonAutoReconnect.Checked;

            //_messageClient.ConnectAsync(endPoint);
        }

        private void toolStripButtonDisconnect_Click(object sender, EventArgs e)
        {
            _messageClient.DisconnectAsync();
        }

        private void toolStripButtonSend_Click(object sender, EventArgs e)
        {
            _messageClient.SendAsync(new byte[1024 * 1024 * 15], null);
        }

        private void toolStripButtonAutoReconnect_Click(object sender, EventArgs e)
        {
            _messageClient.AutoReconnect = toolStripButtonAutoReconnect.Checked;
        }
    }
}
