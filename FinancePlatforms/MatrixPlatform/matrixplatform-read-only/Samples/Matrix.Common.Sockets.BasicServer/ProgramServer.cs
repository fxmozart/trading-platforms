// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Windows.Forms;

namespace Matrix.Common.Sockets.BasicServer
{
    public class ProgramServer
    {
        static void Main()
        {
            Application.Run(new FormServer());
        }

        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    static void MainConsole()
        //    {
        //        MessageServer messageServer = new MessageServer(new IPEndPoint(IPAddress.Loopback, 6312));
        //        messageServer.ClientConnectedEvent += new MessageServer.ServerClientUpdateDelegate(messageServer_ClientConnectedEvent);
        //        messageServer.ClientDisconnectedEvent += new MessageServer.ServerClientUpdateDelegate(messageServer_ClientDisconnectedEvent);

        //        messageServer.Start();
        //        Console.WriteLine("Server started.");

        //        //messageServer.clie
        //        Console.ReadKey();
        //    }

        //    static void messageServer_ClientConnectedEvent(MessageServer server, AsyncCommunicator client)
        //    {
        //        SystemMonitor.Report("+ Client connected [" + client.Id.ToString() + "]");
        //        client.MessageReceivedEvent += new AsyncCommunicator.MessageUpdateDelegate(client_MessageReceivedEvent);
        //    }

        //    static void messageServer_ClientDisconnectedEvent(MessageServer server, AsyncCommunicator client)
        //    {
        //        SystemMonitor.Report("- Client disconnected [" + client.Id.ToString() + "]");
        //    }

        //    static void client_MessageReceivedEvent(AsyncCommunicator client, object message)
        //    {
        //        SystemMonitor.Report(">> Client [" + client.Id.ToString() + "] received [" + message.ToString() + "]");
        //    }
    }
}
