// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Net;
using System.Windows.Forms;
using Matrix.Common.Diagnostics;
using Matrix.Common.Diagnostics.TracerCore;
using Matrix.Framework.MessageBus.Clients.ExecutionStrategies;
using Matrix.Framework.MessageBus.Net;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.SuperPool.Core;

namespace Matrix.Framework.SuperPool.Demonstration
{
    public partial class FormClient : Form, ICommunicationInterface
    {
        public string ClientName { get; set; }

        Matrix.Framework.SuperPool.Core.SuperPool _pool;
        SuperPoolClient _poolClient;

        /// <summary>
        /// Constructor.
        /// </summary>
        public FormClient()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Text += " - " + this.ClientName;

            // The steps here are exactly the same, as in server side, only difference is the ClientMessageBus 
            // instead of ServerMessageBus. Since this is the only difference, all the remaining source code
            // is completely independent of whether its a server or a client side.

            // Assign the default tracer, to provide system wide tracing functionality.
            SystemMonitor.AssignTracer(new Tracer());
            this.tracerControl1.Tracer = SystemMonitor.Tracer;

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Loopback, ServerMessageBus.DefaultPort);

            // Create the underlying (client) message bus, that takes care of transporting the 
            // actual communication messages; the message bus TCP communication is created
            // at default port, at localhost.
            ClientMessageBus messageBus = new ClientMessageBus(endPoint, this.ClientName, null);

            // Initialize the super pool with this message bus.
            _pool = new Matrix.Framework.SuperPool.Core.SuperPool(messageBus);

            // Create the client that will server as a connection between this
            // class and the super pool and add the client to the pool.
            _poolClient = new SuperPoolClient("Client." + this.ClientName, this);
            _pool.AddClient(_poolClient);

            // Use this to assign a specific execution strategy to a given client.
            // _poolClient.SetupExecutionStrategy(new FrameworkThreadPoolExecutionStrategy());
        }

        /// <summary>
        /// Helper method.
        /// </summary>
        /// <param name="message"></param>
        void Report(string message)
        {
            textBoxReport.AppendText(message + Environment.NewLine);
        }

        #region ICommunicationInterface Members

        public event HelperDelegate EventOne;

        public string DoWork(string parameter1)
        {
            // It is safe to directly access the UI elements here; since we are inside a [Win.Forms.Control] class child,
            // the super pool will automatically execute the calls we receive on the UI thread. This default behaviour 
            // is controllable trough the MessageSuperPoolClient.AutoControlInvoke flag, and can be disabled.
            Report("Doing work [" + parameter1 + "].");
            return string.Format("Client [{0}] did some work.", this.ClientName);
        }

        #endregion

        private void toolStripButtonRaiseEvent_Click(object sender, EventArgs e)
        {
            HelperDelegate del = EventOne;
            if (del != null)
            {
                del("Raise param");
                Report("Raised [EventOne].");
            }
        }

        private void toolStripButtonCall_Click(object sender, EventArgs e)
        {
            Report("Sending work to server ...");
            
            // This will send a shout call to all those visible on the current super pool, that implement the interface.
            // Since we are single client on the local super pool, and connected to the server super pool, the call
            // will be sent to the server.
            _poolClient.CallAll<ICommunicationInterface>().DoWork(string.Format("Client[{0}] sends work [{1}].", this.ClientName, toolStripTextBox1.Text));
        }
    }
}
