// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.SuperPool.Core;

namespace Matrix.Framework.SuperPool.Demonstration
{
    /// <summary>
    /// Helper delegate with one string parameter.
    /// </summary>
    /// <param name="parameter1"></param>
    public delegate void HelperDelegate(string parameter1);

    /// <summary>
    /// This interface defines how the communication is done.
    /// We have defined one event and one method, to test them both.
    /// 
    /// This can be replaced with any interface of your choice... 
    /// to define how the communication between any 2 super pool
    /// components will look like. A single component can have
    /// as many interfaces as needed.
    /// </summary>
    [SuperPoolInterface]
    public interface ICommunicationInterface
    {
        event HelperDelegate EventOne;
        string DoWork(string parameter1);
    }
}
