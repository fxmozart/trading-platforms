// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using Matrix.Common.Core;
using Matrix.Common.Extended.ThreadPools;

namespace Matrix.Common.Extended
{
    /// <summary>
    /// A general helper class, contains all kinds of routines that help the general operation of an application.
    /// </summary>
    public class GeneralHelper : CommonHelper
    {
        /// <summary>
        /// Could use the default thread pool as well, but that does have a ~0.5sec delay when starting.
        /// </summary>
        static ThreadPoolFastEx _threadPoolEx = new ThreadPoolFastEx(typeof(GeneralHelper).Name);

        /// <summary>
        /// Explicit static constructor to tell C# compiler
        /// not to mark type as BeforeFieldInit. Required for 
        /// thread safety of static elements.
        /// </summary>
        static GeneralHelper()
        {
            _threadPoolEx.MaximumThreadsCount = 16;
            //_threadPoolEx.MaximumSimultaniouslyRunningThreadsAllowed = 12;
        }

        /// <summary>
        /// Helper.
        /// </summary>
        public static double TicksToMilliseconds(long ticks)
        {
            return ConvertTicksToMilliseconds(ticks);
        }

        /// <summary>
        /// Helper.
        /// </summary>
        public static double ConvertTicksToMilliseconds(long ticks)
        {
            return ((double)ticks / (double)Stopwatch.Frequency) * 1000;
        }


        #region Async Helper

        /// <summary>
        /// Redefine needed so that we can construct and pass anonymous delegates with parater.
        /// No need to specify the Value parameter(s), since they can be recognized automatically by compiler.
        /// </summary>
        public static void FireAndForget<Value>(GeneralHelper.GenericDelegate<Value> d, params object[] args)
        {
            _threadPoolEx.Queue(d, args);
        }

        /// <summary>
        /// Redefine needed so that we can construct and pass anonymous delegates with parater.
        /// No need to specify the Value parameter(s), since they can be recognized automatically by compiler.
        /// </summary>
        public static void FireAndForget<ValueType1, ValueType2>(GeneralHelper.GenericDelegate<ValueType1, ValueType2> d, params object[] args)
        {
            _threadPoolEx.Queue(d, args);
        }

        /// <summary>
        /// Redefine needed so that we can construct and pass anonymous delegates with parater.
        /// No need to specify the Value parameter(s), since they can be recognized automatically by compiler.
        /// </summary>
        public static void FireAndForget<ValueType1, ValueType2, ValueType3>(GenericDelegate<ValueType1, ValueType2, ValueType3> d, params object[] args)
        {
            _threadPoolEx.Queue(d, args);
        }

        /// <summary>
        /// Helper, fire and forget an execition on the delegate.
        /// </summary>
        public static void FireAndForget(Delegate d, params object[] args)
        {
            _threadPoolEx.Queue(d, args);
        }

        /// <summary>
        /// A fire and forget for a default delegate.
        /// </summary>
        /// <param name="theDelegate"></param>
        public static void FireAndForget(DefaultDelegate theDelegate)
        {
            FireAndForget(theDelegate, null);
        }

        #endregion

    }
}
