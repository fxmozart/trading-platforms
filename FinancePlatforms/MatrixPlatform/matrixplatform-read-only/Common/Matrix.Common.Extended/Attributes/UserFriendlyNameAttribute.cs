// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Text;

namespace Matrix.Common.Extended.Attributes
{
    /// <summary>
    /// Attribute allows to mark classes with extended user friendly names.
    /// </summary>
    public class UserFriendlyNameAttribute : Attribute
    {
        string _name;
        public string Name
        {
            get { return _name; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public UserFriendlyNameAttribute(string name)
        {
            _name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        static public string GetTypeAttributeName(Type classType)
        {
            string name = GeneralHelper.SeparateCapitalLetters(classType.Name);
            GetTypeAttributeValue(classType, ref name);
            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        static public bool GetTypeAttributeValue(Type classType, ref string name)
        {
            object[] attributes = classType.GetCustomAttributes(typeof(UserFriendlyNameAttribute), false);
            if (attributes != null && attributes.Length > 0)
            {
                name = ((UserFriendlyNameAttribute)attributes[0]).Name;
                return true;
            }
            return false;
        }
    }
}
