// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Common.Extended
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public interface IPersistentEx
//    {
//        /// <summary>
//        /// In the entire class graph / hierarchy that is being saved, those IDs must be unique.
//        /// </summary>
//        string PersistencyId
//        {
//            get;
//        }

//        /// <summary>
//        /// Has the consistency initialization been done.
//        /// </summary>
//        bool PersistencyIsInitialzed
//        {
//            get;
//        }

//        void InitializePersistency(IPersistentManager manager);
        
//        // Receiver to return true to confirm it has modified the data and want's it apply, false to deny this operation and keep data untouched.
//        bool OnSaveState(IPersistentManager manager, PersistentData data);

//        // Receiver to return true to confirm it has modified the data and want's it apply, false to deny this operation and keep data untouched.
//        bool OnRestoreState(IPersistentManager manager, PersistentData data);
//    }
//}
