// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Common.Extended
//{
//    /// <summary>
//    /// Use the IPersistent directly, or this class provided for ease of use.
//    /// </summary>
//    public abstract class PersistentEx : IPersistentEx
//    {
//        IPersistentManager _manager;

//        protected IPersistentManager PersistencyManager
//        {
//            get { lock (this) { return _manager; } }
//        }

//        public bool PersistencyIsInitialzed
//        {
//            get { lock (this) { return _manager != null; } }
//        }

//        public abstract string PersistencyId
//        {
//            get;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public PersistentEx()
//        {
//        }

//        public abstract bool OnSaveState(IPersistentManager manager, PersistentData data);
//        public abstract bool OnRestoreState(IPersistentManager manager, PersistentData data);

//        /// <summary>
//        /// Delegate allows the Consistent to notify parent it requires an update.
//        /// </summary>
//        /// <param name="delegateInstance"></param>
//        public void InitializePersistency(IPersistentManager manager)
//        {
//            _manager = manager;
//        }

//        public void SaveToManager()
//        {
//            lock (this)
//            {
//                System.Diagnostics.Debug.Assert(_manager != null);
//                if (_manager != null)
//                {
//                    _manager.SaveObjectState(this);
//                }
//            }
//        }

//        public void RestoreFromManager()
//        {
//            lock (this)
//            {
//                System.Diagnostics.Debug.Assert(_manager != null);
//                if (_manager != null)
//                {
//                    _manager.RestoreObjectState(this);
//                }
//            }
//        }

//    }
//}
