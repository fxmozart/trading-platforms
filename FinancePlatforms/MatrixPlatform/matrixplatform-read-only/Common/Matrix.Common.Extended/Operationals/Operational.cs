// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Runtime.Serialization;

#if Matrix_Diagnostics
using Matrix.Common.Diagnostics;
#endif

namespace Matrix.Common.Extended.Operationals
{
    /// <summary>
    /// Gives some common predefined code for the implementation of the IOperational interface.
    /// </summary>
    [CLSCompliant(false)]
    [Serializable]
    public class Operational : IOperational, IDeserializationCallback
    {
        protected volatile OperationalStateEnum _operationalState = OperationalStateEnum.UnInitialized;
        
        /// <summary>
        /// The current operational state of the object.
        /// </summary>
        public OperationalStateEnum OperationalState
        {
            get { return _operationalState; }
        }

        volatile bool _statusSynchronizationEnabled = true;
        /// <summary>
        /// Is the status synchronization enabled.
        /// </summary>
        protected bool StatusSynchronizationEnabled
        {
            get { return _statusSynchronizationEnabled; }
            set { _statusSynchronizationEnabled = value; }
        }

        volatile IOperational _statusSynchronizationSource = null;
        
        /// <summary>
        /// If the current unit must synchronize status with a given source, set this variable
        /// and the synchronization is done automatically.
        /// </summary>
        protected IOperational StatusSynchronizationSource
        {
            get { return _statusSynchronizationSource; }
            set 
            {
                if (_statusSynchronizationSource != null)
                {
                    _statusSynchronizationSource.OperationalStateChangedEvent -= new OperationalStateChangedDelegate(_statusSynchronizationSource_OperationalStatusChangedEvent);
                    _statusSynchronizationSource = null;
                }

                _statusSynchronizationSource = value;

                if (_statusSynchronizationSource != null)
                {
                    _statusSynchronizationSource.OperationalStateChangedEvent += new OperationalStateChangedDelegate(_statusSynchronizationSource_OperationalStatusChangedEvent);

                    if (_operationalState != _statusSynchronizationSource.OperationalState)
                    {
                        ChangeOperationalState(_statusSynchronizationSource.OperationalState);
                    }
                }
            }
        }

        [field: NonSerialized]
        public event OperationalStateChangedDelegate OperationalStateChangedEvent;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Operational()
        {
        }

        public virtual void OnDeserialization(object sender)
        {
            StatusSynchronizationSource = _statusSynchronizationSource;
        }

        /// <summary>
        /// Change the component operational state.
        /// </summary>
        /// <param name="operationalState"></param>
        protected virtual void ChangeOperationalState(OperationalStateEnum operationalState)
        {
            OperationalStateEnum previousState;
            lock (this)
            {
                if (operationalState == _operationalState)
                {
                    return;
                }

                previousState = _operationalState;
            }

#if Matrix_Diagnostics
            SystemMonitor.Debug(this.GetType().Name + " was " + previousState.ToString() + " is now " + operationalState.ToString());
#endif

            _operationalState = operationalState;
            if (OperationalStateChangedEvent != null)
            {
                OperationalStateChangedEvent(this, previousState);
            }
        }

        /// <summary>
        /// Follow synchrnozation source status.
        /// </summary>
        void _statusSynchronizationSource_OperationalStatusChangedEvent(IOperational parameter1, OperationalStateEnum parameter2)
        {
            if (StatusSynchronizationEnabled)
            {
#if Matrix_Diagnostics
                SystemMonitor.Debug(this.GetType().Name + " is following its source " + parameter1.GetType().Name + " to state " + parameter1.OperationalState.ToString());
#endif
                this.ChangeOperationalState(parameter1.OperationalState);
            }
            else
            {
#if Matrix_Diagnostics
                SystemMonitor.Debug(this.GetType().Name + " is not following its source " + parameter1.GetType().Name + " to new state because synchronization is disabled.");
#endif
            }
        }

        /// <summary>
        /// Raise event helper.
        /// </summary>
        /// <param name="previousState"></param>
        protected void RaiseOperationalStatusChangedEvent(OperationalStateEnum previousState)
        {
            if (OperationalStateChangedEvent != null)
            {
                OperationalStateChangedEvent(this, previousState);
            }
        }

        /// <summary>
        /// Helper.
        /// </summary>
        public static bool IsInitOrOperational(OperationalStateEnum state)
        {
            return state == OperationalStateEnum.Initialized || state == OperationalStateEnum.Operational;
        }

    }
}
