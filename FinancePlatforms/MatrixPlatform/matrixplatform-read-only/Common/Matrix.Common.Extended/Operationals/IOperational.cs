// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Common.Extended.Operationals
{
    /// <summary>
    /// Enum with states an object can be in. Only one state at a given moment is typically allowed.
    /// Not every object goes trough all the stages, it is up to itself to determine what state and when to be in.
    /// </summary>
    public enum OperationalStateEnum
    {
        Unknown, // [Reccommendation] State of the item now known.
        Constructed, // [Reccommendation] Object was constructed.
        Initializing, // [Reccommendation] Object is initializing (maybe waiting for additional data).
        Initialized, // [Reccommendation] Object is initialized.
        Operational, // [Reccommendation] Object is ready for operation.
        NotOperational, // [Reccommendation] Object is not ready for operation.
        UnInitialized, // [Reccommendation] Object was uninitialized.
        Disposed // [Reccommendation] Object was disposed.
    }

    /// <summary>
    /// IOperational related delegate.
    /// </summary>
    public delegate void OperationalStateChangedDelegate(IOperational operational, OperationalStateEnum previousOperationState);

    /// <summary>
    /// Interface defines an object that has operational and non operation states.
    /// </summary>
    public interface IOperational
    {
        /// <summary>
        /// The current state of the object.
        /// </summary>
        OperationalStateEnum OperationalState { get; }

        /// <summary>
        /// Raised when operator changes state, the second parameter is the *previous operational* state.
        /// </summary>
        event OperationalStateChangedDelegate OperationalStateChangedEvent;
    }
}
