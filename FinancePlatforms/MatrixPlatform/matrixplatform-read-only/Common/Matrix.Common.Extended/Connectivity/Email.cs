// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Matrix.Common.Extended.Connectivity
{
    /// <summary>
    /// Email helper class.
    /// </summary>
    public static class Email
    {
        /// <summary>
        /// Send general email, fully configurable.
        /// </summary>
        /// <returns></returns>
        public static bool SendEmailAsync(string username, string password, string smtpServer, int port, string from, string to,
                                          string subject, string body, SendCompletedEventHandler handler)
        {
            SmtpClient client = new SmtpClient(smtpServer, port);
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential(username, password);

            MailMessage message = new MailMessage(from, to);
            message.Body = body;
            message.Subject = subject;

            try
            {
                client.SendAsync(message, null);
                if (handler != null)
                {
                    client.SendCompleted += handler;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Send email trough GMAIL SSL.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="handler"></param>
        public static bool SendGmailSSLAsync(string username, string password, string from, string to, 
                                             string subject, string body, SendCompletedEventHandler handler)
        {
            return SendEmailAsync(username, password, "smtp.gmail.com", 587, from, to, subject, body, handler);
        }

    }
}
