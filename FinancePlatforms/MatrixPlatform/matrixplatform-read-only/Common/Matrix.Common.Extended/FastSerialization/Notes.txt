﻿Classes in this folder are from this article:
http://www.codeproject.com/KB/dotnet/FastSerializer.aspx

The purpose is to assist in the process of binary serialization, 
and make it significantly faster and smaller (up to 300 times in 7-8times in size).

Also the optional miniLZO compression module is GPL Licensed.