// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using Matrix.Common.Core;

namespace Matrix.Common.Extended.ThreadPools
{
    /// <summary>
    /// Class extends tha custom fast thread pool with a few easy to use features 
    /// (yet they are slow, so use with cautioun).
    /// </summary>
    public class ThreadPoolFastEx : ThreadPoolFast
    {
        static List<Type> OwnerTypes = new List<Type>(new Type[] { typeof(ThreadPoolFastEx) });

        //Dictionary<MethodInfo, FastInvokeHelper.FastInvokeHandlerDelegate> _methodDelegates = new Dictionary<MethodInfo, FastInvokeHelper.FastInvokeHandlerDelegate>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Name of this thread pool</param>
        public ThreadPoolFastEx(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Helper, obtain the correspoding fast delegate of a method info.
        /// </summary>
        public FastInvokeHelper.FastInvokeHandlerDelegate GetMessageHandler(MethodInfo methodInfo)
        {
            return FastInvokeHelper.GetMethodInvoker(methodInfo, true, true);
            //lock (_methodDelegates)
            //{
            //    if (_methodDelegates.ContainsKey(methodInfo))
            //    {
            //        return _methodDelegates[methodInfo];
            //    }

            //    FastInvokeHelper.FastInvokeHandlerDelegate resultHandler = FastInvokeHelper.GetMethodInvoker(methodInfo, true, true);
            //    _methodDelegates[methodInfo] = resultHandler;
            //    return resultHandler;
            //}
        }

        /// <summary>
        /// This is dreadfully slow and can overload CPU with only 3000 calls per second!
        /// </summary>
        /// <returns></returns>
        string ObtainCallerName()
        {
            if (Debugger.IsAttached)
            {
                MethodBase method = ReflectionHelper.GetExternalCallingMethod(2, OwnerTypes);
                if (method != null)
                {
                    return method.Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Redefine, matches the .NET thread pool queue, for simple interchange.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="args"></param>
        public void Queue(WaitCallback d, object param)
        {
            Queue((Delegate)d, new object[] { param });
        }

        /// <summary>
        /// Add a new delegate call to be executed.
        /// This call gives a performance of approx 300 - 400 K calls per second, so it is not very fast.
        /// For more speed, use the QueueFastDelegate and the QueueTargetInfo methods (1.5-3.5 Mil on a Dual Core).
        /// </summary>
        public void Queue(Delegate d, params object[] args)
        {
            if (d == null)
            {
                return;
            }

            // This is 3000 calls per seconds SLOW.
            string callerName = ObtainCallerName();

            // The "d.Method" call is AWFULLY SLOW.
            QueueTargetInfo(new TargetInfo(callerName, d.Target, GetMessageHandler(d.Method), false, this, args));
        }



    }
}
