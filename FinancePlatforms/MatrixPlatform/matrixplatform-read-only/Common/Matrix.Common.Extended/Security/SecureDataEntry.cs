// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security;

namespace Matrix.Common.Extended.Security
{
    /// <summary>
    /// Class stores sensitive information securely.
    /// Works in cooperation with SecureDataManager, since the master password
    /// is only stored once in the manager.
    /// Currently supports working with strings.
    /// </summary>
    [Serializable]
    public class SecureDataEntry
    {
        SecureDataManager _manager;

        Dictionary<string, string> _fields = new Dictionary<string, string>();
        /// <summary>
        /// This is for internal access only, use the methods instead.
        /// </summary>
        internal Dictionary<string, string> FieldsUnsafe
        {
            get { return _fields; }
        }

        /// <summary>
        /// Entries only constructed by manager.
        /// </summary>
        internal SecureDataEntry(SecureDataManager manager)
        {
            _manager = manager;
        }

        /// <summary>
        /// Add a string entry for keeping.
        /// </summary>
        public bool SetField(string entryName, SecureString entryValue)
        {
            string ecryptedData;
            if (_manager.Encrypt(entryValue, out ecryptedData) == false)
            {
                return false;
            }

            lock (this)
            {
                _fields[entryName] = ecryptedData;
            }

            return true;
        }

        /// <summary>
        /// Get value of a field.
        /// </summary>
        public SecureString GetField(string entryName)
        {
            string entryArray;
            lock (this)
            {
                if (_fields.ContainsKey(entryName) == false)
                {
                    return null;
                }
                entryArray = _fields[entryName];
            }
            
            SecureString result;
            if (_manager.Decrypt(entryArray, out result) == false)
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// Remote field.
        /// </summary>
        /// <param name="entryName"></param>
        /// <returns></returns>
        public bool RemoveField(string entryName)
        {
            lock (this)
            {
                return _fields.Remove(entryName);
            }
        }

        /// <summary>
        /// Clear all fields in entry.
        /// </summary>
        public void Clear()
        {
            lock (this)
            {
                _fields.Clear();
            }
        }

    }
}
