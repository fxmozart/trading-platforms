// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Matrix.Common.FrontEnd.UI.Controls
{
    public partial class BrushControl : UserControl
    {
        Brush _brush;
        public Brush Brush
        {
            get 
            { 
                return _brush; 
            }

            set 
            {
                _brush = value;
                UpdateUI();
            }
        }

        string _brushName;
        public string BrushName
        {
            get 
            { 
                return _brushName;
            }

            set 
            { 
                _brushName = value;
                UpdateUI();
            }
        }

        bool _readOnly = false;

        public bool ReadOnly
        {
            get 
            { 
                return _readOnly; 
            }

            set 
            { 
                _readOnly = value;
                UpdateUI();
            }
        }

        public delegate void BrushChangedDelegate(BrushControl control);
        public event BrushChangedDelegate BrushChangedEvent;

        /// <summary>
        /// 
        /// </summary>
        public BrushControl()
        {
            InitializeComponent();
        }

        private void BrushControl_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (_brush != null)
            {
                e.Graphics.FillRectangle(_brush, new RectangleF(labelColor.Location, labelColor.Size));
            }
        }

        /// <summary>
        /// Update user interface based on the underlying information.
        /// </summary>
        void UpdateUI()
        {
            labelName.Text = _brushName;
        }

        private void labelColor_Click(object sender, EventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }

            ColorDialog dialog = new ColorDialog();
            if (_brush is SolidBrush)
            {
                dialog.Color = ((SolidBrush)_brush).Color;
            }
            else if (_brush is LinearGradientBrush)
            {
                dialog.Color = ((LinearGradientBrush)_brush).LinearColors[0];
            }

            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                _brush = new SolidBrush(dialog.Color);
            }

            UpdateUI();

            if (BrushChangedEvent != null)
            {
                BrushChangedEvent(this);
            }

            this.Refresh();
        }

        private void comboBoxDash_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BrushChangedEvent != null)
            {
                BrushChangedEvent(this);
            }
        }

        private void labelEmpty_Click(object sender, EventArgs e)
        {
            Brush = null;
            if (BrushChangedEvent != null)
            {
                BrushChangedEvent(this);
            }
            this.Refresh();
        }
    }
}
