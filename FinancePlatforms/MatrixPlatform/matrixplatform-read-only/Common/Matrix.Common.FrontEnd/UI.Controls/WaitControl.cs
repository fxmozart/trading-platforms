// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Matrix.Common.FrontEnd.UI.Controls
{
    /// <summary>
    /// Control used to hide underlying controls on a parent control, when an operation that stops access to them is needed.
    /// </summary>
    public partial class WaitControl : UserControl
    {
        /// <summary>
        /// Invocation safety.
        /// </summary>
        public string Message
        {
            get { return this.labelMain.Text; }
            set 
            {
                if (this.InvokeRequired == false)
                {
                    labelMain.Text = value;
                }
                else
                {
                    WinFormsHelper.BeginManagedInvoke(this, delegate() { labelMain.Text = value; });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Active
        {
            get
            {
                return this.Visible;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public WaitControl()
        {
            InitializeComponent();
            this.Visible = false;
        }

        /// <summary>
        /// Invocation safety.
        /// </summary>
        /// <param name="activate"></param>
        public void SetActiveState(bool activate)
        {
            if (this.InvokeRequired == false)
            {
                if (this.IsHandleCreated == false)
                {
                    this.CreateControl();
                }

                this.Visible = true;
                DoActivate(activate);
            }
            else
            {
                WinFormsHelper.BeginManagedInvoke(this, delegate()
                                                            {
                                                                if (this.IsHandleCreated == false)
                                                                {
                                                                    this.CreateControl();
                                                                }

                                                                DoActivate(activate);
                                                            });
            }
        }

        /// <summary>
        /// Helper.
        /// </summary>
        /// <param name="activate"></param>
        void DoActivate(bool activate)
        {
            if (activate)
            {
                this.Left = 0;
                this.Top = 0;
                this.Width = Parent.Width;
                this.Height = Parent.Height;
                this.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

                this.BringToFront();
                this.Show();
            }
            else
            {
                this.Hide();
            }
        }

        private void timerUI_Tick(object sender, EventArgs e)
        {
            pictureBox1.Visible = !pictureBox1.Visible;
        }

        private void labelMain_VisibleChanged(object sender, EventArgs e)
        {
            timerUI.Enabled = this.Visible;
        }
    }
}
