// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Matrix.Common.FrontEnd.UI.Controls
{
    public partial class PenControl : UserControl
    {
        Pen _pen;
        public Pen Pen
        {
            get 
            { 
                return _pen; 
            }

            set 
            { 
                _pen = (Pen)value.Clone();
                UpdateUI();
            }
        }

        string _penName;
        public string PenName
        {
            get 
            { 
                return _penName;
            }

            set 
            { 
                _penName = value;
                UpdateUI();
            }
        }

        bool _readOnly = false;

        public bool ReadOnly
        {
            get 
            { 
                return _readOnly; 
            }

            set 
            { 
                _readOnly = value;
                UpdateUI();
            }
        }

        public delegate void PenChangedDelegate(PenControl control);
        public event PenChangedDelegate PenChangedEvent;

        /// <summary>
        /// 
        /// </summary>
        public PenControl()
        {
            InitializeComponent();
            comboBoxDash.Items.AddRange(Enum.GetNames(typeof(System.Drawing.Drawing2D.DashStyle)));
        }

        private void PenControl_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Update user interface based on the underlying information.
        /// </summary>
        void UpdateUI()
        {
            if (_pen == null)
            {
                labelName.Text = "";
                comboBoxDash.SelectedIndex = -1;
                labelColor.BackColor = this.BackColor;
                return;
            }

            labelColor.BackColor = _pen.Color;
            labelName.Text = _penName;
            comboBoxDash.Enabled = !ReadOnly;
            comboBoxDash.SelectedIndex = (int)_pen.DashStyle;
        }

        private void labelColor_Click(object sender, EventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }

            ColorDialog dialog = new ColorDialog();
            dialog.Color = _pen.Color;
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                _pen.Color = dialog.Color;
            }

            UpdateUI();

            if (PenChangedEvent != null)
            {
                PenChangedEvent(this);
            }
        }

        private void comboBoxDash_SelectedIndexChanged(object sender, EventArgs e)
        {
            _pen.DashStyle = (System.Drawing.Drawing2D.DashStyle)Enum.Parse(typeof(System.Drawing.Drawing2D.DashStyle), comboBoxDash.SelectedItem.ToString());

            if (PenChangedEvent != null)
            {
                PenChangedEvent(this);
            }
        }
    }
}
