// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Drawing;
using System.Runtime.Serialization;
using Matrix.Common.Core.Serialization;

namespace Matrix.Common.FrontEnd.Serialization
{
    /// <summary>
    /// 
    /// </summary>
    [SerializationSurrogate(typeof(Pen))]
    public class PenSurrogate : ISerializationSurrogate
    {
        #region ISerializationSurrogate Members

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Pen pen = (Pen)obj;

            UISerializationHelper.ManualSerialize("color", pen.Color, info);
            info.AddValue("dashStyle", (int)pen.DashStyle);
            // Can not serialize - throws "OutOfMemoryException"
            //info.AddValue("dashPattern", pen.DashPattern);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Color color;
            UISerializationHelper.ManualDeSerialize(info, "color", out color);

            Pen pen = new Pen(color);
            pen.DashStyle = (System.Drawing.Drawing2D.DashStyle)info.GetInt32("dashStyle");
            //pen.DashPattern = (float[])info.GetValue("dashPattern", typeof(float[]));
            return pen;
        }

        #endregion


    }
}
