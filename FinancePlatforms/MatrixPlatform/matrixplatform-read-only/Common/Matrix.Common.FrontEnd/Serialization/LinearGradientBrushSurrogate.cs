// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.Serialization;
using Matrix.Common.Core.Serialization;

namespace Matrix.Common.FrontEnd.Serialization
{
    /// <summary>
    /// 
    /// </summary>
    [SerializationSurrogate(typeof(LinearGradientBrush))]
    public class LinearGradientBrushSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            LinearGradientBrush brush = (LinearGradientBrush)obj;
            UISerializationHelper.ManualSerialize("linearColor0", brush.LinearColors[0], info);
            UISerializationHelper.ManualSerialize("linearColor1", brush.LinearColors[1], info);

            info.AddValue("blend", brush.Blend);

            //info.AddValue("interpolationColors", brush.InterpolationColors);
            info.AddValue("transform", brush.Transform);
            info.AddValue("rectangle", brush.Rectangle);
            info.AddValue("wrapMode", (int)brush.WrapMode);
            info.AddValue("gammaCorrection", brush.GammaCorrection);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            RectangleF rectangle = (RectangleF)info.GetValue("rectangle", typeof(RectangleF));

            Color color1, color2;
            UISerializationHelper.ManualDeSerialize(info, "linearColor0", out color1);
            UISerializationHelper.ManualDeSerialize(info, "linearColor1", out color2);

            LinearGradientBrush brush = new LinearGradientBrush(rectangle, color1, color2, LinearGradientMode.ForwardDiagonal);
            brush.WrapMode = (WrapMode)info.GetInt32("wrapMode");
            brush.GammaCorrection = info.GetBoolean("gammaCorrection");

            //brush.InterpolationColors = (ColorBlend)info.GetValue("interpolationColors", typeof(ColorBlend));
            brush.Blend = (Blend)info.GetValue("blend", typeof(Blend));
            brush.Transform = (System.Drawing.Drawing2D.Matrix)info.GetValue("transform", typeof(System.Drawing.Drawing2D.Matrix));
            return brush;
        }
    }
}
