// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Matrix.Common.FrontEnd.GraphicSpace
{
    public class SpaceCamera
    {
        public System.Drawing.Drawing2D.Matrix TransformationMatrix
        {
            get
            {
                System.Drawing.Drawing2D.Matrix matrix = new System.Drawing.Drawing2D.Matrix();
                matrix.Translate(_position.X, _position.Y);
                matrix.Scale(_zoom, _zoom);
                return matrix;
            }
        }

        PointF _position = new PointF(0, 0);
        public PointF Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        float _zoom = 1;
        public float Zoom
        {
            set
            {
                _zoom = value;
            }
        }

        float _width = 0;
        public float Width
        {
            //get
            //{
            //    return _width;
            //}
            set
            {
                _width = value;
            }
        }

        float _height = 0;
        public float Height
        {
            //get
            //{
            //    return _height;
            //}
            set
            {
                _height = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public SpaceCamera()
        {
        }

        /// <summary>
        /// Shows the given point center screen.
        /// </summary>
        /// <param name="point"></param>
        public void ShowPoint(PointF point)
        {
            Position = new PointF(point.X + _width / 2, point.Y + _height / 2);
        }

        public PointF ReverseScalePoint(PointF point)
        {
            point.X = point.X / this._zoom;
            point.Y = point.Y / this._zoom;
            return point;
        }

        /// <summary>
        /// Reverse transforms the point.
        /// </summary>
        public PointF ReverseTransformPoint(PointF point)
        {
            System.Drawing.Drawing2D.Matrix matrix = TransformationMatrix.Clone();
            matrix.Invert();
            PointF[] resultPoints = new PointF[] { point };
            matrix.TransformPoints(resultPoints);
            return resultPoints[0];
        }

    }
}
