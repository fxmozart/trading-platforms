// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Drawing.Drawing2D;

namespace Matrix.Common.FrontEnd.GraphicSpace
{
    /// <summary>
    /// Summary description for ObjectItem.
    /// </summary>
    [Serializable]
    public class Stencil
    {
        public Stencil(Space space)
        {
            System.Diagnostics.Debug.Assert(space != null);
            _objectSpace = space;
        }

        public string Name
        {
            get
            {
                if (DocumentTag != null)
                {
                    return "OI[" + DocumentTag.ToString() + "]";
                }
                else
                {
                    return "Object Item";
                }
            }
        }

        protected Object DocumentTag;

        Space _objectSpace;
        public Space Space
        {
            get
            {
                return _objectSpace;
            }
        }

        List<Shape> _shapes = new List<Shape>();
        public List<Shape> Shapes
        {
            get { return _shapes; }
            set { _shapes = value; }
        }

        public System.Drawing.Drawing2D.Matrix TransformationMatrix
        {
            get
            {
                System.Drawing.Drawing2D.Matrix matrix = new System.Drawing.Drawing2D.Matrix();
                matrix.Translate(Position.X, Position.Y);
                return matrix;
            }
        }

        private PointF _position = new PointF();
        public PointF Position
        {
            get { return _position; }
            set { _position = value; }
        }

        bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        public bool IsMouseOver
        {
            get
            {
                foreach (Shape shape in Shapes)
                {
                    if (shape.IsMouseOver)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Update the shapes mouse in and mouse out.
        /// </summary>
        /// <param name="mousePosition"></param>
        public void SetMousePosition(PointF mousePosition)
        {
            // Convert to stencil coordinates.
            PointF[] points = new PointF[] { mousePosition };
            System.Drawing.Drawing2D.Matrix matrix = this.TransformationMatrix.Clone();
            matrix.Invert();
            matrix.TransformPoints(points);

            foreach (Shape shape in Shapes)
            {
                if (shape.IsPointInside(points[0]) && shape.IsMouseOver == false)
                {
                    shape.MouseEnter();
                }

                if (shape.IsPointInside(points[0]) == false && shape.IsMouseOver == true)
                {
                    shape.MouseLeave();
                }
            }
        }

        public bool IsPointInside(PointF point)
        {
            foreach (Shape shape in Shapes)
            {
                if (shape.IsPointInside(point))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Remove self from object space.
        /// </summary>
        public void Remove()
        {
            if(Space != null)
            {
                Space.RemoveStencil(this);
            }
        }

    }
}
