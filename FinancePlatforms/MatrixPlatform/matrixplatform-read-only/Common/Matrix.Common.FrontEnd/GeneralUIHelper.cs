// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using Matrix.Common.Diagnostics;

namespace Matrix.Common.FrontEnd
{
    /// <summary>
    /// 
    /// </summary>
    public static class GeneralUIHelper
    {
        /// <summary>
        /// This only applies the simples baseMethod of looking for a web site shortcut icon - the "favicon.ico" image.
        /// </summary>
        /// <param name="websiteUri"></param>
        /// <returns></returns>
        public static Image GetWebSiteShortcutIcon(Uri websiteUri)
        {
            Image result = null;
            string host = websiteUri.Host;

            try
            {
                WebRequest requestImg = WebRequest.Create("http://" + websiteUri.Host + "/favicon.ico");
                requestImg.Timeout = 10000;

                WebResponse response = requestImg.GetResponse();
                if (response.ContentLength > 0)
                {
                    result = Image.FromStream(response.GetResponseStream());
                }
            }
            catch (Exception ex)
            {
                SystemMonitor.Error("Failed to get image [" + ex.Message + "]");
                return null;
            }

            //else
            //{
            //    HtmlDocument doc = webBrowser1.Document;
            //    HtmlElementCollection collect = doc.GetElementsByTagName("link");

            //    foreach (HtmlElement element in collect)
            //    {
            //        if (element.GetAttribute("rel").ToLower() == "shortcut icon"
            //        || element.GetAttribute("rel").ToLower() == "icon" || element.GetAttribute("type").ToLower() == "image/x-icon")
            //            iconPath = element.GetAttribute("href");
            //    }

            //    this.Text = doc.Title;

            //    requestImg = WebRequest.Create("http://" + e.Url.Host + iconPath);

            //    response = requestImg.GetResponse();

            //    myStream = response.GetResponseStream();
            //}

            return result;
        }

    }
}
