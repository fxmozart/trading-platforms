// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using Matrix.Common.Core.Collections;

namespace Matrix.Common.FrontEnd.Math
{
    public class SimpleMovingAverage : IMovingAverage
    {
        CircularList<float> samples;
        protected float total;

        /// <summary>
        /// Get the average for the current number of samples.
        /// </summary>
        public float Average
        {
            get
            {
                if (samples.Count == 0)
                {
                    throw new ApplicationException("Number of samples is 0.");
                }

                return total / samples.Count;
            }
        }

        /// <summary>
        /// Constructor, initializing the sample size to the specified number.
        /// </summary>
        public SimpleMovingAverage(int numSamples)
        {
            if (numSamples <= 0)
            {
                throw new ArgumentOutOfRangeException("numSamples can't be negative or 0.");
            }

            samples = new CircularList<float>(numSamples);
            total = 0;
        }

        /// <summary>
        /// Adds a sample to the sample collection.
        /// </summary>
        public void AddSample(float val)
        {
            if (samples.Count == samples.Length)
            {
                total -= samples.Value;
            }

            samples.Value = val;
            total += val;
            samples.Next();
        }

        /// <summary>
        /// Clears all samples to 0.
        /// </summary>
        public void ClearSamples()
        {
            total = 0;
            samples.Clear();
        }

        /// <summary>
        /// Initializes all samples to the specified value.
        /// </summary>
        public void InitializeSamples(float v)
        {
            samples.SetAll(v);
            total = v * samples.Length;
        }
    }
}
