// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Matrix.Common.FrontEnd.UI.Draggable
{
    /// <summary>
    /// Class represents the window strip at the top of any drag control.
    /// </summary>
    public partial class DragStripControl : UserControl
    {
        LinearGradientBrush _activeCaptionBrush = null;
        Point _mousePosition = Point.Empty;

        Point _mouseDown = Point.Empty;

        public delegate void DockDelegate(DragControl control, DockStyle style);
        public event DockDelegate DockPaneEvent;

        public DragControl ParentControl { get { return this.Parent as DragControl; } }

        volatile bool _hasFocus = false;
        public bool HasFocus
        {
            get { return _hasFocus; }
            set { _hasFocus = value; }
        }

        public delegate void UpdateDelegate(DragControl control);
        public event UpdateDelegate CloseEvent;
        public event UpdateDelegate MaximizeEvent;

        /// <summary>
        /// Constructor.
        /// </summary>
        public DragStripControl()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (this.DesignMode)
            {
                return;
            }

            Blend activeBackColorGradientBlend = new Blend(2);
            activeBackColorGradientBlend.Factors = new float[] { 0.5F, 1.0F };
            activeBackColorGradientBlend.Positions = new float[] { 0.0F, 1.0F };

            //_activeCaptionBrush = new LinearGradientBrush(new PointF(0, 0), new PointF(3, 3), SystemColors.ActiveCaption, SystemColors.Control);
            Color ActiveBackColorGradientBegin = SystemColors.GradientActiveCaption;
            Color ActiveBackColorGradientEnd = SystemColors.ActiveCaption;

            _activeCaptionBrush = new LinearGradientBrush(new Rectangle(0,0, 10, this.Height), ActiveBackColorGradientBegin, ActiveBackColorGradientEnd, LinearGradientMode.Vertical);
            _activeCaptionBrush.Blend = activeBackColorGradientBlend;

            OnResize(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            _mouseDown = Point.Empty;
            if (e.X > 20 && e.X < this.Width - 20)
            {
                _mouseDown = e.Location;
            }

            if (this.ParentControl.Dock == DockStyle.None)
            {// Only floating - bring to front on touch.
                this.ParentControl.BringToFront();
            }
        }

        void PerformDragDrop(Point location)
        {
            DragControl parent = (DragControl)this.Parent;
            parent.DraggableLocation = location;

            // Synchronous drag operation.
            int index = this.Parent.GetHashCode();
            //parent.Parent.Controls.IndexOf(parent);
            DoDragDrop(index, DragDropEffects.All);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (_mouseDown != Point.Empty && (e.X - _mouseDown.X != 0 || e.Y - _mouseDown.Y != 0) 
                && e.Button == MouseButtons.Left && Parent.Dock == DockStyle.None)
            {
                PerformDragDrop(e.Location);
            }

            _mousePosition = e.Location;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            _mousePosition = Point.Empty;
            this.Invalidate();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (this.DesignMode)
            {
                return;
            }

            if (_activeCaptionBrush != null)
            {
                //_activeCaptionBrush.ResetTransform();
                //_activeCaptionBrush.ScaleTransform(1.7f * this.Width, this.Height, MatrixOrder.Prepend);
            }

            this.Invalidate();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (e.X < 18)
            {
                contextMenuStripMain.Show(this.PointToScreen(e.Location));
            }

            if (e.X > this.Width - 20)
            {// Close control
                if (CloseEvent != null)
                {
                    CloseEvent(this.Parent as DragControl);
                }
            }
            else if (e.X < this.Width - 20 && e.X > this.Width - 33)
            {// Maximize control.
                if (MaximizeEvent != null)
                {
                    MaximizeEvent(this.Parent as DragControl);
                }
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            if (this.DesignMode)
            {
                return;
            }

            pe.Graphics.FillRectangle(_activeCaptionBrush, new Rectangle(0, 0, this.Width, this.Height));

            int marginLeft = 18;
            bool hasImage = ParentControl.Image != null;
            if (hasImage)
            {
                if (this.Width > 45)
                {
                    pe.Graphics.DrawImage(ParentControl.Image, 18, 1);
                }
                marginLeft = 36;
            }


            if (this.Width > 45)
            {
                RectangleF textLayoutRectangle = new RectangleF(marginLeft, 2, this.Width - marginLeft - 18, this.Height);
                if (_hasFocus)
                {
                    pe.Graphics.DrawString(Parent.Name, SystemFonts.CaptionFont, SystemBrushes.ActiveCaptionText, textLayoutRectangle);
                }
                else
                {
                    pe.Graphics.DrawString(Parent.Name, SystemFonts.CaptionFont, SystemBrushes.InactiveCaptionText, textLayoutRectangle);
                }
            }

            if (_mousePosition == Point.Empty || _mousePosition.X >= 16)
            {
                pe.Graphics.DrawImageUnscaled(Matrix.Common.FrontEnd.Properties.Resources.square, new Point(1, 1));
            }
            else
            {
                pe.Graphics.DrawImageUnscaled(Matrix.Common.FrontEnd.Properties.Resources.square_triangle, new Point(1, 1));
            }

            // Draw the "X" button.
            Rectangle rectangle = new Rectangle(new Point(this.Width - 18, 1), Matrix.Common.FrontEnd.Properties.Resources.DockPane_Close.Size);
            GraphicsHelper.DrawImageColorMapped(pe.Graphics, Properties.Resources.DockPane_Close, rectangle, Color.Black, Color.White);

            if (_mousePosition != Point.Empty && _mousePosition.X > this.Width - 20)
            {
                rectangle.X++;
                rectangle.Width -= 2;
                rectangle.Height -= 2;
                pe.Graphics.DrawRectangle(Pens.White, rectangle);
            }
                 
            // Draw the square dock button.
            if (_mousePosition != Point.Empty && _mousePosition.X > this.Width - 33 && _mousePosition.X < this.Width - 20)
            {// On mouse over.
                rectangle = new Rectangle(new Point(this.Width - 31, 4), Matrix.Common.FrontEnd.Properties.Resources.DockPane_Close.Size);
                rectangle.Width -= 6;
                rectangle.Height -= 6;
                pe.Graphics.FillRectangle(Brushes.White, rectangle);
            }
            else
            {
                rectangle = new Rectangle(new Point(this.Width - 31, 8), Matrix.Common.FrontEnd.Properties.Resources.DockPane_Close.Size);
                rectangle.Width -= 9;
                rectangle.Height -= 10;
                pe.Graphics.FillRectangle(Brushes.White, rectangle);

                rectangle = new Rectangle(new Point(this.Width - 31, 4), Matrix.Common.FrontEnd.Properties.Resources.DockPane_Close.Size);
                rectangle.Width -= 6;
                rectangle.Height -= 6;
                pe.Graphics.DrawRectangle(Pens.White, rectangle);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void topToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.Top);
        }

        private void leftToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.Left);
        }

        private void bottomToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.Bottom);
        }

        private void rightToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.Right);
        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.Fill);
        }

        private void floatingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DockPaneEvent(this.Parent as DragControl, DockStyle.None);
        }

        private void contextMenuStripMain_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            floatingToolStripMenuItem.Enabled = ParentControl.DragContainer.AllowFloating;
        }


    }
}
