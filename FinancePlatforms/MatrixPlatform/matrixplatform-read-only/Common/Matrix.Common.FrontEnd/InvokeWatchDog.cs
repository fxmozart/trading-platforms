// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Matrix.Common.Diagnostics;

namespace Matrix.Common.FrontEnd
{
    /// <summary>
    /// The Invoke watch mechanism looks after blocked "Invoke"s.
    /// </summary>
    internal class InvokeWatchDog
    {
        System.Timers.Timer _invokeWatchTimer = new System.Timers.Timer(250);

        List<WinFormsHelper.AsyncResultInfo> _invokeResults = new List<WinFormsHelper.AsyncResultInfo>();

        /// <summary>
        /// Constructor.
        /// </summary>
        public InvokeWatchDog()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        ~InvokeWatchDog()
        {
            _invokeWatchTimer.Stop();
            _invokeWatchTimer = null;

            // Final warning is useless since the application is closing already and controls do not respond.
            //lock (InvokeResults)
            //{
            //    if (InvokeResults.Count > 0)
            //    {
            //        SystemMonitor.Warning("A total of [" + InvokeResults.Count + "] blocked/incomplete invokes were found.");
            //    }
            //}
        }

        /// <summary>
        /// Start the watchdog operation.
        /// </summary>
        public void Start()
        {
            lock (this)
            {
                _invokeWatchTimer.Elapsed += new System.Timers.ElapsedEventHandler(_invokeWatchTimer_Elapsed);
                _invokeWatchTimer.Start();
            }
        }

        public void Add(WinFormsHelper.AsyncResultInfo info)
        {
            if (_invokeWatchTimer.Enabled)
            {
                lock (this)
                {
                    _invokeResults.Add(info);
                }
            }
        }

        void _invokeWatchTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (this)
            {
                DateTime now = DateTime.Now;
                for (int i = _invokeResults.Count - 1; i >= 0; i--)
                {
                    if (_invokeResults[i].AsyncResult.IsCompleted)
                    {
                        _invokeResults.RemoveAt(i);
                    }
                    else
                        if ((now - _invokeResults[i].PublishTime).TotalSeconds > 10)
                        {
                            SystemMonitor.Warning("Blocked invoke [" + _invokeResults[i].MethodName + "]");
                            _invokeResults.RemoveAt(i);
                        }
                }
            }
        }
    }
}
