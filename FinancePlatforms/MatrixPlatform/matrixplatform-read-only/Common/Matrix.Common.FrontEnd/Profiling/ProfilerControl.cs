// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Matrix.Common.FrontEnd.Profiling
{
    public partial class ProfilerControl : UserControl
    {
        public ProfilerControl()
        {
            InitializeComponent();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
#if DEBUG
            if (Profiling.Profiler.Entries != null)
            {
                this.listBoxMain.Items.Clear();
                string[] names = Enum.GetNames(typeof(Profiling.Profiler.ProfilerEntry));
                for (int i = 0; i < Profiling.Profiler.Entries.Length; i++)
                {
                    string message = names[i] + ": entries [" + Profiling.Profiler.Entries[i] + "] time ["+Profiling.Profiler.Times[i]/10000+"]";
                    this.listBoxMain.Items.Add(message);
                }
            }
#endif
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
#if DEBUG
            if (Profiling.Profiler.Entries != null)
            {
                for (int i = 0; i < Profiling.Profiler.Entries.Length; i++)
                {
                    Profiling.Profiler.Entries[i] = 0;
                    Profiling.Profiler.Times[i] = 0;
                }
            }
#endif

        }


    }
}
