// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Common.FrontEnd.Profiling
{
    /// <summary>
    /// This is a very early version of an external profiling mechanism, communicating to 
    /// profiling app with windows messages; has support for both managed and unmanaged code.
    /// </summary>
    public class ProfilerHelper
    {
        /// <summary>
        /// Send a measurement to profiling app.
        /// </summary>
        public static void SendMeasure()
        {
            ////long freq = 2666700000;
            //long count1/*, count2, count3*/;
            ////CommonSupport.HighPerformanceTimer.QueryPerformanceFrequency(out freq);

            //HighPerformanceTimer.QueryPerformanceCounter(out count1);

            //uint xHigh = (uint)((ulong)count1 / uint.MaxValue);
            //uint xLow = (uint)((ulong)count1 % uint.MaxValue);

            ////if (xLow > int.MaxValue)
            ////{
            ////    //xLow -= int.MaxValue;
            ////}

            ////IntPtr ptr = (IntPtr)(xLow);

            //Win32Helper.SendMessage((IntPtr)3278846, 9997, new IntPtr(xHigh), new IntPtr(xLow));


            ////CommonSupport.HighPerformanceTimer.QueryPerformanceCounter(out count2);
            ////CommonSupport.HighPerformanceTimer.QueryPerformanceCounter(out count3);

            ////double combined = (double)count1 / (double)freq;
            ////Debug.Write("<< " + count1 + " @ " + freq + " : " + combined + Environment.NewLine);

            ////combined = (double)count2 / (double)freq;
            ////Debug.Write("<< " + count2 + " @ " + freq + " : " + combined + Environment.NewLine);

            ////combined = (double)count3 / (double)freq;
            ////Debug.Write("<< " + count3 + " @ " + freq + " : " + combined + Environment.NewLine);
        }
    }
}
