// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Common.FrontEnd.Profiling
{
    public class Profiler
    {
        public enum ProfilerEntry
        {
            MainForm_UpdateUIComponents,
            AManaged_Update,
            GS_DoWork_SimulationModifiers,
            GS_DoWork_PopulationModifiers,
            GS_DoWork_PopulationIndividuals,
        }

#if DEBUG
        public static Int64[] Entries = new Int64[Enum.GetValues(typeof(ProfilerEntry)).Length];
        public static Int64[] Times = new Int64[Enum.GetValues(typeof(ProfilerEntry)).Length];
        static long _lastTicks = 0;
#endif

        // TODO : CONVERT THE PROFILER TO MEDIA TIMERS
        public static void Enter(ProfilerEntry entry)
        {
#if DEBUG
            Entries[(int)entry]++;
            _lastTicks = System.DateTime.Now.Ticks;
#endif
        }

        public static void Leave(ProfilerEntry entry)
        {
#if DEBUG
            Times[(int)entry] += System.DateTime.Now.Ticks - _lastTicks;
#endif
        }

    }
}
