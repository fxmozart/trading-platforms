using System.Windows.Forms;
using Matrix.Common.FrontEnd.UI.Controls;

namespace Matrix.Common.Diagnostics.FrontEnd.TracerControls
{
    partial class TracerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required baseMethod for Designer support - do not modify 
        /// the contents of this baseMethod with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
            System.Windows.Forms.ToolStripLabel toolStripLabel2;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TracerControl));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonEnabled = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAutoUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDetails = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButtonTimeDisplay = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonClear = new System.Windows.Forms.ToolStripButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStripItemsList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.markAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThisMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThisClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThisModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThisMethodToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThieClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofThisModuleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxSelectedItemMessage = new System.Windows.Forms.TextBox();
            this.toolStripFilters = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonPriority = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButtonViewTypeImportant = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonViewTypeAll = new System.Windows.Forms.ToolStripButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageTrace = new System.Windows.Forms.TabPage();
            this.tabPageFilters = new System.Windows.Forms.TabPage();
            this.groupBoxInputFilter = new System.Windows.Forms.GroupBox();
            this.groupBoxViewFilter = new System.Windows.Forms.GroupBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPageSources = new System.Windows.Forms.TabPage();
            this.listView = new Matrix.Common.FrontEnd.UI.Controls.VirtualListViewEx();
            this.columnImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSource = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnGeneral = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitterEx1 = new Matrix.Common.FrontEnd.UI.Controls.SplitterEx();
            this.splitter2 = new Matrix.Common.FrontEnd.UI.Controls.SplitterEx();
            this.methodTracerFilterControl1 = new Matrix.Common.Diagnostics.FrontEnd.TracerControls.MethodTracerFilterControl();
            this.typeTracerFilterControlView = new Matrix.Common.Diagnostics.FrontEnd.TracerControls.TypeTracerFilterControl();
            this.sourcesTracerControl1 = new Matrix.Common.Diagnostics.FrontEnd.TracerControls.SourcesTracerControl();
            toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip.SuspendLayout();
            this.contextMenuStripItemsList.SuspendLayout();
            this.toolStripFilters.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageTrace.SuspendLayout();
            this.tabPageFilters.SuspendLayout();
            this.groupBoxViewFilter.SuspendLayout();
            this.tabPageSources.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator3
            // 
            toolStripSeparator3.Name = "toolStripSeparator3";
            toolStripSeparator3.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripSeparator4
            // 
            toolStripSeparator4.Name = "toolStripSeparator4";
            toolStripSeparator4.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripLabel2
            // 
            toolStripLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            toolStripLabel2.ForeColor = System.Drawing.SystemColors.GrayText;
            toolStripLabel2.Name = "toolStripLabel2";
            toolStripLabel2.Size = new System.Drawing.Size(29, 22);
            toolStripLabel2.Text = "View";
            // 
            // toolStripSeparator5
            // 
            toolStripSeparator5.Name = "toolStripSeparator5";
            toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator6
            // 
            toolStripSeparator6.Name = "toolStripSeparator6";
            toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStrip
            // 
            this.toolStrip.AutoSize = false;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonEnabled,
            toolStripSeparator3,
            this.toolStripButtonRefresh,
            toolStripSeparator2,
            this.toolStripButtonAutoUpdate,
            this.toolStripSeparator1,
            this.toolStripButtonAutoScroll,
            this.toolStripSeparator8,
            this.toolStripButtonDetails,
            toolStripSeparator4,
            this.toolStripDropDownButtonTimeDisplay,
            this.toolStripSeparator11,
            this.toolStripButtonClear});
            this.toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 2, 0);
            this.toolStrip.Size = new System.Drawing.Size(1261, 26);
            this.toolStrip.TabIndex = 5;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonEnabled
            // 
            this.toolStripButtonEnabled.Checked = true;
            this.toolStripButtonEnabled.CheckOnClick = true;
            this.toolStripButtonEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonEnabled.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEnabled.Image")));
            this.toolStripButtonEnabled.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEnabled.Name = "toolStripButtonEnabled";
            this.toolStripButtonEnabled.Size = new System.Drawing.Size(65, 21);
            this.toolStripButtonEnabled.Text = "Enabled";
            this.toolStripButtonEnabled.CheckStateChanged += new System.EventHandler(this.toolStripButtonEnabled_CheckStateChanged);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRefresh.Image")));
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(62, 21);
            this.toolStripButtonRefresh.Text = "Update";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // toolStripButtonAutoUpdate
            // 
            this.toolStripButtonAutoUpdate.AutoSize = false;
            this.toolStripButtonAutoUpdate.Checked = true;
            this.toolStripButtonAutoUpdate.CheckOnClick = true;
            this.toolStripButtonAutoUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonAutoUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAutoUpdate.Name = "toolStripButtonAutoUpdate";
            this.toolStripButtonAutoUpdate.Size = new System.Drawing.Size(77, 19);
            this.toolStripButtonAutoUpdate.Text = "Auto Update";
            this.toolStripButtonAutoUpdate.ToolTipText = "Auto Update [Every 500ms]";
            this.toolStripButtonAutoUpdate.CheckedChanged += new System.EventHandler(this.toolStripButtonAutoUpdate_CheckedChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripButtonAutoScroll
            // 
            this.toolStripButtonAutoScroll.AutoSize = false;
            this.toolStripButtonAutoScroll.CheckOnClick = true;
            this.toolStripButtonAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAutoScroll.Name = "toolStripButtonAutoScroll";
            this.toolStripButtonAutoScroll.Size = new System.Drawing.Size(67, 19);
            this.toolStripButtonAutoScroll.Text = "Auto Scroll";
            this.toolStripButtonAutoScroll.ToolTipText = "Auto Scroll to End";
            this.toolStripButtonAutoScroll.CheckedChanged += new System.EventHandler(this.toolStripButtonAutoScroll_CheckedChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripButtonDetails
            // 
            this.toolStripButtonDetails.AutoSize = false;
            this.toolStripButtonDetails.Checked = true;
            this.toolStripButtonDetails.CheckOnClick = true;
            this.toolStripButtonDetails.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonDetails.Enabled = false;
            this.toolStripButtonDetails.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDetails.Name = "toolStripButtonDetails";
            this.toolStripButtonDetails.Size = new System.Drawing.Size(47, 19);
            this.toolStripButtonDetails.Text = "Details";
            this.toolStripButtonDetails.ToolTipText = "Show Selected Trace Item Details";
            // 
            // toolStripDropDownButtonTimeDisplay
            // 
            this.toolStripDropDownButtonTimeDisplay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButtonTimeDisplay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonTimeDisplay.Name = "toolStripDropDownButtonTimeDisplay";
            this.toolStripDropDownButtonTimeDisplay.Size = new System.Drawing.Size(79, 21);
            this.toolStripDropDownButtonTimeDisplay.Text = "Time Display";
            this.toolStripDropDownButtonTimeDisplay.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButtonTimeDisplay_DropDownItemClicked);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripButtonClear
            // 
            this.toolStripButtonClear.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClear.Image")));
            this.toolStripButtonClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClear.Name = "toolStripButtonClear";
            this.toolStripButtonClear.Size = new System.Drawing.Size(52, 21);
            this.toolStripButtonClear.Text = "Clear";
            this.toolStripButtonClear.Click += new System.EventHandler(this.toolStripButtonClear_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "dot.png");
            this.imageList.Images.SetKeyName(1, "application_connection.png");
            this.imageList.Images.SetKeyName(2, "ERROR.PNG");
            this.imageList.Images.SetKeyName(3, "navigate_right.png");
            this.imageList.Images.SetKeyName(4, "navigate_left.png");
            this.imageList.Images.SetKeyName(5, "WARNING.PNG");
            this.imageList.Images.SetKeyName(6, "CONSOLE.PNG");
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 500;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // contextMenuStripItemsList
            // 
            this.contextMenuStripItemsList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markAllToolStripMenuItem,
            this.showAllToolStripMenuItem});
            this.contextMenuStripItemsList.Name = "contextMenuStripItemsList";
            this.contextMenuStripItemsList.Size = new System.Drawing.Size(126, 48);
            // 
            // markAllToolStripMenuItem
            // 
            this.markAllToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ofThisMethodToolStripMenuItem,
            this.ofThisClassToolStripMenuItem,
            this.ofThisModuleToolStripMenuItem});
            this.markAllToolStripMenuItem.Name = "markAllToolStripMenuItem";
            this.markAllToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.markAllToolStripMenuItem.Text = "Mark All";
            // 
            // ofThisMethodToolStripMenuItem
            // 
            this.ofThisMethodToolStripMenuItem.Name = "ofThisMethodToolStripMenuItem";
            this.ofThisMethodToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ofThisMethodToolStripMenuItem.Text = "of this Method";
            this.ofThisMethodToolStripMenuItem.Click += new System.EventHandler(this.markAllFromThisMethodToolStripMenuItem_Click);
            // 
            // ofThisClassToolStripMenuItem
            // 
            this.ofThisClassToolStripMenuItem.Name = "ofThisClassToolStripMenuItem";
            this.ofThisClassToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ofThisClassToolStripMenuItem.Text = "of this Class";
            this.ofThisClassToolStripMenuItem.Click += new System.EventHandler(this.markAllFromThisClassToolStripMenuItem_Click);
            // 
            // ofThisModuleToolStripMenuItem
            // 
            this.ofThisModuleToolStripMenuItem.Name = "ofThisModuleToolStripMenuItem";
            this.ofThisModuleToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ofThisModuleToolStripMenuItem.Text = "of this Module";
            this.ofThisModuleToolStripMenuItem.Click += new System.EventHandler(this.markAllFromThisModuleToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem
            // 
            this.showAllToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ofThisMethodToolStripMenuItem1,
            this.ofThieClassToolStripMenuItem,
            this.ofThisModuleToolStripMenuItem1});
            this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
            this.showAllToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.showAllToolStripMenuItem.Text = "Show All";
            // 
            // ofThisMethodToolStripMenuItem1
            // 
            this.ofThisMethodToolStripMenuItem1.Name = "ofThisMethodToolStripMenuItem1";
            this.ofThisMethodToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.ofThisMethodToolStripMenuItem1.Text = "of this Method";
            this.ofThisMethodToolStripMenuItem1.Click += new System.EventHandler(this.ofThisMethodToolStripMenuItem1_Click);
            // 
            // ofThieClassToolStripMenuItem
            // 
            this.ofThieClassToolStripMenuItem.Name = "ofThieClassToolStripMenuItem";
            this.ofThieClassToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ofThieClassToolStripMenuItem.Text = "of this Class";
            this.ofThieClassToolStripMenuItem.Click += new System.EventHandler(this.ofThisClassToolStripMenuItem_Click);
            // 
            // ofThisModuleToolStripMenuItem1
            // 
            this.ofThisModuleToolStripMenuItem1.Name = "ofThisModuleToolStripMenuItem1";
            this.ofThisModuleToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.ofThisModuleToolStripMenuItem1.Text = "of this Module";
            this.ofThisModuleToolStripMenuItem1.Click += new System.EventHandler(this.ofThisModuleToolStripMenuItem1_Click);
            // 
            // textBoxSelectedItemMessage
            // 
            this.textBoxSelectedItemMessage.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxSelectedItemMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSelectedItemMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxSelectedItemMessage.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSelectedItemMessage.Location = new System.Drawing.Point(3, 541);
            this.textBoxSelectedItemMessage.Multiline = true;
            this.textBoxSelectedItemMessage.Name = "textBoxSelectedItemMessage";
            this.textBoxSelectedItemMessage.ReadOnly = true;
            this.textBoxSelectedItemMessage.Size = new System.Drawing.Size(1247, 54);
            this.textBoxSelectedItemMessage.TabIndex = 1;
            this.textBoxSelectedItemMessage.Text = "Selected item details.";
            // 
            // toolStripFilters
            // 
            this.toolStripFilters.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            toolStripLabel2,
            this.toolStripSeparator9,
            this.toolStripSplitButtonPriority,
            toolStripSeparator5,
            this.toolStripButtonViewTypeImportant,
            this.toolStripButtonViewTypeAll,
            toolStripSeparator6});
            this.toolStripFilters.Location = new System.Drawing.Point(0, 26);
            this.toolStripFilters.Name = "toolStripFilters";
            this.toolStripFilters.Size = new System.Drawing.Size(1261, 25);
            this.toolStripFilters.TabIndex = 24;
            this.toolStripFilters.Text = "toolStrip1";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSplitButtonPriority
            // 
            this.toolStripSplitButtonPriority.Name = "toolStripSplitButtonPriority";
            this.toolStripSplitButtonPriority.Size = new System.Drawing.Size(54, 22);
            this.toolStripSplitButtonPriority.Text = "Priority";
            // 
            // toolStripButtonViewTypeImportant
            // 
            this.toolStripButtonViewTypeImportant.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonViewTypeImportant.Image = global::Matrix.Common.Diagnostics.FrontEnd.Properties.Resources.WARNING_RED;
            this.toolStripButtonViewTypeImportant.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonViewTypeImportant.Name = "toolStripButtonViewTypeImportant";
            this.toolStripButtonViewTypeImportant.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonViewTypeImportant.Text = "Type Important";
            this.toolStripButtonViewTypeImportant.ToolTipText = "Show only important items.";
            this.toolStripButtonViewTypeImportant.Click += new System.EventHandler(this.toolStripButtonViewTypeImportant_Click);
            // 
            // toolStripButtonViewTypeAll
            // 
            this.toolStripButtonViewTypeAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonViewTypeAll.Image = global::Matrix.Common.Diagnostics.FrontEnd.Properties.Resources.document_check;
            this.toolStripButtonViewTypeAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonViewTypeAll.Name = "toolStripButtonViewTypeAll";
            this.toolStripButtonViewTypeAll.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonViewTypeAll.Text = "Type Any";
            this.toolStripButtonViewTypeAll.ToolTipText = "Show any type of items.";
            this.toolStripButtonViewTypeAll.Click += new System.EventHandler(this.toolStripButtonViewTypeAll_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageTrace);
            this.tabControl.Controls.Add(this.tabPageFilters);
            this.tabControl.Controls.Add(this.tabPageSources);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 51);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1261, 624);
            this.tabControl.TabIndex = 25;
            // 
            // tabPageTrace
            // 
            this.tabPageTrace.Controls.Add(this.listView);
            this.tabPageTrace.Controls.Add(this.splitterEx1);
            this.tabPageTrace.Controls.Add(this.textBoxSelectedItemMessage);
            this.tabPageTrace.Location = new System.Drawing.Point(4, 22);
            this.tabPageTrace.Name = "tabPageTrace";
            this.tabPageTrace.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTrace.Size = new System.Drawing.Size(1253, 598);
            this.tabPageTrace.TabIndex = 0;
            this.tabPageTrace.Text = "Trace";
            this.tabPageTrace.UseVisualStyleBackColor = true;
            // 
            // tabPageFilters
            // 
            this.tabPageFilters.Controls.Add(this.groupBoxInputFilter);
            this.tabPageFilters.Controls.Add(this.groupBoxViewFilter);
            this.tabPageFilters.Controls.Add(this.splitter2);
            this.tabPageFilters.Location = new System.Drawing.Point(4, 22);
            this.tabPageFilters.Name = "tabPageFilters";
            this.tabPageFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFilters.Size = new System.Drawing.Size(1253, 598);
            this.tabPageFilters.TabIndex = 1;
            this.tabPageFilters.Text = "Filters";
            this.tabPageFilters.UseVisualStyleBackColor = true;
            // 
            // groupBoxInputFilter
            // 
            this.groupBoxInputFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxInputFilter.Location = new System.Drawing.Point(479, 3);
            this.groupBoxInputFilter.Name = "groupBoxInputFilter";
            this.groupBoxInputFilter.Size = new System.Drawing.Size(771, 592);
            this.groupBoxInputFilter.TabIndex = 25;
            this.groupBoxInputFilter.TabStop = false;
            this.groupBoxInputFilter.Text = "Input (input filters will block entries as they try to enter the system, as oppos" +
                "ed to view filters that only filter what is visible at the moment)";
            // 
            // groupBoxViewFilter
            // 
            this.groupBoxViewFilter.Controls.Add(this.methodTracerFilterControl1);
            this.groupBoxViewFilter.Controls.Add(this.splitter1);
            this.groupBoxViewFilter.Controls.Add(this.typeTracerFilterControlView);
            this.groupBoxViewFilter.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBoxViewFilter.Location = new System.Drawing.Point(9, 3);
            this.groupBoxViewFilter.Name = "groupBoxViewFilter";
            this.groupBoxViewFilter.Size = new System.Drawing.Size(470, 592);
            this.groupBoxViewFilter.TabIndex = 24;
            this.groupBoxViewFilter.TabStop = false;
            this.groupBoxViewFilter.Text = "View";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(220, 16);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 573);
            this.splitter1.TabIndex = 20;
            this.splitter1.TabStop = false;
            // 
            // tabPageSources
            // 
            this.tabPageSources.Controls.Add(this.sourcesTracerControl1);
            this.tabPageSources.Location = new System.Drawing.Point(4, 22);
            this.tabPageSources.Name = "tabPageSources";
            this.tabPageSources.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSources.Size = new System.Drawing.Size(1253, 598);
            this.tabPageSources.TabIndex = 2;
            this.tabPageSources.Text = "Sources";
            this.tabPageSources.UseVisualStyleBackColor = true;
            // 
            // listView
            // 
            this.listView.AutoScroll = true;
            this.listView.AutoScrollSlack = 3;
            this.listView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnImage,
            this.columnSource,
            this.columnGeneral,
            this.columnMessage,
            this.columnHeader1});
            this.listView.ContextMenuStrip = this.contextMenuStripItemsList;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView.FullRowSelect = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(3, 3);
            this.listView.Name = "listView";
            this.listView.ShowGroups = false;
            this.listView.Size = new System.Drawing.Size(1247, 530);
            this.listView.SmallImageList = this.imageList;
            this.listView.TabIndex = 15;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.VirtualMode = true;
            this.listView.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listView_RetrieveVirtualItem);
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listViewMain_SelectedIndexChanged);
            // 
            // columnImage
            // 
            this.columnImage.Text = "";
            this.columnImage.Width = 22;
            // 
            // columnSource
            // 
            this.columnSource.Text = "S";
            this.columnSource.Width = 18;
            // 
            // columnGeneral
            // 
            this.columnGeneral.Text = "Info";
            // 
            // columnMessage
            // 
            this.columnMessage.Text = "Message";
            this.columnMessage.Width = 750;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Source";
            this.columnHeader1.Width = 597;
            // 
            // splitterEx1
            // 
            this.splitterEx1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterEx1.Location = new System.Drawing.Point(3, 533);
            this.splitterEx1.Name = "splitterEx1";
            this.splitterEx1.Size = new System.Drawing.Size(1247, 8);
            this.splitterEx1.TabIndex = 18;
            this.splitterEx1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(3, 3);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(6, 592);
            this.splitter2.TabIndex = 22;
            this.splitter2.TabStop = false;
            // 
            // methodTracerFilterControl1
            // 
            this.methodTracerFilterControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.methodTracerFilterControl1.Filter = null;
            this.methodTracerFilterControl1.Location = new System.Drawing.Point(230, 16);
            this.methodTracerFilterControl1.Name = "methodTracerFilterControl1";
            this.methodTracerFilterControl1.Size = new System.Drawing.Size(237, 573);
            this.methodTracerFilterControl1.TabIndex = 14;
            // 
            // typeTracerFilterControlView
            // 
            this.typeTracerFilterControlView.Dock = System.Windows.Forms.DockStyle.Left;
            this.typeTracerFilterControlView.Filter = null;
            this.typeTracerFilterControlView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.typeTracerFilterControlView.Location = new System.Drawing.Point(3, 16);
            this.typeTracerFilterControlView.Name = "typeTracerFilterControlView";
            this.typeTracerFilterControlView.Size = new System.Drawing.Size(217, 573);
            this.typeTracerFilterControlView.TabIndex = 19;
            // 
            // sourcesTracerControl1
            // 
            this.sourcesTracerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourcesTracerControl1.Location = new System.Drawing.Point(3, 3);
            this.sourcesTracerControl1.Name = "sourcesTracerControl1";
            this.sourcesTracerControl1.Size = new System.Drawing.Size(1247, 592);
            this.sourcesTracerControl1.TabIndex = 0;
            this.sourcesTracerControl1.Tracer = null;
            // 
            // TracerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.toolStripFilters);
            this.Controls.Add(this.toolStrip);
            this.DoubleBuffered = true;
            this.Name = "TracerControl";
            this.Size = new System.Drawing.Size(1261, 675);
            this.Load += new System.EventHandler(this.TracerControl_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.contextMenuStripItemsList.ResumeLayout(false);
            this.toolStripFilters.ResumeLayout(false);
            this.toolStripFilters.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageTrace.ResumeLayout(false);
            this.tabPageTrace.PerformLayout();
            this.tabPageFilters.ResumeLayout(false);
            this.groupBoxViewFilter.ResumeLayout(false);
            this.tabPageSources.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStripButton toolStripButtonClear;
        private MethodTracerFilterControl methodTracerFilterControl1;
        private System.Windows.Forms.ImageList imageList;
        private VirtualListViewEx listView;
        private ColumnHeader columnGeneral;
        private ColumnHeader columnMessage;
        private ToolStripButton toolStripButtonAutoUpdate;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton toolStripButtonAutoScroll;
        private Timer timerUpdate;
        private ToolStripButton toolStripButtonEnabled;
        private ContextMenuStrip contextMenuStripItemsList;
        private ToolStripMenuItem markAllToolStripMenuItem;
        private ToolStripMenuItem ofThisMethodToolStripMenuItem;
        private ToolStripMenuItem ofThisClassToolStripMenuItem;
        private ToolStripMenuItem ofThisModuleToolStripMenuItem;
        private ToolStripMenuItem showAllToolStripMenuItem;
        private ToolStripMenuItem ofThisMethodToolStripMenuItem1;
        private ToolStripMenuItem ofThieClassToolStripMenuItem;
        private ToolStripMenuItem ofThisModuleToolStripMenuItem1;
        private TypeTracerFilterControl typeTracerFilterControlView;
        private ToolStripButton toolStripButtonDetails;
        private TextBox textBoxSelectedItemMessage;
        private ToolStripDropDownButton toolStripDropDownButtonTimeDisplay;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStrip toolStripFilters;
        private ToolStripSeparator toolStripSeparator9;
        private ToolStripSeparator toolStripSeparator11;
        private ToolStripDropDownButton toolStripSplitButtonPriority;
        private ColumnHeader columnSource;
        private ColumnHeader columnImage;
        private SourcesTracerControl sourcesTracerControl1;
        private TabControl tabControl;
        private TabPage tabPageTrace;
        private TabPage tabPageFilters;
        private SplitterEx splitter2;
        private TabPage tabPageSources;
        private GroupBox groupBoxInputFilter;
        private GroupBox groupBoxViewFilter;
        private Splitter splitter1;
        private SplitterEx splitterEx1;
        private ToolStripButton toolStripButtonViewTypeImportant;
        private ToolStripButton toolStripButtonViewTypeAll;
        private ColumnHeader columnHeader1;
    }
}