// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;
using Matrix.Common.Core.Serialization;
using Matrix.Common.Diagnostics.TracerCore;
using Matrix.Common.Diagnostics.TracerCore.Filters;
using Matrix.Common.Diagnostics.TracerCore.Items;
using Matrix.Common.Diagnostics.TracerCore.Sinks;
using Matrix.Common.Extended;
using Matrix.Common.FrontEnd;
using Matrix.Common.FrontEnd.UI.Controls;
using Matrix.Common.Sockets.NetworkTracer;
using Matrix.Common.Core;

namespace Matrix.Common.Diagnostics.FrontEnd.TracerControls
{
    /// <summary>
    /// Control allows the visualization of tracer and tracer items, filters etc.
    /// It is designed to operate on the items of the first tracer item sink keeper in tracer.
    /// </summary>
    public partial class TracerControl : UserControl
    {
        MethodTracerFilter _methodFilter;
        StringTracerFilter _stringFilter;
        StringTracerFilter _stringInputFilter;
        TypeTracerFilter _typeFilter;
        PriorityFilter _priorityFilter;

        const char Separator = ';';

        string _markingMatch = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string MarkingMatch
        {
            get { return _markingMatch; }
            set 
            { 
                _markingMatch = value;

                DoUpdateUI();
                this.Refresh();
            }
        }

        volatile bool _itemsModified = false;

        volatile TracerItemKeeperSink _itemKeeperSink = null;

        volatile Tracer _tracer;
        /// <summary>
        /// The tracer assigned to this control.
        /// </summary>
        public Tracer Tracer
        {
            get { return _tracer; }
            set
            {
                TracerItemKeeperSink itemKeeperSink = _itemKeeperSink;
                if (itemKeeperSink != null)
                {
                    itemKeeperSink.ItemAddedEvent -= new TracerItemKeeperSink.ItemUpdateDelegate(_tracer_ItemAddedEvent);
                    itemKeeperSink.ItemsFilteredEvent -= new TracerItemKeeperSink.TracerUpdateDelegate(_itemKeeperSink_ItemsFilteredEvent);
                    itemKeeperSink.FilterUpdateEvent -= new TracerItemSink.SinkUpdateDelegate(_itemSink_FilterUpdateEvent);
                    itemKeeperSink.ClearFilters();
                    _itemKeeperSink = null;
                }

                if (_tracer != null)
                {
                    _methodFilter = null;
                    _stringFilter = null;
                    
                    _typeFilter = null;
                    _priorityFilter = null;

                    _tracer = null;

                    methodTracerFilterControl1.Filter = null;
                    typeTracerFilterControlView.Filter = null;
                }

                _tracer = value;
                //_mode = ModeEnum.Default;

                sourcesTracerControl1.Tracer = _tracer as NetworkTracer;

                if (_tracer != null)
                {
                    _itemKeeperSink = (TracerItemKeeperSink)_tracer.GetSinkByType(typeof(TracerItemKeeperSink));

                    _stringInputFilter = new StringTracerFilter();
                    _tracer.Add(_stringInputFilter);

                    // Attach negative string filter property as data source.
                    _inputExcludeStrip.SetDataSource(_stringInputFilter, 
                                                     _stringInputFilter.GetType().GetProperty("NegativeFilterStrings"));

                    if (_itemKeeperSink != null)
                    {
                        _itemKeeperSink.ItemAddedEvent += new TracerItemKeeperSink.ItemUpdateDelegate(_tracer_ItemAddedEvent);
                        _itemKeeperSink.ItemsFilteredEvent += new TracerItemKeeperSink.TracerUpdateDelegate(_itemKeeperSink_ItemsFilteredEvent);
                        _itemKeeperSink.FilterUpdateEvent += new TracerItemSink.SinkUpdateDelegate(_itemSink_FilterUpdateEvent);

                        _methodFilter = new MethodTracerFilter();
                        _stringFilter = new StringTracerFilter();
                        _typeFilter = new TypeTracerFilter();
                        _priorityFilter = new PriorityFilter();

                        _itemKeeperSink.AddFilter(_methodFilter);
                        _itemKeeperSink.AddFilter(_stringFilter);
                        _itemKeeperSink.AddFilter(_typeFilter);
                        _itemKeeperSink.AddFilter(_priorityFilter);

                        // Attach positive filter property string data source.
                        _searchingMatchStrip.SetDataSource(_stringFilter,
                                                           _stringFilter.GetType().GetProperty("PositiveFilterString"));
                        
                        // Attach negative string filter property as data source.
                        _viewExcludeStrip.SetDataSource(_stringFilter, 
                                                        _stringFilter.GetType().GetProperty("NegativeFilterStrings"));

                        methodTracerFilterControl1.Filter = _methodFilter;
                        typeTracerFilterControlView.Filter = _typeFilter;
                    }

                }

                UpdateUI();
                WinFormsHelper.DirectOrManagedInvoke(this, UpdateFiltersUI);
            }
        }

        /// <summary>
        /// Is the method filter control visible.
        /// </summary>
        public bool ShowMethodFilter
        {
            get { return this.methodTracerFilterControl1.Visible; }
            set { /*this.methodTracerFilterControl1.Visible = value;*/ }
        }

        ///// <summary>
        ///// Is the control auto updating upon receiving new messages.
        ///// </summary>
        //public bool AutoUpdate
        //{
        //    get { return toolStripButtonAutoUpdate.Checked; }
        //    set { toolStripButtonAutoUpdate.Checked = true; }
        //}

        StringsControlToolStripEx _inputExcludeStrip = null;
        StringsControlToolStripEx _viewExcludeStrip = null;
        StringsControlToolStripEx _markingMatchStrip = null;
        StringsControlToolStripEx _searchingMatchStrip = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TracerControl()
        {
            InitializeComponent();
            _tracer = new Tracer();

            this.Text = "TracerControl";

            listView.VirtualItemsSelectionRangeChanged += new ListViewVirtualItemsSelectionRangeChangedEventHandler(listView_VirtualItemsSelectionRangeChanged);

            lock (listView)
            {
                listView.AdvancedColumnManagementUnsafe.Add(2, new VirtualListViewEx.ColumnManagementInfo() { AutoResizeMode = ColumnHeaderAutoResizeStyle.ColumnContent });
                listView.AdvancedColumnManagementUnsafe.Add(4, new VirtualListViewEx.ColumnManagementInfo() { FillWhiteSpace = true });
            }

            // Search items.
            _searchingMatchStrip = new StringsControlToolStripEx();
            _searchingMatchStrip.Label = "Search";
            // Data source assigned on tracer assignment.
            //toolStripFilters.Items.Add(new ToolStripSeparator());
            WinFormsHelper.MoveToolStripItems(_searchingMatchStrip, toolStripFilters);

            // Mark items.
            _markingMatchStrip = new StringsControlToolStripEx();
            _markingMatchStrip.Label = "Mark";
            _markingMatchStrip.SetDataSource(this, this.GetType().GetProperty("MarkingMatch"));
            toolStripFilters.Items.Add(new ToolStripSeparator()); 
            WinFormsHelper.MoveToolStripItems(_markingMatchStrip, toolStripFilters);

            // View exclude filtering...
            _viewExcludeStrip = new StringsControlToolStripEx();
            _viewExcludeStrip.Label = "Exclude";
            WinFormsHelper.MoveToolStripItems(_viewExcludeStrip, toolStripFilters);

            // Input filtering...
            ToolStripLabel label = new ToolStripLabel("Filter");
            label.ForeColor = SystemColors.GrayText;
            toolStripFilters.Items.Add(new ToolStripSeparator());
            toolStripFilters.Items.Add(label);

            // Input exlude items.
            _inputExcludeStrip = new StringsControlToolStripEx();
            _inputExcludeStrip.Label = "Exclude";
            // Data source assigned on tracer assignment.
            toolStripFilters.Items.Add(new ToolStripSeparator());
            WinFormsHelper.MoveToolStripItems(_inputExcludeStrip, toolStripFilters);
        }

        private void TracerControl_Load(object sender, EventArgs e)
        {
            toolStripButtonDetails.Checked = false;
            toolStripButtonAutoScroll.Checked = listView.AutoScroll;

            toolStripSplitButtonPriority.DropDownItems.Add("All").Click += new EventHandler(TracerControlPriorityItem_Click);
            toolStripSplitButtonPriority.DropDownItems.Add(new ToolStripSeparator());

            foreach (string name in Enum.GetNames(typeof(Tracer.TimeDisplayFormatEnum)))
            {
                toolStripDropDownButtonTimeDisplay.DropDownItems.Add(name).Tag = Enum.Parse(typeof(Tracer.TimeDisplayFormatEnum), name);
            }

            foreach (string name in Enum.GetNames(typeof(TracerItem.PriorityEnum)))
            {
                ToolStripItem item = toolStripSplitButtonPriority.DropDownItems.Add(name + " and above");
                item.Tag = Enum.Parse(typeof(TracerItem.PriorityEnum), name);
                item.Click += new EventHandler(TracerControlPriorityItem_Click);
            }

            typeTracerFilterControlView.CreateControl();

            UpdateUI();
        }

        void TracerControlSourceItem_Click(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Save state.
        /// </summary>
        /// <param name="state"></param>
        public void SaveState(SerializationInfoEx state)
        {
            if (_stringFilter != null)
            {
                state.AddValue("_stringFilter", _stringFilter);
            }

            if (_stringInputFilter != null)
            {
                state.AddValue("_stringInputFilter", _stringInputFilter);
            }

        }

        /// <summary>
        /// Load state.
        /// </summary>
        /// <param name="state"></param>
        public void LoadState(SerializationInfoEx state)
        {
            if (state.ContainsValue("_stringFilter"))
            {
                StringTracerFilter stringFilter = state.GetValue<StringTracerFilter>("_stringFilter");
                if (_stringFilter != null)
                {
                    _stringFilter.CopyDataFrom(stringFilter);
                }
            }

            if (state.ContainsValue("_stringInputFilter"))
            {
                StringTracerFilter stringInputFilter = state.GetValue<StringTracerFilter>("_stringInputFilter");
                if (_stringInputFilter != null)
                {
                    _stringInputFilter.CopyDataFrom(stringInputFilter);
                }
            }

            WinFormsHelper.DirectOrManagedInvoke(this, UpdateFiltersUI);
        }

        void TracerControlPriorityItem_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem item = sender as ToolStripDropDownItem;
            PriorityFilter filter = _priorityFilter;
            if (filter == null)
            {
                return;
            }
            
            if (item.Tag != null)
            {
                filter.MinimumPriority = (TracerItem.PriorityEnum)item.Tag;
            }
            else
            {
                filter.MinimumPriority = TracerItem.PriorityEnum.Trivial;
            }

            UpdateUI();
        }


        void _itemSink_FilterUpdateEvent(TracerItemSink tracer)
        {
            WinFormsHelper.BeginFilteredManagedInvoke(this, DoUpdateUI);
        }

        const int CleanVirtualItemsCount = 3;

        /// <summary>
        /// 
        /// </summary>
        public void UpdateUI()
        {
            WinFormsHelper.DirectOrManagedInvoke(this, DoUpdateUI);
        }

        void UpdateFiltersUI()
        {
            //if (_stringFilter != null && toolStripTextBoxSearch.Text != _stringFilter.PositiveFilterString)
            //{
            //    toolStripTextBoxSearch.Text = _stringFilter.PositiveFilterString;
            //}
            //else
            //{
            //    toolStripTextBoxSearch.Text = string.Empty;
            //}

            //string excludeText = string.Empty;
            //if (_stringFilter != null && _stringFilter.NegativeFilterStrings != null)
            //{
            //    excludeText = GeneralHelper.ToString(_stringFilter.NegativeFilterStrings, Separator.ToString());
            //}

            //if (toolStripTextBoxExclude.Text != excludeText)
            //{
            //    toolStripTextBoxExclude.Text = excludeText;
            //}

            //excludeText = string.Empty;
            //if (_stringInputFilter != null && _stringInputFilter.NegativeFilterStrings != null)
            //{
            //    excludeText = GeneralHelper.ToString(_stringInputFilter.NegativeFilterStrings, Separator.ToString());
            //}

            _inputExcludeStrip.LoadValues();
            _viewExcludeStrip.LoadValues();
            //_markingMatchStrip.LoadValues();
        }

        /// <summary>
        /// Update user interface based on the underlying information.
        /// </summary>
        void DoUpdateUI()
        {
            try
            {
                if (this.Tracer == null || _itemKeeperSink == null || this.DesignMode)
                {
                    return;
                }

                if (_priorityFilter == null || _priorityFilter.MinimumPriority == TracerItem.PriorityEnum.Minimum)
                {
                    toolStripSplitButtonPriority.Text = "Priority [All]";
                }
                else
                {
                    toolStripSplitButtonPriority.Text = "Priority [+" + GeneralHelper.SplitCapitalLetters(_priorityFilter.MinimumPriority.ToString()) + "]";
                }

                toolStripDropDownButtonTimeDisplay.Text = "Time [" + GeneralHelper.SplitCapitalLetters(_tracer.TimeDisplayFormat.ToString()) + "]";
                this.toolStripButtonEnabled.Checked = this.Tracer.Enabled;

                // Give some slack for the vlist, since it has problems due to Microsoft List implementation.
                listView.VirtualListSize = _itemKeeperSink.FilteredItemsCount + CleanVirtualItemsCount;
                
            }
            catch (Exception ex)
            {
                SystemMonitor.Error("UI Logic Error [" + ex.Message + "]");
            }
        }

        void listView_VirtualItemsSelectionRangeChanged(object sender, ListViewVirtualItemsSelectionRangeChangedEventArgs e)
        {
            
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            if (_itemKeeperSink != null)
            {
                _itemKeeperSink.ReFilterItems();
            }

            //DoUpdateUI();
        }

        private void listViewMain_Resize(object sender, EventArgs e)
        {
            this.listView.Columns[listView.Columns.Count - 1].Width = -2;
            this.listView.Invalidate();
        }

        private void toolStripButtonClear_Click(object sender, EventArgs e)
        {
            this.Tracer.Clear(false);

            //_mode = ModeEnum.Default;
            DoUpdateUI();
        }

        private void listViewMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            TracerItem item = null;
            if (_itemKeeperSink != null && listView.SelectedIndices.Count > 0)
            {
                int index = listView.SelectedIndices[0];

                if (index < listView.VirtualListSize - CleanVirtualItemsCount)
                {
                    item = _itemKeeperSink.GetFilteredItem(index);
                }
            }

            LoadTracerItem(item);
        }

        protected void LoadTracerItem(TracerItem item)
        {
            if (item == null)
            {
                textBoxSelectedItemMessage.Text = string.Empty;
            }
            else
            {
                textBoxSelectedItemMessage.Text = item.PrintMessage(true);
            }
            //this.propertyGridItem.SelectedObject = item;
        }

        //private void toolStripTextBoxSearch_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        toolStripButtonSearch_Click(sender, EventArgs.Empty);
        //    }
        //}

        //private void toolStripButtonSearchClear_Click(object sender, EventArgs e)
        //{
        //    toolStripTextBoxSearch.Text = "";
        //    toolStripButtonSearch_Click(sender, e);
        //}

        //private void toolStripButtonSearch_Click(object sender, EventArgs e)
        //{
        //    _stringFilter.PositiveFilterString = toolStripTextBoxSearch.Text;
        //}

        protected Color GetPriorityColor(TracerItem.PriorityEnum color)
        {
            switch (color)
            {
                case TracerItem.PriorityEnum.Trivial:
                case TracerItem.PriorityEnum.VeryLow:
                case TracerItem.PriorityEnum.Low:
                case TracerItem.PriorityEnum.Medium:
                    return Color.Transparent;

                case TracerItem.PriorityEnum.High:
                    return Color.MistyRose;
                case TracerItem.PriorityEnum.VeryHigh:
                    return Color.LightSalmon;
                case TracerItem.PriorityEnum.Critical:
                    return Color.Red;
            }

            return Color.Transparent;
        }

        private void listView_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            e.Item = new ListViewItem();

            TracerItem tracerItem = null;

            // If we are in the last items, make sure to always leave them blank.
            if (e.ItemIndex <= listView.VirtualListSize - CleanVirtualItemsCount)
            {
                if (_itemKeeperSink != null)
                {
                    tracerItem = _itemKeeperSink.GetFilteredItem(e.ItemIndex);
                }
            }

            if (tracerItem == null)
            {// Empty item.
                e.Item.SubItems.Clear();
                for (int i = 0; i < listView.Columns.Count; i++)
                {
                    e.Item.SubItems.Add(string.Empty);
                }
                return;
            }

            if (tracerItem.ExtendedType == TracerItem.ExtendedTypeEnum.None
                || tracerItem.ExtendedType == TracerItem.ExtendedTypeEnum.Operation)
            {
                switch (tracerItem.Type)
                {
                    case TracerItem.TypeEnum.Debug:
                        e.Item.ImageIndex = 0;
                        break;
                    case TracerItem.TypeEnum.Warning:
                        e.Item.ImageIndex = 5;
                        break;
                    case TracerItem.TypeEnum.Error:
                        e.Item.ImageIndex = 2;
                        break;
                    case TracerItem.TypeEnum.Fatal:
                        e.Item.ImageIndex = 6;
                        break;
                    default:
                        // If there are only items with no images, the image column width gets
                        // substraced from the Column.0 width and this causes a bug.
                        e.Item.ImageIndex = 0;
                        break;
                }
            }
            else
            {
                switch (tracerItem.ExtendedType)
                {
                    case TracerItem.ExtendedTypeEnum.None:
                        // Not happening.
                        break;
                    case TracerItem.ExtendedTypeEnum.Operation:
                        // Not happening.
                        break;
                    case TracerItem.ExtendedTypeEnum.MethodEntry:
                        e.Item.ImageIndex = 3;
                        break;
                    case TracerItem.ExtendedTypeEnum.MethodExit:
                        e.Item.ImageIndex = 4;
                        break;
                    default:
                        break;
                }
            }

            if (e.Item.UseItemStyleForSubItems)
            {
                e.Item.UseItemStyleForSubItems = false;
            }

            ListViewItem.ListViewSubItem sourceSubItem = new ListViewItem.ListViewSubItem(e.Item, string.Empty);
            //sourceSubItem.BackColor = Color.Green;
            e.Item.SubItems.Add(sourceSubItem);

            ListViewItem.ListViewSubItem infoSubItem = new ListViewItem.ListViewSubItem();
            Color color = GetPriorityColor(tracerItem.Priority);
            if (color != infoSubItem.BackColor)
            {
                infoSubItem.BackColor = color;
            }

            string day = tracerItem.DateTime.Day.ToString();
            if (tracerItem.DateTime.Day == 1)
            {
                day += "st";
            }
            else if (tracerItem.DateTime.Day == 2)
            {
                day += "nd";
            }
            else if (tracerItem.DateTime.Day == 3)
            {
                day += "rd";
            }
            else
            {
                day += "th";
            }

            string time = string.Empty;
            Tracer tracer = _tracer;
            if (tracer != null)
            {
                if (tracer.TimeDisplayFormat == Tracer.TimeDisplayFormatEnum.ApplicationTicks)
                {// Application time.
                    time = Math.Round(((decimal)tracerItem.ApplicationTick / (decimal)Stopwatch.Frequency), 6).ToString();
                }
                else if (tracer.TimeDisplayFormat == Tracer.TimeDisplayFormatEnum.DateTime)
                {// Date time conventional.
                    time = day + tracerItem.DateTime.ToString(", HH:mm:ss:ffff");
                }
                else
                {// Combined.
                    time = day + tracerItem.DateTime.ToString(", HH:mm:ss:ffff");
                    time += " | " + Math.Round(((decimal)tracerItem.ApplicationTick / (decimal)Stopwatch.Frequency), 6).ToString();
                }
            }

            infoSubItem.Text = tracerItem.Index + ", " + time;
            e.Item.SubItems.Add(infoSubItem);

            ListViewItem.ListViewSubItem messageSubItem = new ListViewItem.ListViewSubItem(
                e.Item, tracerItem.Message /*.PrintMessage(true)*/);

            if (string.IsNullOrEmpty(_markingMatch) == false)
            {
                if (StringTracerFilter.FilterItem(tracerItem, _markingMatch, null))
                {
                    messageSubItem.BackColor = Color.PowderBlue;
                }
            }
            
            e.Item.SubItems.Add(messageSubItem);

            e.Item.SubItems.Add(StringHelper.ToStringSafe(tracerItem.Source));
        }

        void _tracer_ItemAddedEvent(TracerItemKeeperSink sink, TracerItem item)
        {
            if (toolStripButtonAutoUpdate.Checked)
            {// Only updating on new items added, when auto update is enabled.
                _itemsModified = true;
            }
        }

        void _itemKeeperSink_ItemsFilteredEvent(TracerItemKeeperSink tracer)
        {
            _itemsModified = true;
        }

        private void toolStripButtonAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            //timerUpdate.Enabled = toolStripButtonAutoUpdate.Checked;
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            if (_itemsModified)
            {
                _itemsModified = false;
                DoUpdateUI();
            }
            else
            {
                listView.UpdateAutoScrollPosition();
            }

            //TracerHelper.TraceEntry();
        }

        private void toolStripButtonEnabled_CheckStateChanged(object sender, EventArgs e)
        {
            if (Tracer == null)
            {
                return;
            }

            this.Tracer.Enabled = toolStripButtonEnabled.Checked;
        }

        private void markAllFromThisMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = null;
            item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;

            if (item != null)
            {
                MarkingMatch = item.DeclaringTypeName + "." + item.DeclaringMethodName;
            }
        }

        private void markAllFromThisClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = null;
            item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;

            if (item != null)
            {
                MarkingMatch = item.DeclaringTypeModuleName.Substring(0, item.DeclaringTypeModuleName.LastIndexOf(".")) + "." + item.DeclaringTypeName;
            }
        }

        private void markAllFromThisModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;
            if (item != null)
            {
                MarkingMatch = item.DeclaringTypeModuleName.Substring(0, item.DeclaringTypeModuleName.LastIndexOf(".")) + ".";
            }
        }

        private void ofThisMethodToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;

            if (item != null)
            {
                _stringFilter.PositiveFilterString = item.DeclaringTypeName + "." + item.DeclaringMethodName;
                //toolStripTextBoxSearch.Text = item.MethodBase.DeclaringType.Name + "." + item.MethodBase.Name;
            }

            //toolStripButtonSearch_Click(sender, e);
        }

        private void ofThisClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;
            if (item != null)
            {
                _stringFilter.PositiveFilterString = item.DeclaringTypeModuleName.Substring(0, item.DeclaringTypeModuleName.LastIndexOf(".")) + "." + item.DeclaringTypeName;
                //toolStripTextBoxSearch.Text = item.MethodBase.DeclaringType.Module.Name.Substring(0, item.MethodBase.DeclaringType.Module.Name.LastIndexOf(".")) + "." + item.MethodBase.DeclaringType.Name;
            }

            //toolStripButtonSearch_Click(sender, e);
        }

        private void ofThisModuleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0 || _itemKeeperSink == null)
            {
                return;
            }

            MethodTracerItem item = _itemKeeperSink.GetFilteredItem(listView.SelectedIndices[0]) as MethodTracerItem;

            if (item != null)
            {
                _stringFilter.PositiveFilterString = "[" + item.DeclaringTypeModuleName.Substring(0, item.DeclaringTypeModuleName.LastIndexOf(".")) + ".";
                //toolStripTextBoxSearch.Text = "[" + item.MethodBase.DeclaringType.Module.Name.Substring(0, item.MethodBase.DeclaringType.Module.Name.LastIndexOf(".")) + ".";
            }

            //toolStripButtonSearch_Click(sender, e);
        }

        private void toolStripButtonAutoScroll_CheckedChanged(object sender, EventArgs e)
        {
            this.listView.AutoScroll = toolStripButtonAutoScroll.Checked;
        }

        private void toolStripDropDownButtonTimeDisplay_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            Tracer tracer = _tracer;
            if (tracer != null)
            {
                tracer.TimeDisplayFormat = (Tracer.TimeDisplayFormatEnum)e.ClickedItem.Tag;
            }

            UpdateUI();
        }

        private void toolStripButtonViewTypeImportant_Click(object sender, EventArgs e)
        {
            typeTracerFilterControlView.toolStripButtonCheckImportant_Click(sender, e);
        }

        private void toolStripButtonViewTypeAll_Click(object sender, EventArgs e)
        {
            typeTracerFilterControlView.toolStripButtonCheckAll_Click(sender, e);
        }

    }
}
