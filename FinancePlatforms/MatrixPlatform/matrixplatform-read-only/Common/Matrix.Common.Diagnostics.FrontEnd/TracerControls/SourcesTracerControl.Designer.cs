﻿namespace Matrix.Common.Diagnostics.FrontEnd.TracerControls
{
    partial class SourcesTracerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripLabel toolStripLabel1;
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Clients", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Servers", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SourcesTracerControl));
            this.toolStripServer = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonServer = new System.Windows.Forms.ToolStripButton();
            this.listViewSources = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.toolStripSources = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBoxAddress = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRemove = new System.Windows.Forms.ToolStripButton();
            toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripServer.SuspendLayout();
            this.toolStripSources.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripLabel1
            // 
            toolStripLabel1.Name = "toolStripLabel1";
            toolStripLabel1.Size = new System.Drawing.Size(39, 22);
            toolStripLabel1.Text = "Server";
            // 
            // toolStripServer
            // 
            this.toolStripServer.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripServer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                                                             toolStripLabel1,
                                                                                             this.toolStripSeparator2,
                                                                                             this.toolStripButtonServer});
            this.toolStripServer.Location = new System.Drawing.Point(0, 0);
            this.toolStripServer.Name = "toolStripServer";
            this.toolStripServer.Size = new System.Drawing.Size(266, 25);
            this.toolStripServer.TabIndex = 2;
            this.toolStripServer.Text = "toolStrip1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonServer
            // 
            this.toolStripButtonServer.CheckOnClick = true;
            this.toolStripButtonServer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonServer.Name = "toolStripButtonServer";
            this.toolStripButtonServer.Size = new System.Drawing.Size(35, 22);
            this.toolStripButtonServer.Text = "Start";
            this.toolStripButtonServer.ToolTipText = "Start or stop server.";
            this.toolStripButtonServer.Click += new System.EventHandler(this.toolStripButtonServer_Click);
            // 
            // listViewSources
            // 
            this.listViewSources.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listViewSources.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                              this.columnHeader1,
                                                                                              this.columnHeader2});
            this.listViewSources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSources.FullRowSelect = true;
            listViewGroup1.Header = "Clients";
            listViewGroup1.Name = "Clients";
            listViewGroup2.Header = "Servers";
            listViewGroup2.Name = "Servers";
            this.listViewSources.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
                                                                                              listViewGroup1,
                                                                                              listViewGroup2});
            this.listViewSources.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewSources.Location = new System.Drawing.Point(0, 50);
            this.listViewSources.Name = "listViewSources";
            this.listViewSources.Size = new System.Drawing.Size(266, 217);
            this.listViewSources.TabIndex = 3;
            this.listViewSources.UseCompatibleStateImageBehavior = false;
            this.listViewSources.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "State";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Address";
            this.columnHeader2.Width = 206;
            // 
            // toolStripSources
            // 
            this.toolStripSources.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripSources.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                                                              this.toolStripLabel2,
                                                                                              this.toolStripSeparator1,
                                                                                              this.toolStripTextBoxAddress,
                                                                                              this.toolStripButtonAdd,
                                                                                              this.toolStripButtonRemove});
            this.toolStripSources.Location = new System.Drawing.Point(0, 25);
            this.toolStripSources.Name = "toolStripSources";
            this.toolStripSources.Size = new System.Drawing.Size(266, 25);
            this.toolStripSources.TabIndex = 4;
            this.toolStripSources.Text = "toolStrip2";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel2.Text = "Sources";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBoxAddress
            // 
            this.toolStripTextBoxAddress.Name = "toolStripTextBoxAddress";
            this.toolStripTextBoxAddress.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBoxAddress.Text = "127.0.0.1";
            this.toolStripTextBoxAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBoxAddress_KeyDown);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(30, 22);
            this.toolStripButtonAdd.Text = "Add";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonRemove
            // 
            this.toolStripButtonRemove.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonRemove.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRemove.Image")));
            this.toolStripButtonRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemove.Name = "toolStripButtonRemove";
            this.toolStripButtonRemove.Size = new System.Drawing.Size(50, 22);
            this.toolStripButtonRemove.Text = "Remove";
            this.toolStripButtonRemove.Click += new System.EventHandler(this.toolStripButtonRemove_Click);
            // 
            // SourcesTracerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listViewSources);
            this.Controls.Add(this.toolStripSources);
            this.Controls.Add(this.toolStripServer);
            this.Name = "SourcesTracerControl";
            this.Size = new System.Drawing.Size(266, 267);
            this.toolStripServer.ResumeLayout(false);
            this.toolStripServer.PerformLayout();
            this.toolStripSources.ResumeLayout(false);
            this.toolStripSources.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripServer;
        private System.Windows.Forms.ListView listViewSources;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripButton toolStripButtonServer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStrip toolStripSources;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxAddress;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonRemove;
    }
}