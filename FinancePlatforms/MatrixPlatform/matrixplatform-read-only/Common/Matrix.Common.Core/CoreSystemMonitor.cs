// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;

namespace Matrix.Common.Core
{
    /// <summary>
    /// A simplified version of the System monitor, allows the core components
    /// to have some primitive access to this type of functionality as well.
    /// </summary>
    public static class CoreSystemMonitor
    {
        public delegate void ReportDelegate(string details, Exception optionalException);

        public static event ReportDelegate InfoEvent;
        public static event ReportDelegate OperationErrorEvent;
        public static event ReportDelegate OperationWarningEvent;
        public static event ReportDelegate ErrorEvent;
        public static event ReportDelegate WarningEvent;

        internal static void Info(string errorMessage)
        {
            ReportDelegate del = InfoEvent;
            if (del != null)
            {
                del(errorMessage, null);
            }
        }

        internal static void OperationError(string errorDetails)
        {
            ReportDelegate del = OperationErrorEvent;
            if (del != null)
            {
                del(errorDetails, null);
            }
        }

        internal static void OperationError(string errorDetails, Exception exception)
        {
            ReportDelegate del = OperationErrorEvent;
            if (del != null)
            {
                del(errorDetails, exception);
            }
        }

        internal static void OperationWarning(string warningMessage)
        {
            ReportDelegate del = OperationWarningEvent;
            if (del != null)
            {
                del(warningMessage, null);
            }
        }

        internal static void Error(string errorMessage)
        {
            ReportDelegate del = ErrorEvent;
            if (del != null)
            {
                del(errorMessage, null);
            }
        }

        internal static void Error(string errorMessage, Exception exception)
        {
            ReportDelegate del = ErrorEvent;
            if (del != null)
            {
                del(errorMessage, exception);
            }
        }

        internal static void Warning(string warningMessage)
        {
            ReportDelegate del = WarningEvent;
            if (del != null)
            {
                del(warningMessage, null);
            }
        }
    }
}
