// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;

namespace Matrix.Common.Core.Identification
{
    /// <summary>
    /// Serves as a basis for multiple types of identificantion classes.
    /// The concept is to provide a unified way of undentifying elements/instances etc.
    /// This is the top of the component hierarchy.
    /// </summary>
    [Serializable]
    //[TypeConverterAttribute(typeof(ExpandableObjectConverter))]
    public class ComponentId : IComparable<ComponentId>, IEquatable<ComponentId>
    {
        /// <summary>
        /// Unique GUID of the client.
        /// </summary>
        public Guid Guid { get; protected set; }

        /// <summary>
        /// Name of the client.
        /// </summary>
        public string Name { get; protected set; }
        
        /// <summary>
        /// Is this an empty instance (not actual value).
        /// </summary>
        public bool IsEmpty
        {
            get { return this.Guid == Guid.Empty; }
        }

        /// <summary>
        /// Empty value.
        /// </summary>
        public static ComponentId Empty
        {
            get { return new ComponentId(Guid.Empty, string.Empty); }
        }

        /// <summary>
        /// Default parameterless constructor (may be used in XML serialization).
        /// </summary>
        public ComponentId():
            this(Guid.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ComponentId(Guid guid, string name)
        {
            this.Guid = guid;
            this.Name = name;
        }

        /// <summary>
        /// Comparition operator.
        /// </summary>
        public static bool operator ==(ComponentId a, ComponentId b)
        {
            if (((object)a != null) && ((object)b != null))
            {
                return a.Equals(b);
            }

            if ((object)a == null && (object)b == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool operator !=(ComponentId a, ComponentId b)
        {
            return !(a == b);
        }
        
        /// <summary>
        /// Override the default equals, to handle operations within collections.
        /// </summary>
        public override bool Equals(object obj)
        {
            return Equals((ComponentId)obj);
        }

        /// <summary>
        /// Override the default equals, to handle operations within collections.
        /// </summary>
        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }

        /// <summary>
        /// Print general information related to this object.
        /// </summary>
        /// <returns></returns>
        public virtual string Print()
        {
            return Name + "[" + Guid.ToString() + "]";
        }

        public override string ToString()
        {
            return Print();
        }

        #region IComparable<Id> Members

        /// <summary>
        /// Compare is done partial (Guid only).
        /// </summary>
        public int CompareTo(ComponentId other)
        {
            return Guid.CompareTo(other.Guid);
        }

        #endregion

        #region IEquatable<Id> Members

        /// <summary>
        /// Equal is done partial (Guid only).
        /// </summary>
        public bool Equals(ComponentId other)
        {
            if (other == null)
            {
                return false;
            }

            return (Guid.Equals(other.Guid));
        }

        #endregion
    }
}
