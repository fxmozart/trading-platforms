// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Matrix.Common.Core;

namespace Matrix.Common.Core.Serialization
{
    /// <summary>
    /// Serialize messages using the .NET framework binary formatter.
    /// </summary>
    public class BinarySerializer : SerializerBase
    {
        BinaryFormatter _formatter = new BinaryFormatter();

        /// <summary>
        /// Throws.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override bool SerializeData(Stream stream, object message)
        {
            BinaryFormatter formatter = _formatter;
            if (formatter == null)
            {
                return false;
            }

            formatter.Serialize(stream, message);
            return true;
        }

        /// <summary>
        /// Throws.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        protected override object DeserializeData(Stream stream)
        {
            BinaryFormatter formatter = _formatter;
            if (formatter == null)
            {
                return null;
            }

            try
            {
                return formatter.Deserialize(stream);
            }
            catch (Exception ex)
            {
                CoreSystemMonitor.OperationError("Failed to deserialize object", ex);
                throw new InvalidDataException(ex.Message, ex);
            }

        }

        //public override object Duplicate(object item)
        //{
        //    return SerializationHelper.BinaryClone(item);
        //}
    }
}
