// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;

namespace Matrix.Common.Core.Serialization
{
    /// <summary>
    /// Helper class to contain and make easier access to serialization data of all serializable types.
    /// Extended version of the System SerializationInfo class.
    /// </summary>
    [Serializable]
    public class SerializationInfoEx
    {
        readonly Dictionary<string, object> _objects = new Dictionary<string, object>();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SerializationInfoEx()
        {
        }

        /// <summary>
        /// Deserialize data from stream.
        /// </summary>
        public SerializationInfoEx(MemoryStream stream)
        {
            object value;
            if (SerializationHelper.Deserialize(stream, out value).IsSuccess)
            {
                _objects = (Dictionary<string, object>)value;
            }
        }

        /// <summary>
        /// Clear persisted data.
        /// </summary>
        public void Clear()
        {
            lock (this)
            {
                _objects.Clear();
            }
        }

        /// <summary>
        /// Clear persisted data by part (or all) of name.
        /// </summary>
        public void ClearByNamePart(string namePart)
        {
            lock (this)
            {
                foreach (string pairName in CommonHelper.EnumerableToArray(_objects.Keys))
                {
                    if (pairName.Contains(namePart))
                    {
                        _objects.Remove(pairName);
                    }
                }
            }
        }

        /// <summary>
        /// Delete the value with the given name.
        /// </summary>
        /// <param name="name"></param>
        public bool DeleteValue(string name)
        {
            lock (this)
            {
                return _objects.Remove(name);
            }
        }

        /// <summary>
        /// Convert persisted data to stream.
        /// </summary>
        /// <returns></returns>
        public MemoryStream ToStream()
        {
            lock (this)
            {
                MemoryStream stream = new MemoryStream();
                SerializationHelper.Serialize(stream, _objects);
                return stream;
            }
        }

        /// <summary>
        /// Add new value to the list of persisted values.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddValue(string name, object value)
        {
            lock (this)
            {
                _objects[name] = value;
            }
        }

        /// <summary>
        /// Read string value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetString(string name)
        {
            return GetValue<string>(name);
        }

        /// <summary>
        /// Read an int value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetInt32(string name)
        {
            return GetValue<Int32>(name);
        }

        /// <summary>
        /// Read a single value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Single GetSingle(string name)
        {
            return GetValue<Single>(name);
        }

        /// <summary>
        /// Read a boolean value.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool GetBoolean(string name)
        {
            return GetValue<bool>(name);
        }

        /// <summary>
        /// Obtain a serialized value with the type T and name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetValue<T>(string name)
        {
            lock (this)
            {
                return (T)_objects[name];
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public bool TryGetValue<T>(string name, ref T value)
        {
            lock (this)
            {
                if (_objects.ContainsKey(name))
                {
                    value = (T)_objects[name];
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// If value is not present, create a new instance and return it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetValueOrNew<T>(string name)
            where T : new()
        {
            lock (this)
            {
                if (ContainsValue(name))
                {
                    return (T)_objects[name];
                }
                else
                {
                    return new T();
                }
            }
        }

        /// <summary>
        /// Helper, redefine of ContainsValue().
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasValue(string name)
        {
            return ContainsValue(name);
        }

        /// <summary>
        /// Check if a value with this name is contained in the list of serialized values.
        /// </summary>
        public bool ContainsValue(string name)
        {
            lock (this)
            {
                return _objects.ContainsKey(name);
            }
        }
    }
}
