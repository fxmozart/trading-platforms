// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Common.Core.Results
{
    /// <summary>
    /// Class provides common extended result information, for an operation.
    /// 
    /// *IMPROTANT* if Result is struct, it can never be null so allows automated safety in this direction.
    /// </summary>
    [Serializable]
    public class Result
    {
        public enum ResultEnum
        {
            Success,
            Failure
        };

        /// <summary>
        /// Default predefined value.
        /// </summary>
        public static Result Success
        {
            get { return new Result(ResultEnum.Success); }
        }

        /// <summary>
        /// Default predefined value.
        /// </summary>
        public static Result Failure
        {
            get { return new Result(ResultEnum.Failure); }
        }

        volatile ResultEnum _value;
        /// <summary>
        /// Value of the result.
        /// </summary>
        public ResultEnum Value
        {
            get { return _value; }
            set { _value = value; }
        }

        Exception _optionalException;
        /// <summary>
        /// Exception instance that may have occured during the operation.
        /// </summary>
        public Exception OptionalException
        {
            get { return _optionalException; }
            set { _optionalException = value; }
        }

        volatile string _optionalMessage;
        /// <summary>
        /// Any message from the operation.
        /// </summary>
        public string OptionalMessage
        {
            get { return _optionalMessage; }
            set { _optionalMessage = value; }
        }

        public bool IsSuccess
        {
            get
            {
                return Value == ResultEnum.Success;
            }
        }

        public bool IsFailure
        {
            get
            {
                return Value == ResultEnum.Failure;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Result(ResultEnum result)
        {
            _optionalMessage = string.Empty;
            _optionalException = null;
            _value = result;
        }

        /// <summary>
        /// Create a success result with this message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Result Succeed(string message)
        {
            return new Result(ResultEnum.Success) { OptionalMessage = message };
        }

        /// <summary>
        /// Create a fail result with this message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Result Fail(string message)
        {
            return new Result(ResultEnum.Failure) { OptionalMessage = message };
        }

        /// <summary>
        /// Create a fail result with this message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Result Fail(string message, Exception exception)
        {
            return new Result(ResultEnum.Failure) { OptionalMessage = message, OptionalException = exception };
        }

        /// <summary>
        /// Create a fail result with this exception.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Result Fail(Exception exception)
        {
            return new Result(ResultEnum.Failure) { OptionalException = exception };
        }

        public override string ToString()
        {
            return base.ToString() + ", Msg[" + OptionalMessage + "], Exc[" + CommonHelper.GetExceptionMessage(OptionalException) + "]";
        }
    }
}
