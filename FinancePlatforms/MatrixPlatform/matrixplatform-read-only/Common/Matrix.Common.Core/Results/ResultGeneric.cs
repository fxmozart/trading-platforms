// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix.Common.Core.Results
{
    /// <summary>
    /// Extends default result by providing the ability to carry a custom value.
    /// </summary>
    /// <typeparam name="ResultType">The type of the custom value of this result.</typeparam>
    public class Result<ResultType> : Result
    {
        public ResultType ResultValue { get; set; }

        /// <summary>
        /// Default predefined value.
        /// </summary>
        public new static Result<ResultType> Success
        {
            get { return new Result<ResultType>(ResultEnum.Success); }
        }

        /// <summary>
        /// Default predefined value.
        /// </summary>
        public new static Result<ResultType> Failure
        {
            get { return new Result<ResultType>(ResultEnum.Failure); }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Result(ResultEnum value) 
            : base(value)
        {
        }

        /// <summary>
        /// Default success constructor.
        /// </summary>
        public Result(ResultType result)
            : base(ResultEnum.Success)
        {
            ResultValue = result;
        }

        /// <summary>
        /// Default fail constructor.
        /// </summary>
        public Result()
            : base(ResultEnum.Failure)
        {
        }

        public static Result<ResultType> Succeed(ResultType result)
        {
            return new Result<ResultType>(ResultEnum.Success) { ResultValue = result };
        }

        public static Result<ResultType> Succeed(ResultType result, string message)
        {
            return new Result<ResultType>(ResultEnum.Success) { ResultValue = result, OptionalMessage = message };
        }

        /// <summary>
        /// Create a fail result with this message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Result<ResultType> Fail(ResultType result, string message)
        {
            return new Result<ResultType>(ResultEnum.Failure) { ResultValue = result, OptionalMessage = message };
        }

    }
}
