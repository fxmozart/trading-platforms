// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using Matrix.Common.Core;

namespace Matrix.Common.Core
{
    /// <summary>
    /// Helps with the control and events of lifetime events of the application.
    /// In console applications make sure to call "SetApplicationClosing" upon close.
    /// In ApplicationDomains mode - tested under nunit and handled.
    /// In Service mode - not tested.
    /// </summary>
    public static class ApplicationLifetimeHelper
    {
        static System.Threading.Mutex _applicationMutex = null;

        static volatile Stopwatch _applicationStopwatch = null;
        /// <summary>
        /// The stopwatch measures time since the application was started.
        /// </summary>
        public static long ApplicationStopwatchTicks
        {
            get
            {
                return _applicationStopwatch.ElapsedTicks;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static long ApplicationStopwatchMilliseconds
        {
            get
            {
                return _applicationStopwatch.ElapsedMilliseconds;
            }
        }

        /// <summary>
        /// CompletionEvent raised when application closing. Preffered to use this instead of Application.ApplicationExit
        /// since a Windows Service implementation may require modifications.
        /// </summary>
        public static event CommonHelper.DefaultDelegate ApplicationClosingEvent;

        volatile static bool _applicationClosing = false;
        /// <summary>
        /// 
        /// </summary>
        public static bool ApplicationClosing
        {
            get
            {
                //Console.
                if (_applicationClosing && Environment.HasShutdownStarted)
                {// A shutdown has started.
                    _applicationClosing = false;
                }

                return _applicationClosing;
            }
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static ApplicationLifetimeHelper()
        {
            _applicationStopwatch = new Stopwatch();
            _applicationStopwatch.Start();

            //System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            //process.Exited += new EventHandler(process_Exited);

            // This greatly helps when the module is loaded inside an AppDomain (like in nUnit).
            AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);

            // This is a soft connection to the Application class, and will operate only 
            // when it is available in the using application.
            Type application = Type.GetType("System.Windows.Forms.Application, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            if (application != null)
            {
                EventInfo exitEventInfo = application.GetEvent("ApplicationExit");
                if (exitEventInfo != null)
                {
                    exitEventInfo.AddEventHandler(null, new EventHandler(Application_ApplicationExit));
                }
            }
        }

        static void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            SetApplicationClosing();
        }

        /// <summary>
        /// Creates a static application mutex, that is usefull for 
        /// checking if the application is already running.
        /// </summary>
        /// <returns>Returns false this call fails (already called this before), or true if it is good.</returns>
        public static bool TryGetApplicationMutex(string mutexName, out bool createdNew)
        {
            createdNew = false;
            if (_applicationMutex != null)
            {
                return false;
            }

            _applicationMutex = new System.Threading.Mutex(true, mutexName, out createdNew);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ReleaseApplicationMutex()
        {
            if (_applicationMutex != null)
            {
                _applicationMutex.Close();
                _applicationMutex = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SetApplicationClosing()
        {
            if (_applicationClosing == false)
            {
                _applicationClosing = true;

                CommonHelper.DefaultDelegate delegateInstance = ApplicationClosingEvent;
                if (delegateInstance != null)
                {
                    delegateInstance();
                }
            }
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            SetApplicationClosing();

            //// On closing the application, release the threadpool resources and threads to speed up closing.
            //// _threadPoolEx.Dispose();

            //_applicationClosing = true;

            //if (ApplicationClosingEvent != null)
            //{
            //    ApplicationClosingEvent();
            //}
        }

        //static void process_Exited(object sender, EventArgs e)
        //{
        //    int h = 2;
        //}
    }
}
