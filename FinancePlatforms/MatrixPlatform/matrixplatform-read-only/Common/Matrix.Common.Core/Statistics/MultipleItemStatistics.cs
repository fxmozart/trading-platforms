// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Matrix.Common.Core.Statistics
{
    /// <summary>
    /// A set of item statistics.
    /// </summary>
    public class MultipleItemStatisticsSet
    {
        List<PropertyStatisticsData> _propertiesData = new List<PropertyStatisticsData>();
        public List<PropertyStatisticsData> PropertiesData
        {
            get
            {
                return _propertiesData;
            }
        }

        string _name;
        public string Name
        {
            get { return _name; }
        }

        public int ItemsCount
        {
            get
            {
                if (_propertiesData.Count > 0)
                {
                    return _propertiesData[0].Values.Count;
                }
                return 0;
            }
        }


        public object[] Items
        {
            set
            {
                if (value.Length == 0)
                {
                    return;
                }

                Type t = value[0].GetType();
                List<PropertyInfo> propertyInfos = new List<PropertyInfo>(t.GetProperties(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance));

                for (int i = propertyInfos.Count - 1; i >= 0; i--)
                {
                    if (propertyInfos[i].GetCustomAttributes(typeof(ItemStatisticsAttribute), true).Length != 0)
                    {// This property is not marked with the proper attribute.
                        _propertiesData.Add(new PropertyStatisticsData(this, propertyInfos[i], value));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public MultipleItemStatisticsSet(string name, object[] items)
        {
            _name = name;
            Items = items;
        }

        public double[][] CombinedPropertyData()
        {
            return null;
        }
    }
}
