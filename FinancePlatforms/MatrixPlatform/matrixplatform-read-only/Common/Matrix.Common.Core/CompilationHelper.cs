// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Core;
using Microsoft.CSharp;
using System.Reflection;
using System.CodeDom.Compiler;

namespace Matrix.Common.Core
{
    /// <summary>
    /// Helper class, helps with the runtime compilation operations.
    /// </summary>
    public static class CompilationHelper
    {
        /// <summary>
        ///  This will compile sources using the tools integrated in the .NET framework.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <param name="resultMessagesAndRows"></param>
        /// <returns>Will return null if compilation was not successful.</returns>
        public static Assembly CompileSourceToAssembly(string sourceCode, out Dictionary<string, int> resultMessagesAndRows)
        {
            resultMessagesAndRows = new Dictionary<string, int>();

            CSharpCodeProvider codeProvider = new CSharpCodeProvider();

            System.CodeDom.Compiler.CompilerParameters parameters = new CompilerParameters();
            parameters.GenerateInMemory = true;

            // this will add references to all the assemblies we are using.
            foreach (Assembly assembly in ReflectionHelper.GetAssemblies(true, true))
            {
                parameters.ReferencedAssemblies.Add(assembly.Location);
            }

            CompilerResults results = codeProvider.CompileAssemblyFromSource(parameters, sourceCode);
            
            if (results.Errors.Count > 0)
            {
                foreach (CompilerError error in results.Errors)
                {
                    resultMessagesAndRows.Add("Line " + error.Line.ToString() + ": (" + error.ErrorNumber.ToString() + ")" + error.ErrorText, error.Line);
                }
                return null;
            }
            else
            {
                resultMessagesAndRows.Add("Compiled succesfully", -1);
                return results.CompiledAssembly;
            }
        }

        //This is a compilation done the CSScriptLibrary way
        //try
        //{
        //    Assembly assembly = CSScriptLibrary.CSScript.LoadCode(source, null, true);
        //    ListViewItem item2 = new ListViewItem("Compilation Succesfull", "ok");
        //    this.listView1.Items.Add(item2);
        //    return assembly;
        //}
        //catch (Exception ex)
        //{
        //    foreach(String line in ex.Message.Split(new char[] {}))
        //    {
        //        ListViewItem item = new ListViewItem(line, "error");
        //        this.listView1.Items.Add(item);
        //    }
        //    return null;
        //}


    }
}
