// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Core.Collections;
using Matrix.Common.Diagnostics.TracerCore.Filters;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore.Sinks
{
    /// <summary>
    /// Class defines common functionality for tracer item sink implementations.
    /// </summary>
    public abstract class TracerItemSink : ITracerItemSink
    {
        object _syncRoot = new object();

        /// <summary>
        /// Sync root object, used for lock synchronizations.
        /// </summary>
        public object SyncRoot
        {
            get { return _syncRoot; }
        }

        volatile bool _enabled = true;
        /// <summary>
        /// Sink enabled / disabled.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        bool _processNonFilteredOutItems = true;
        /// <summary>
        /// Sink accepts all or only filtered items.
        /// </summary>
        public bool ProcessNonFilteredOutItems
        {
            get { return _processNonFilteredOutItems; }
            set { _processNonFilteredOutItems = value; }
        }

        volatile Tracer _tracer;
        /// <summary>
        /// Sinks owner tracer.
        /// </summary>
        public Tracer Tracer
        {
            get { return _tracer; }
        }

        /// <summary>
        /// Filters specific for this sink.
        /// </summary>
        ListUnique<TracerFilter> _filters = new ListUnique<TracerFilter>();

        public TracerFilter[] FiltersArray
        {
            get
            {
                lock (_syncRoot)
                {
                    return _filters.ToArray();
                }
            }
        }

        public delegate void SinkUpdateDelegate(TracerItemSink tracer);

        [field: NonSerialized]
        public event SinkUpdateDelegate FilterUpdateEvent;

        /// <summary>
        /// 
        /// </summary>
        public TracerItemSink(Tracer tracer)
        {
            _tracer = tracer;
        }

        /// <summary>
        /// Override in child class to handle item acqustion.
        /// </summary>
        protected abstract bool OnReceiveItem(TracerItem item, bool isFilteredOutByTracer, bool isFilteredOutBySink);

        /// <summary>
        /// 
        /// </summary>
        public virtual bool ReceiveItem(TracerItem item, bool isFilteredOut)
        {
            if (_enabled && (isFilteredOut || _processNonFilteredOutItems))
            {
                bool filteredBySink = false;
                filteredBySink = !Tracer.FilterItem(FiltersArray, item);

                return OnReceiveItem(item, isFilteredOut, filteredBySink);
            }

            return true;
        }

        /// <summary>
        /// Add sink specific filter.
        /// It is up to the sinks implementation to how the filter will be used.
        /// </summary>
        public bool AddFilter(TracerFilter filter)
        {
            lock (_syncRoot)
            {
                if (_filters.Add(filter))
                {
                    filter.FilterUpdatedEvent += new TracerFilter.FilterUpdatedDelegate(filter_FilterUpdatedEvent);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearFilters()
        {
            foreach (TracerFilter filter in FiltersArray)
            {
                RemoveFilter(filter);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool RemoveFilter(TracerFilter filter)
        {
            lock (_syncRoot)
            {
                if (_filters.Remove(filter))
                {
                    filter.FilterUpdatedEvent -= new TracerFilter.FilterUpdatedDelegate(filter_FilterUpdatedEvent);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Clear()
        {
        }

        protected virtual void filter_FilterUpdatedEvent(TracerFilter filter)
        {
            if (FilterUpdateEvent != null)
            {
                FilterUpdateEvent(this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
            Clear();
            _tracer = null;
        }
    }
}
