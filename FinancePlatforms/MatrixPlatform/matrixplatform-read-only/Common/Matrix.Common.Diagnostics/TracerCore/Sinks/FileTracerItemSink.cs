// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Matrix.Common.Core.FileIO;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore.Sinks
{
    /// <summary>
    /// Class allows to trace trace item information to a text file.
    /// </summary>
    public class FileTracerItemSink : TracerItemSink
    {
        FileWriterHelper _fileWriter = new FileWriterHelper();

        /// <summary>
        /// Full path to the file that is to receive the trace information.
        /// </summary>
        public string FilePath
        {
            get { return _fileWriter.InitialFilePath; }
            set { Initialize(value); }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public FileTracerItemSink(Tracer tracer)
            : base(tracer)
        {
        }

        /// <summary>
        /// Detailed constructor.
        /// </summary>
        public FileTracerItemSink(Tracer tracer, string filePath)
            : base(tracer)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Perform the sink initialization, open file etc.
        /// </summary>
        protected void Initialize(string filePath)
        {
            _fileWriter.Initialize(filePath);
        }

        /// <summary>
        /// Free any resources.
        /// </summary>
        public override void Dispose()
        {
            FileWriterHelper fileWriter = _fileWriter;
            _fileWriter = null;
            if (fileWriter != null)
            {
                fileWriter.Dispose();
            }

            base.Dispose();
        }

        /// <summary>
        /// Received an item, sink it to file.
        /// </summary>
        protected override bool OnReceiveItem(TracerItem item, bool isFilteredOutByTracer, bool isFilteredOutBySink)
        {
            return _fileWriter.WriteLine(item.PrintFileLine());
        }
    }
}
