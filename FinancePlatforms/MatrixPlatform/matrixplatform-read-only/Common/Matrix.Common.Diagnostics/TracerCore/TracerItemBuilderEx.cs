// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore
{
    /// <summary>
    /// Extends the item builder functionality, by providing 
    /// a default way to link it to a tracer.
    /// 
    /// The tracer item builder can operate with no tracer 
    /// assigned, working only as a standalone builder.
    /// </summary>
    public class TracerItemBuilderEx : TracerItemBuilder
    {
        const bool Log4NetOutput = false;

        volatile object _owner;
        /// <summary>
        /// Owner instance of this builder.
        /// </summary>
        public object Owner
        {
            get { return _owner; }
        }

        //volatile bool _printOwnerInfoOnEachTrace = true;
        ///// <summary>
        ///// Should information on the owner be printed 
        ///// on the start of the message of each trace.
        ///// </summary>
        //public bool PrintOwnerInfoOnEachTrace
        //{
        //    get { return _printOwnerInfoOnEachTrace; }
        //    set { _printOwnerInfoOnEachTrace = value; }
        //}

        volatile Tracer _tracer;
        /// <summary>
        /// The tracer assigned with this builder.
        /// A builder can operate with no tracer assigned, working only as a standalone builder.
        /// </summary>
        public Tracer Tracer
        {
            get { return _tracer; }
            set { _tracer = value; }
        }

        volatile TracerItem.PriorityEnum _minimumPriority = TracerItem.PriorityEnum.Minimum;
        /// <summary>
        /// Minimum priority for items to pass trough the monitor.
        /// </summary>
        public TracerItem.PriorityEnum MinimumTracePriority
        {
            get { return _minimumPriority; }
            set { _minimumPriority = value; }
        }

        #region Log4Net Integration

        log4net.ILog _iLog = null;

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        public TracerItemBuilderEx(object owner)
        {
            _owner = owner;
            
            if (Log4NetOutput)
            {
                _iLog = log4net.LogManager.GetLogger(owner != null ? owner.GetType() : null);
            }
        }

        public override void Dispose()
        {
            _iLog = null;
            _owner = null;
            Tracer = null;

            base.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool TraceAllowed(TracerItem.PriorityEnum priority)
        {
            Tracer tracer = Tracer;
            if (tracer != null && tracer.Enabled == false)
            {// Stop all items here, since the tracer is not assigned.
                return false;
            }

            return (priority >= _minimumPriority);
        }

        protected override TracerItem CreateItem(bool extractMethodInformation, InstanceMonitor monitor,
            TracerItem.TypeEnum type, TracerItem.ExtendedTypeEnum extendedType, 
            TracerItem.PriorityEnum priority, string message, Exception exception)
        {
            object owner = _owner;
            //if (PrintOwnerInfoOnEachTrace && owner != null)
            //{
            //    message = owner.ToString() + "; " + message;
            //}

            Tracer tracer = _tracer;
            TracerItem item = base.CreateItem(extractMethodInformation, monitor, type, extendedType, priority, message, exception);
            if (item != null && tracer != null)
            {
                tracer.Add(item);
            }

            log4net.ILog iLog = _iLog;
            if (Log4NetOutput && iLog != null)
            {
                switch (type)
                {
                    case TracerItem.TypeEnum.Debug:
                        iLog.Debug(item, exception);
                        break;
                    case TracerItem.TypeEnum.Info:
                        iLog.Info(item, exception);
                        break;
                    case TracerItem.TypeEnum.Warning:
                        iLog.Warn(item, exception);
                        break;
                    case TracerItem.TypeEnum.Error:
                        iLog.Error(item, exception);
                        break;
                    case TracerItem.TypeEnum.Fatal:
                        iLog.Fatal(item, exception);
                        break;
                    default:
                        break;
                }
            }

            return item;
        }

    }
}
