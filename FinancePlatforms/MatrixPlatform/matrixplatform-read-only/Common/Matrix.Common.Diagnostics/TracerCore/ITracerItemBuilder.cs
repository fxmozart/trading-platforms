// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITracerItemBuilder : IDisposable
    {
        /// <summary>
        /// Report a piece of code is not implemented.
        /// </summary>
        /// <param name="parameters"></param>
        TracerItem NotImplementedError(params string[] parameters);

        /// <summary>
        /// Report a piece of code is not implemented, however missing functionality is not critical to operation.
        /// </summary>
        /// <param name="parameters"></param>
        TracerItem NotImplementedWarning(params string[] parameters);

        /// <summary>
        /// Check given condition, if true, report operation error with given message.
        /// Operation errors are a lighter version of a error, and may be expected to 
        /// occur during normal operation of the application (for. ex. a given non critical 
        /// resource was not retrieved, operation has timed out etc.)
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="errorMessage"></param>
        TracerItem OperationErrorIf(bool condition, string errorMessage);

        /// <summary>
        /// Chech given condition, if true, report operation warning with given message.
        /// Operation warnings are a lighter version of a warning, and may be expected to 
        /// occur during normal operaiton of the application.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="message"></param>
        TracerItem OperationWarningIf(bool condition, string message);

        /// <summary>
        /// 
        /// </summary>
        TracerItem OperationWarningIf(bool condition, string message, TracerItem.PriorityEnum priority);

        /// <summary>
        /// Check condition and report a warning, if condition met.
        /// Warnings serve to notify that something in operation of application has gone
        /// wrong, however the error is not critical.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="parameters"></param>
        TracerItem WarningIf(bool condition, params string[] parameters);

        /// <summary>
        /// 
        /// </summary>
        TracerItem Debug(string reportMessage);

        /// <summary>
        /// 
        /// </summary>
        TracerItem Debug(string reportMessage, Exception exception);

        /// <summary>
        /// Will report only if condition is true.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="reportMessage"></param>
        TracerItem InfoIf(bool condition, string reportMessage);

        /// <summary>
        /// 
        /// </summary>
        TracerItem Info(string reportMessage);

        /// <summary>
        /// Report a simplme message to the report management system.
        /// </summary>
        /// <param name="reportMessage"></param>
        TracerItem Info(string reportMessage, TracerItem.PriorityEnum priority);

        /// <summary>
        /// Report operation warning; it is a normal occurence in the work of the system. It can be caused
        /// for example by the lack of access to a resource or some error in a data stream.
        /// </summary>
        /// <param name="warningMessage"></param>
        TracerItem OperationWarning(string warningMessage);

        /// <summary>
        /// 
        /// </summary>
        TracerItem OperationWarning(string warningMessage, TracerItem.PriorityEnum priority);

        /// <summary>
        /// A Warning notifies that in some part of the systems operation a recovarable error has occured.
        /// </summary>
        /// <param name="warningMessage"></param>
        TracerItem Warning(string warningMessage);

        /// <summary>
        /// Report operation error with exception.
        /// </summary>
        /// <param name="ex"></param>
        TracerItem OperationError(string errorDetails, Exception exception);

        /// <summary>
        /// Report operation error; it is a lighter, more common version of a error, and may be expected to 
        /// occur during normal operation of the application (for. ex. a given non critical 
        /// resource was not retrieved, operation has timed out etc.)
        /// </summary>
        /// <param name="errorMessage"></param>
        TracerItem OperationError(string errorMessage);

        /// <summary>
        /// 
        /// </summary>
        TracerItem OperationError(string errorMessage, TracerItem.PriorityEnum priority);

        /// <summary>
        /// 
        /// </summary>
        TracerItem Fatal(string errorMessage);

        /// <summary>
        /// 
        /// </summary>
        TracerItem Fatal(string errorMessage, Exception exception);

        /// <summary>
        /// Perform a check on a condition. 
        /// If the conditions is true, set error.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="errorMessage"></param>
        TracerItem ErrorIf(bool condition);

        /// <summary>
        /// Perform a check on a condition. If the conditions is true set error with specified message.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="errorMessage"></param>
        TracerItem ErrorIf(bool condition, string errorMessage);
        
        /// <summary>
        /// Report an serious error. Those errors are usually a sign something in the work
        /// of the application has gone seriously wrong, and operation can not continue
        /// properly (for ex. unexpected exception, access to critical resources etc.)
        /// </summary>
        /// <param name="errorMessage"></param>
        TracerItem Error(string errorMessage);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        TracerItem Error(string errorMessage, TracerItem.PriorityEnum priority);

        /// <summary>
        /// Helper, redefine with exception consumption.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="exception"></param>
        TracerItem Error(string errorMessage, Exception exception);

    }
}
