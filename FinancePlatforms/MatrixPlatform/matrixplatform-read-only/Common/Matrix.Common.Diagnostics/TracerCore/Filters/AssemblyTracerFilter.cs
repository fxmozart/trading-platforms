// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;
//using System.Collections.ObjectModel;

//namespace Common.Extended
//{
//    /// <summary>
//    /// Filter for tracer item based on assembly of origin.
//    /// </summary>
//    [Serializable]
//    class AssemblyTracerFilter : TracerFilter
//    {
//        List<Assembly> _filteredOutAssemblies = new List<Assembly>();
//        /// <summary>
//        /// 
//        /// </summary>
//        public Assembly[] FilteredOutAssembliesArray
//        {
//            get { lock (this) { return _filteredOutAssemblies.ToArray(); } }
//        }

//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        /// <param name="tracer"></param>
//        public AssemblyTracerFilter(Tracer tracer)
//            : base(tracer)
//        {
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void SetAssemblyFiltered(Assembly assembly, bool filtered)
//        {
//            lock (this)
//            {
//                if (filtered)
//                {
//                    if (_filteredOutAssemblies.Contains(assembly) == false)
//                    {
//                        _filteredOutAssemblies.Add(assembly);
//                    }
//                }
//                else
//                {
//                    _filteredOutAssemblies.Remove(assembly);
//                }
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public override bool FilterItem(TracerItem item)
//        {
//            foreach (Assembly assembly in FilteredOutAssembliesArray)
//            {
//                if (item.Assembly == assembly)
//                {
//                    return false;
//                }
//            }
//            return true;
//        }
//    }
//}
