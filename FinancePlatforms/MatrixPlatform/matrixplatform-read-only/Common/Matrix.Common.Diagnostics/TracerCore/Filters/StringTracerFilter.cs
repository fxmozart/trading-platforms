// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore.Filters
{
    /// <summary>
    /// Filter for tracer items, based on string contents.
    /// </summary>
    [Serializable]
    public class StringTracerFilter : TracerFilter
    {
        volatile string _positiveFilterString = string.Empty;
        /// <summary>
        /// Only items containing this will be allowed.
        /// </summary>
        public string PositiveFilterString
        {
            get { return _positiveFilterString; }

            set
            {
                if (value != _positiveFilterString)
                {
                    _positiveFilterString = value;
                    RaiseFilterUpdatedEvent(false);
                }
            }
        }

        volatile List<string> _negativeFilterStrings = new List<string>();
        /// <summary>
        /// No items containing any of these will be allowed.
        /// </summary>
        public List<string> NegativeFilterStrings
        {
            get 
            {
                lock (_negativeFilterStrings)
                {
                    return new List<string>(_negativeFilterStrings);
                }
            }

            set 
            {
                _negativeFilterStrings = new List<string>(value);
                RaiseFilterUpdatedEvent(false);
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public StringTracerFilter()
        {
        }

        /// <summary>
        /// Deserialization constructor.
        /// </summary>
        public StringTracerFilter(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _positiveFilterString = info.GetString("_positiveFilterString");
            _negativeFilterStrings = (List<string>)info.GetValue("_negativeFilterStrings", typeof(List<string>));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("_positiveFilterString", _positiveFilterString);
            info.AddValue("_negativeFilterStrings", _negativeFilterStrings);
        }

        /// <summary>
        /// Obtain filtering data from other filter.
        /// </summary>
        public override void CopyDataFrom(TracerFilter otherFilter)
        {
            StringTracerFilter filter = otherFilter as StringTracerFilter;
            this._positiveFilterString = filter.PositiveFilterString;
            if (filter.NegativeFilterStrings == null)
            {
                this._negativeFilterStrings = null;
            }
            else
            {
                if (filter.NegativeFilterStrings != null)
                {
                    this._negativeFilterStrings = new List<string>(filter.NegativeFilterStrings);
                }
                else
                {
                    this._negativeFilterStrings = null;
                }
            }

            RaiseFilterUpdatedEvent(false);
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool FilterItem(TracerItem item)
        {
            if (string.IsNullOrEmpty(_positiveFilterString) == false || _negativeFilterStrings != null)
            {
                return FilterItem(item, _positiveFilterString, _negativeFilterStrings);
            }

            return true;
        }

        /// <summary>
        /// Perform the actual filtering of items, agains positive and negative filter strings.
        /// Will *lock the negativeFilterStrings*, if it finds it.
        /// </summary>
        /// <returns></returns>
        public static bool FilterItem(TracerItem item, string positiveFilterString, List<string> negativeFilterStrings)
        {
            string message = item.PrintMessage(true).ToLower();
            
            // Positive filter check.
            if (string.IsNullOrEmpty(positiveFilterString) == false
                && message.Contains(positiveFilterString.ToLower()) == false)
            {
                return false;
            }

            if (negativeFilterStrings != null)
            {
                lock (negativeFilterStrings)
                {
                    // Negative filter check.
                    foreach (string filter in negativeFilterStrings)
                    {
                        if (string.IsNullOrEmpty(filter) == false
                            && message.Contains(filter.ToLower()))
                        {
                            return false;
                        }
                    }
                }
            }

            // Pass.
            return true;
        }
    }
}
