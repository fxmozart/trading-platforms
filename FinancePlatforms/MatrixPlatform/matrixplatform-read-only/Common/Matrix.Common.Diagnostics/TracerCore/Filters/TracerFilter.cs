// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Runtime.Serialization;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore.Filters
{
    /// <summary>
    /// Base class for tracer filters. A filter is capable of filtering out trace
    /// items based on some criteria.
    /// </summary>
    public abstract class TracerFilter : ISerializable
    {
        Tracer _tracer;
        public Tracer Tracer
        {
            get { return _tracer; }
        }

        volatile bool _delayedUpdatePending = false;

        public delegate void FilterUpdatedDelegate(TracerFilter filter);
        public event FilterUpdatedDelegate FilterUpdatedEvent;

        #region Instance Control

        /// <summary>
        /// Constructor.
        /// </summary>
        public TracerFilter()
        {
        }

        /// <summary>
        /// Deserialization.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public TracerFilter(SerializationInfo info, StreamingContext context)
        {
        }

        /// <summary>
        /// Serialization.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Initialize(Tracer tracer)
        {
            if (_tracer != null)
            {
                SystemMonitor.OperationError("Filter already initialized.");
                return false;
            }

            _tracer = tracer;
            return true;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="otherFilter"></param>
        public virtual void CopyDataFrom(TracerFilter otherFilter)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        internal void PerformDelayedUpdateEvent()
        {
            if (_delayedUpdatePending)
            {
                _delayedUpdatePending = false;
                RaiseFilterUpdatedEvent(false);
            }
        }

        /// <summary>
        /// Raise event related to filter updated.
        /// </summary>
        /// <param name="delayedUpdate">Should we wait for the singaling call, that calls for all updates to be done (delayed update), or raise right away. Delayed usefull to prevent FilterItem lock issues.</param>
        protected void RaiseFilterUpdatedEvent(bool delayedUpdate)
        {
            if (delayedUpdate)
            {
                _delayedUpdatePending = true;
            }
            else
            {
                if (FilterUpdatedEvent != null)
                {
                    FilterUpdatedEvent(this);
                }
            }
        }

        /// <summary>
        /// Will return true if item is allowed to pass filter.
        /// </summary>
        public abstract bool FilterItem(TracerItem item);

    }
}
