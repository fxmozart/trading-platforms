// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics.TracerCore.Filters
{
    /// <summary>
    /// Filter applies filtering by item type fo the tracer items passed in.
    /// </summary>
    [Serializable]
    public class TypeTracerFilter : TracerFilter
    {
        Dictionary<TracerItem.TypeEnum, bool> _itemTypes = new Dictionary<TracerItem.TypeEnum, bool>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tracer"></param>
        public TypeTracerFilter()
        {
            // Load the basic types.
            Array items = Enum.GetValues(typeof(TracerItem.TypeEnum));
            for (int i = 0; i < items.Length; i++)
            {
                _itemTypes.Add((TracerItem.TypeEnum)items.GetValue(i), true);
            }
        }

        /// <summary>
        /// Deserialization constructor.
        /// </summary>
        public TypeTracerFilter(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Perform filtering of an item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool FilterItem(TracerItem item)
        {
            lock (this)
            {
                if (_itemTypes[item.Type])
                {
                    return true;
                }
            }

            return false;
        }

        public bool GetItemTypeFiltering(TracerItem.TypeEnum itemType)
        {
            lock (this)
            {
                if (_itemTypes.ContainsKey(itemType))
                {
                    return _itemTypes[itemType];
                }
            }

            SystemMonitor.Warning("Unknown item type introduced.");
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetItemTypesFiltering(TracerItem.TypeEnum[] itemTypes, bool[] filterings)
        {
            bool modified = false;
            lock (this)
            {
                for (int i = 0; i < itemTypes.Length; i++)
                {
                    if (_itemTypes.ContainsKey(itemTypes[i])
                        && _itemTypes[itemTypes[i]] == filterings[i])
                    {
                        continue;
                    }

                    modified = true;
                    _itemTypes[itemTypes[i]] = filterings[i];
                }

            }

            if (modified)
            {
                RaiseFilterUpdatedEvent(false);
            }
        }
    }
}
