// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Matrix.Common.Core.Collections;
using Matrix.Common.Diagnostics.TracerCore.Filters;
using Matrix.Common.Diagnostics.TracerCore.Items;
using Matrix.Common.Diagnostics.TracerCore.Sinks;

namespace Matrix.Common.Diagnostics.TracerCore
{
    /// <summary>
    /// Main Tracing manement class. Allows the tracing of messages, filtering, etc.
    /// Tracer does not store items, as it only passes them further to the sinks.
    /// To store items from tracer use the TracerItemKeeperSink.
    /// </summary>
    public class Tracer : IDisposable
    {
        volatile bool _enabled = true;
        /// <summary>
        /// Is the tracer enabled.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        /// <summary>
        /// The formats available for printing time information.
        /// </summary>
        public enum TimeDisplayFormatEnum : int
        {
            DateTime,
            ApplicationTicks,
            Combined
        }

        volatile TimeDisplayFormatEnum _timeDisplayFormat = TimeDisplayFormatEnum.ApplicationTicks;
        /// <summary>
        /// The format for printing time information.
        /// </summary>
        public TimeDisplayFormatEnum TimeDisplayFormat
        {
            get { return _timeDisplayFormat; }
            set { _timeDisplayFormat = value; }
        }

        long _totalItemsCount = 0;
        /// <summary>
        /// Count of all items ever passed trough this tracer.
        /// May be bigger than actual number of items stored in it.
        /// </summary>
        public long TotalItemsCount
        {
            get { return Interlocked.Read(ref _totalItemsCount); }
        }

        HotSwapList<ITracerItemSink> _itemSinksHotSwap = new HotSwapList<ITracerItemSink>();

        /// <summary>
        /// The collection of item sinks currently assigned to this tracer.
        /// </summary>
        public ReadOnlyCollection<ITracerItemSink> ItemSinks
        {
            get { return _itemSinksHotSwap.AsReadOnly(); }
        }

        HotSwapList<TracerFilter> _filtersHotSwap = new HotSwapList<TracerFilter>();
        
        /// <summary>
        /// A collection of all the filters currently applied to this tracer.
        /// </summary>
        public ReadOnlyCollection<TracerFilter> Filters
        {
            get { return _filtersHotSwap.AsReadOnly(); }
        }

        public delegate void TracerClearDelegate(Tracer tracer, bool completeClear);
        public delegate void ItemUpdateDelegate(Tracer tracer, bool filteredOut, TracerItem item);
        
        public event ItemUpdateDelegate ItemAddedEvent;
        public event TracerClearDelegate TracerClearedEvent;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Tracer()
        {
        }

        /// <summary>
        /// This will insert the tracer in the thread's data slots, making it possible
        /// for thread specific tracing to be done.
        /// </summary>
        /// <param name="thread"></param>
        public void AttachToThread(Thread thread)
        {
            throw new NotImplementedException("Operation not supported.");
        }

        /// <summary>
        /// Remove self as the trade's default tracer, inserted in the thread's data slot.
        /// </summary>
        /// <param name="thread"></param>
        public void DetachFromThread(Thread thread)
        {
            throw new NotImplementedException("Operation not supported.");
        }

        /// <summary>
        /// Helper, returns item sink by its type (if present in tracer).
        /// </summary>
        /// <param name="sinkType"></param>
        /// <returns></returns>
        public ITracerItemSink GetSinkByType(Type sinkType)
        {
            foreach (ITracerItemSink sink in ItemSinks)
            {
                if (sink.GetType() == sinkType)
                {
                    return sink;
                }
            }

            return null;
        }

        /// <summary>
        /// Will clear class filters and sinks (or optionaly also remove them).
        /// </summary>
        /// <param name="completeClear">Will also remove the sinks and filters. Pass false to only clear them.</param>
        public void Clear(bool completeClear)
        {
            foreach (ITracerItemSink sink in ItemSinks)
            {
                sink.Clear();
                if (completeClear)
                {
                    sink.Dispose();
                }
            }

            if (completeClear)
            {
                _itemSinksHotSwap.Clear();
                _filtersHotSwap.Clear();
            }

            if (TracerClearedEvent != null)
            {
                TracerClearedEvent(this, completeClear);
            }
        }

        /// <summary>
        /// Filter single item, passing it trough all filters.
        /// Will return true if item is allowed to pass filter.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="item"></param>
        public static bool FilterItem(IEnumerable<TracerFilter> filters, TracerItem item)
        {
            foreach (TracerFilter filter in filters)
            {
                if (filter.FilterItem(item) == false)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Filter single item, passing it trough all filters.
        /// </summary>
        public bool FilterItem(TracerItem item)
        {
            return FilterItem(Filters, item);
        }

        /// <summary>
        /// Add a new tracer filter to tracer.
        /// Make sure this is correct, since once filter in place, items will not be passed to sinks at all.
        /// </summary>
        public bool Add(TracerFilter filter)
        {
            if (filter.Initialize(this) == false)
            {
                return false;
            }

            return _filtersHotSwap.AddUnique(filter);
        }

        /// <summary>
        /// Add a tracer item sink to tracer.
        /// </summary>
        /// <param name="sink"></param>
        /// <returns></returns>
        public bool Add(ITracerItemSink sink)
        {
            return _itemSinksHotSwap.AddUnique(sink);
        }

        /// <summary>
        /// Remove tracer item sink from tracer.
        /// </summary>
        /// <param name="sink"></param>
        /// <returns></returns>
        public bool Remove(ITracerItemSink sink)
        {
            if (_itemSinksHotSwap.Remove(sink))
            {
                sink.Dispose();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Add a new tracer item to tracer.
        /// </summary>
        public void Add(TracerItem tracerItem)
        {
            if (this.Enabled == false)
            {
                return;
            }

            tracerItem.Index = Interlocked.Increment(ref _totalItemsCount) - 1;

            bool filtered = FilterItem(tracerItem);
            foreach (ITracerItemSink sink in ItemSinks)
            {
                sink.ReceiveItem(tracerItem, !filtered);
            }

            ItemUpdateDelegate del = ItemAddedEvent;
            if (del != null)
            {
                del(this, !filtered, tracerItem);
            }
        }


        #region IDisposable Members

        public void Dispose()
        {
            Clear(true);
        }

        #endregion
    }
}
