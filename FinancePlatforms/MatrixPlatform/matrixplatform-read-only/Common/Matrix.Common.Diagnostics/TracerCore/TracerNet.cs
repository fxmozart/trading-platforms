// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Text;
//using Common.Core;
//using System.Net;

//namespace Common.Extended
//{
//    /// <summary>
//    /// Extends the tracer class functionality, by providing networking capabilities
//    /// (both importing trace items from other sources, or exporting to).
//    /// </summary>
//    public class NetworkTracer : Tracer, ITracerItemSink
//    {
//        SocketMessageServer _server = null;
//        HotSwapDictionary<IPEndPoint, SocketMessageClient> _clientsHotSwap = new HotSwapDictionary<IPEndPoint, SocketMessageClient>();

//        public IEnumerable<SocketCommunicatorEx> ServerClients
//        {
//            get
//            {
//                SocketMessageServer server = _server;
//                if (server == null)
//                {
//                    return new SocketMessageClient[] { };
//                }

//                return server.ClientsOnly;
//            }
//        }

//        public bool IsServerStarted
//        {
//            get
//            {
//                SocketMessageServer server = _server;
//                if (server == null)
//                {
//                    return false;
//                }

//                return server.IsStarted;
//            }
//        }

//        public IEnumerable<SocketMessageClient> Clients
//        {
//            get
//            {
//                return _clientsHotSwap.Values;
//            }
//        }

//        object _syncRoot = new object();

//        public const int DefaultPort = 19627;

//        public delegate void TracerNetUpdateDelegate(NetworkTracer tracer);
//        public event TracerNetUpdateDelegate TracerNetUpdateEvent;

//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public NetworkTracer()
//        {
//        }

//        /// <summary>
//        /// Start a trace server, that provides trace items to all that connect to it.
//        /// </summary>
//        public bool StartTraceServer(int port)
//        {
//            SocketMessageServer server = _server;
//            lock (_syncRoot)
//            {
//                if (server != null)
//                {// Already started.
//                    return false;
//                }

//                _server = new SocketMessageServer(new BinaryMessageSerializer());
//                _server.ClientConnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(_server_ClientUpdateEvent);
//                _server.ClientDisconnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(_server_ClientUpdateEvent);

//                server = _server;
//            }

//            if (server.Start(new System.Net.IPEndPoint(System.Net.IPAddress.Any, port)) == false)
//            {
//                server.Dispose();
//                _server = null;
//                return false;
//            }

//            // Add this as item sink.
//            base.Add(this);

//            RaiseTracerNetUpdateEvent();

//            return true;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public bool StopTraceServer()
//        {
//            SocketMessageServer server;
//            lock (_syncRoot)
//            {
//                server = _server;
//                _server = null;
//            }

//            if (server == null)
//            {
//                return false;
//            }

//            // Remove this as item sink.
//            base.Remove(this);

//            server.Dispose();
//            RaiseTracerNetUpdateEvent();

//            return true;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public bool DisconnectTraceServerClient(SocketCommunicator communicator)
//        {
//            SocketMessageServer server;
//            lock (_syncRoot)
//            {
//                server = _server;
//                _server = null;
//            }

//            if (server == null)
//            {
//                return false;
//            }

//            return server.DisconnectClient(communicator.Id);
//        }

//        void _server_ClientUpdateEvent(SocketMessageServer server, SocketCommunicatorEx client)
//        {
//            RaiseTracerNetUpdateEvent();
//        }

//        protected void RaiseTracerNetUpdateEvent()
//        {
//            TracerNetUpdateDelegate del = TracerNetUpdateEvent;
//            if (del != null)
//            {
//                del(this);
//            }
//        }


//        /// <summary>
//        /// Start a trace listener to that remote address.
//        /// </summary>
//        /// <param name="remoteEndPoint"></param>
//        /// <returns></returns>
//        public bool StartTraceListen(IPEndPoint remoteEndPoint)
//        {
//            SocketMessageClient client = new SocketMessageClient();
//            if (_clientsHotSwap.TryAddValue(remoteEndPoint, client) == false)
//            {
//                // Failed to add listener to that end point.
//                return false;
//            }

//            if (client.ConnectAsync(remoteEndPoint) == false)
//            {
//                _clientsHotSwap.Remove(remoteEndPoint);
//                client.Dispose();
//                return false;
//            }

//            client.AutoReconnect = true;
//            client.MessageReceivedEvent += new SocketCommunicator.MessageUpdateDelegate(client_MessageReceivedEvent);
//            client.ConnectedEvent += new SocketCommunicator.HelperUpdateDelegate(client_UpdatedEvent);
//            client.DisconnectedEvent += new SocketCommunicator.HelperUpdateDelegate(client_UpdatedEvent);

//            RaiseTracerNetUpdateEvent();

//            return true;
//        }

//        void client_UpdatedEvent(SocketCommunicator helper)
//        {
//            RaiseTracerNetUpdateEvent();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public bool StopTraceListen(IPEndPoint remoteEndPoint)
//        {
//            SocketMessageClient client = new SocketMessageClient();
//            if (_clientsHotSwap.TryGetValue(remoteEndPoint, out client) == false)
//            {
//                // Failed to add listener to that end point.
//                return false;
//            }

//            if (_clientsHotSwap.Remove(remoteEndPoint) == false)
//            {// Failed to remove it.
//                return false;
//            }

//            client.MessageReceivedEvent -= new SocketCommunicator.MessageUpdateDelegate(client_MessageReceivedEvent);
//            client.ConnectedEvent -= new SocketCommunicator.HelperUpdateDelegate(client_UpdatedEvent);
//            client.DisconnectedEvent -= new SocketCommunicator.HelperUpdateDelegate(client_UpdatedEvent);

//            client.Dispose();

//            RaiseTracerNetUpdateEvent();

//            return true;
//        }

//        /// <summary>
//        /// Received a trace from remote listener.
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="message"></param>
//        void client_MessageReceivedEvent(SocketCommunicator helper, object message)
//        {
//            if (message is TracerItem == false)
//            {
//                return;
//            }

//            TracerItem item = message as TracerItem;
//            item.SourceId = helper.EndPoint.ToString();
//            base.Add(item);
//        }

//        #region ITracerItemSink Members

//        public bool ReceiveItem(TracerItem item, bool isFilteredOut)
//        {
//            //if (isFilteredOut == false)
//            {
//                SocketMessageServer server = _server;
//                if (server != null)
//                {
//                    server.SendAsync(item, null);
//                }
//            }

//            return true;
//        }

//        public void Clear()
//        {
            
//        }

//        #endregion
//    }
//}
