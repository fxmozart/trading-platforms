// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;

namespace Matrix.Common.Diagnostics.TracerCore.Items
{
    /// <summary>
    /// Tracer item coming from a baseMethod tracing information.
    /// </summary>
    [Serializable]
    public class MethodTracerItem : TracerItem, IDeserializationCallback
    {
        [NonSerialized]
        MethodBase _methodBase;

        /// <summary>
        /// Method that posted the item. Not serialized, since 
        /// receiving side may have no knowledge of this class.
        /// </summary>
        protected MethodBase MethodBase
        {
            get { return _methodBase; }
            set
            {
                _methodBase = value;
                if (value != null)
                {
                    DeclaringTypeModuleName = value.DeclaringType.Module.Name;
                    DeclaringMethodName = value.Name;
                    
                    DeclaringTypeName = value.DeclaringType.Name;
                    DeclaringAssemblyName = value.DeclaringType.Assembly.GetName().Name;
                }
            }
        }

        public string DeclaringMethodName { get; protected set; }
        public string DeclaringTypeModuleName { get; protected set; }
        public string DeclaringTypeName { get; protected set; }

        public string DeclaringAssemblyName { get; protected set; }

        ///// <summary>
        ///// Assembly that posted this entry.
        ///// </summary>
        //public override System.Reflection.Assembly DeclaringAssembly
        //{
        //    get 
        //    { 
        //        MethodBase methodBase = _methodBase;
        //        if (methodBase != null)
        //        {
        //            return methodBase.DeclaringType.Assembly;
        //        }

        //        return null;
        //    }
        //}

        ///// <summary>
        ///// Declaring type that posted this entry (if available).
        ///// </summary>
        //public Type DeclaringType
        //{
        //    get
        //    {
        //        MethodBase methodBase = _methodBase;
        //        if (methodBase != null)
        //        {
        //            return methodBase.DeclaringType;
        //        }

        //        return null;
        //    }
        //}
        
        volatile string _threadName = string.Empty;
        /// <summary>
        /// Name of the thread that placed this trace entry (if provided).
        /// </summary>
        public string ThreadName
        {
            get { return _threadName; }
        }

        volatile string _threadId = string.Empty;
        /// <summary>
        /// Id of the thread that placed this entry (if provided).
        /// </summary>
        public string ThreadId
        {
            get { return _threadId; }
            set { _threadId = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MethodTracerItem(TypeEnum itemType, TracerItem.ExtendedTypeEnum extendedType, TracerItem.PriorityEnum priority, string message, string source, MethodBase methodInfo)
            : base(itemType, extendedType, priority, message, source)
        {
            MethodBase = methodInfo;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MethodTracerItem(TypeEnum itemType, TracerItem.ExtendedTypeEnum extendedType, TracerItem.PriorityEnum priority, string message, string source, MethodBase methodInfo, string threadName, string threadId)
            : base(itemType, extendedType, priority, message, source)
        {
            MethodBase = methodInfo;
            _threadId = threadId;
            _threadName = threadName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        public void OnDeserialization(object sender)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        public override string PrintMessage(bool includeOptionalException)
        {
            MethodBase methodBase = _methodBase;

            //string assemblyName = MethodBaseDeclaringTypeAssemblyFullName, fullMethodName = string.Empty;
            if (methodBase != null)
            {
                //assemblyName = _methodBase.DeclaringType.Assembly.GetName().Name;
                //fullMethodName = assemblyName + "." + _methodBase.DeclaringType.Name + "." + _methodBase.Name;
            }

            string threadInfo = string.Format("{0},{1}", _threadId, _threadName);
            threadInfo = threadInfo.Trim(',');

            return threadInfo + " " + DeclaringTypeModuleName + "." + DeclaringTypeName + "." + DeclaringMethodName + " " + base.PrintMessage(includeOptionalException);
        }

    }
}
