// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.ObjectModel;
using Matrix.Common.Diagnostics.TracerCore;
using Matrix.Common.Core.Collections;
using Matrix.Common.Diagnostics.TracerCore.Filters;
using Matrix.Common.Diagnostics.TracerCore.Items;

namespace Matrix.Common.Diagnostics
{
    /// <summary>
    /// Class provides a way for a specific class instance to control
    /// its access to tracing and system monitoring resources, and
    /// configure the conditions of this access.
    /// </summary>
    public class InstanceMonitor : TracerItemBuilderEx, IDisposable
    {
        HotSwapList<TracerFilter> _filtersHotSwap = new HotSwapList<TracerFilter>();
        
        /// <summary>
        /// Element thread safe, and properly handled, so use directly.
        /// </summary>
        public ReadOnlyCollection<TracerFilter> FiltersHotSwap
        {
            get { return _filtersHotSwap.AsReadOnly(); }
        }

        /// <summary>
        /// Helper, is the default Report() operation allowed to pass trough.
        /// </summary>
        public bool IsReportAllowed
        {
            get
            {
                return TraceAllowed(DefaultReportPriority);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReportImportantAllowed
        {
            get
            {
                return TraceAllowed(TracerItem.PriorityEnum.High);
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public InstanceMonitor(object owner)
            : base(owner)
        {
            base.MethodInfoLoopback = 6;
            this.Tracer = SystemMonitor.Tracer;
            SystemMonitor.TracerAssignedEvent += new SystemMonitor.TracerUpdateDelegate(SystemMonitor_TracerAssignedEvent);
        }

        void SystemMonitor_TracerAssignedEvent(Tracer tracer)
        {
            this.Tracer = SystemMonitor.Tracer;
        }

        /// <summary>
        /// Dispose, not mandatory.
        /// </summary>
        public override void Dispose()
        {
            SystemMonitor.TracerAssignedEvent -= new SystemMonitor.TracerUpdateDelegate(SystemMonitor_TracerAssignedEvent);
            base.Dispose();
        }

        /// <summary>
        /// Add owner info trace capabilities to each item printed and filter based on minimum priority.
        /// </summary>
        protected override TracerItem CreateItem(bool extractMethodInformation, InstanceMonitor monitor,
                                                 TracerItem.TypeEnum type, TracerItem.ExtendedTypeEnum extendedType, TracerItem.PriorityEnum priority, string message, Exception exception)
        {
            if (TraceAllowed(priority) == false)
            {// Item below minimum required priority.
                return null;
            }

            TracerItem item = base.CreateItem(extractMethodInformation, this, type, extendedType, priority, message, exception);
            //item.Source = this.ToString();
            return item;
        }



    }
}
