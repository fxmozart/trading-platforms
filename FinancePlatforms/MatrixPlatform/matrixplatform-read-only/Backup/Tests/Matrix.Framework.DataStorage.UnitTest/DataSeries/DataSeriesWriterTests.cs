// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.DataStorage.DataSeries;
using NUnit.Framework;

namespace Matrix.Framework.DataStorage.UnitTest.DataSeries
{
    /// <summary>
    /// 
    /// </summary>
    [TestFixture]
    public class DataSeriesWriterTest
    {

        public const string FilePath = @"n:\variable.ser";
        //public const string FilePath = @"n:\fixed.ser";

        /// <summary>
        /// Constructor.
        /// </summary>
        public DataSeriesWriterTest()
        {
        }

        [TestFixtureSetUp]
        public void Init()
        {
        }

        [TestFixtureTearDown]
        public void UnInit()
        {
        }

        public static IItemSerializer CreateSerializer()
        {
            return new FixedItemSerializerHelper();
            //return new VariableSerializerHelper();
        }

        /// <summary>
        /// Input parameter in thousands.
        /// </summary>
        /// <param name="valuesCountThousands"></param>
        [Test]
        public void WriteAndContinue([Values(10, 50, 100, 1000, 10000, 50000, 150000)] int valuesCountThousands)
        {
            using (DataSeriesWriter writer = new DataSeriesWriter())
            {
                if (writer.Initalize(CreateSerializer(), FilePath, true) == false)
                {
                    throw new OperationCanceledException("Writer init failed.");
                }

                uint writeStartIndex = writer.ItemIndex;
                for (int i = 0; i < valuesCountThousands; i++)
                {
                    List<object> values = new List<object>();
                    for (int j = 0; j < 1000; j++)
                    {
                        values.Add(i * 1000 + j + (int)writeStartIndex);
                    }

                    writer.Write(values);
                }
            }
        }

        [Test]
        public void WriteOne()
        {
            using (DataSeriesWriter writer = new DataSeriesWriter())
            {
                if (writer.Initalize(CreateSerializer(), FilePath, false) == false)
                {
                    throw new OperationCanceledException("Writer init failed.");
                }

                writer.Write(new object[] { 1 });
            }
        }

        /// <summary>
        /// Input parameter in thousands.
        /// </summary>
        /// <param name="valuesCountThousands"></param>
        [Test]
        public void Write([Values(10, 50, 100, 1000, 10000, 50000, 150000)] int valuesCountThousands)
        {
            using (DataSeriesWriter writer = new DataSeriesWriter())
            {
                if (writer.Initalize(CreateSerializer(), FilePath, false) == false)
                {
                    throw new OperationCanceledException("Writer init failed.");
                }

                List<object> values = new List<object>();
                for (int i = 0; i < valuesCountThousands; i++)
                {
                    for (int j = 0; j < 1000; j++)
                    {
                        values.Add(i * 1000 + j);
                    }

                    writer.Write(values);
                    values.Clear();
                }
            }

        }

        //[Test]
        //public void Write2([ValuesAttribute(10, 50, 100, 1000, 10000, 50000, 150000)] int valuesCountThousands)
        //{
        //    using (DataSeriesWriter writer = new DataSeriesWriter())
        //    {
        //        if (writer.Initalize(CreateSerializer(), FilePath, false) == false)
        //        {
        //            throw new OperationCanceledException("Writer init failed.");
        //        }

        //        List<object> values = new List<object>();

        //        for (int i = 0; i < valuesCountThousands; i++)
        //        {
        //            for (int j = 0; j < 1000; j++)
        //            {
        //                values.Add(i * 1000 + j);
        //            }

        //            if (values.Count > 50000)
        //            {
        //                writer.Write2(values);
        //                values.Clear();
        //            }
        //        }

        //        if (values.Count > 50000)
        //        {
        //            writer.Write2(values);
        //            values.Clear();
        //        }
        //    }

        //}

    }
}
