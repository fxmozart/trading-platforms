// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Threading;
using System.Reflection;
using Matrix.Common.Core.Serialization;
using Matrix.Framework.TestFramework;
using Matrix.Framework.SuperPool.Call;

namespace Matrix.Framework.SuperPool.Test.SpeedTests
{
    /// <summary>
    /// Test gives around 17-18K at current config.
    /// Much higher speed is achieved when using Duplicate(), in the millions.
    /// </summary>
    class SuperPoolCallSerializationSpeedTest : SpeedTest
    {
        /// <summary>
        /// 
        /// </summary>
        public SuperPoolCallSerializationSpeedTest()
            : base(false)
        {
        }

        public override void Update(FormTesting form)
        {
        }

        void Initialize()
        {
        }

        public override bool OnRun(FormTesting form, int count)
        {
            SuperPoolCall call = new SuperPoolCall(1712);

            // Generate a typical call.
            call.MethodInfoLocal = (MethodInfo)MethodInfo.GetCurrentMethod();
            call.Parameters = new object[] { "asdwdas", 85923, EventArgs.Empty };
            call.RequestResponse = false;
            call.State = SuperPoolCall.StateEnum.Finished;

            object result;
            for (_executed = 0; _executed < count; _executed++)
            {
                result = SerializationHelper.BinaryClone(call);
            }

            return true;
        }

    }
}
