// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Threading;
using Matrix.Common.Core;
using Matrix.Common.Diagnostics;
using Matrix.Common.Extended;
using Matrix.Framework.TestFramework;
using Matrix.Framework.MessageBus.Net;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.SuperPool.Core;

namespace Matrix.Framework.SuperPool.Test.SpeedTests
{
    /// <summary>
    /// Speed of event invocation runs currently at about 400K.
    /// </summary>
    class WorkingTest : SpeedTest, I2, I3
    {
        Matrix.Framework.SuperPool.Core.SuperPool pool = null;
        SuperPoolClient client1;
        SuperPoolClient client2;

        X2 x2 = new X2();

        /// <summary>
        /// 
        /// </summary>
        public WorkingTest()
            : base(false)
        {

        }

        public override void Update(FormTesting form)
        {
        }

        void Initialize()
        {
            if (pool != null)
            {
                return;
            }

            pool = new Matrix.Framework.SuperPool.Core.SuperPool(new ServerMessageBus("TestBus1", null, null));
            client1 = new SuperPoolClient("c1", this);

            pool.AddClient(client1);

            client2 = new SuperPoolClient("c2", x2);
            pool.AddClient(client2);

            client2.Subscribe<I2>(client1.Id).AEven += new ADel(x2.SuperPoolConsole_AEven);
            client2.Subscribe<I2>(client1.Id).AEven2 += new ADel(x2.SuperPoolConsole_AEvenSpeed);
        }

        public override bool OnRun(FormTesting form, int count)
        {
            Initialize();

            int a = 0;
            //client2.CallSyncFirst<I3>(TimeSpan.FromSeconds(20)).M2(a);
            client2.CallAll<I3>().M2(a);

            //// Multi call test.
            //for (int i = 0; i < count; i++)
            //{
            //    client2.Call<I2>(client1.Id).Method();
            //}

            //SystemMonitor.Report(" << SuperPoolConsole.AEvent " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString());
            //for (int i = 0; i < 100; i++)
            //{
            //    //SystemMonitor.Report(" << " + i);

            //    AEven(i, GeneralHelper.ApplicationStopwatchTicks);

            //    Thread.Sleep(1000);
            //}

            {// Event test.
                //x2.Target = count;

                //for (int i = 0; i < count; i++)
                //{
                //    AEven2(i, GeneralHelper.ApplicationStopwatchTicks);
                //}

                //x2.ReadyEvent.WaitOne();
            }

            //SystemMonitor.Report(" << SuperPoolConsole.AEvent2 " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString());
            //AEven2(11);
            //AEven2(11);

            //AEven(11);
            //x2.Raise(11);

            //client2.Source = null;

            //pool.Dispose();


            return true;
        }


        #region I2 Members

        public event ADel AEven;

        public event ADel AEven2;

        public void Method()
        {
            base.IncrementExecuted();
        }

        #endregion

        #region I3 Members

        public void M2(params int[] paramsa)
        {
            
        }

        #endregion

    }

    public delegate int ADel(int x, long time);

    [SuperPoolInterface]
    public interface I3
    {
        void M2(params int[] paramsa);
    }

    [SuperPoolInterface]
    public interface I2
    {
        event ADel AEven;
        event ADel AEven2;
        void Method();
    }

    public class X2 : I2
    {
        public event ADel AEven;

        public int Count = 0;

        public int Target = -1;

        public ManualResetEvent ReadyEvent = new ManualResetEvent(false);

        public void Method()
        {
            Thread.Sleep(10000);
        }

        public int SuperPoolConsole_AEven(int x, long time)
        {
            long diff = ApplicationLifetimeHelper.ApplicationStopwatchTicks - time;
            double diffM = GeneralHelper.TicksToMilliseconds(diff);
            SystemMonitor.Info(" >> " + x + " for " + diffM.ToString("###.###"));
            return 0;
        }

        public int SuperPoolConsole_AEvenSpeed(int x, long time)
        {
            if (Interlocked.Increment(ref Count) == Target)
            {
                ReadyEvent.Set();
            }

            return 0;
        }

        public void Raise(int x)
        {
            AEven(x, 0);
        }

        #region I2 Members

        public event ADel AEven2;

        #endregion
    }
}
