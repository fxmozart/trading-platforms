// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading;
using Matrix.Common.Core;
using Matrix.Framework.MessageBus.Clients;
using Matrix.Framework.MessageBus.Core;
using Matrix.Framework.MessageBus.Net;
using NUnit.Framework;

namespace Matrix.Framework.MessageBus.UnitTest
{
    /// <summary>
    /// Speed currently around 3K per second.
    /// </summary>
    [TestFixture]
    public class MessageBusNetTest
    {
        ServerMessageBus _bus;
        ClientMessageBus _proxy;

        ActiveInvocatorClient _client1, _client2;

        int _targetLength = 0;

        ManualResetEvent _sendFinishedEvent = new ManualResetEvent(false);
        ManualResetEvent _sendFinishedEvent2 = new ManualResetEvent(false);

        int _received = 0;
        int _received2 = 0;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MessageBusNetTest()
        {
        }

        [TestFixtureSetUp]
        public void Init()
        {
            _bus = new ServerMessageBus("Bus1", 15726, null);
            _proxy = new ClientMessageBus(new IPEndPoint(System.Net.IPAddress.Loopback, 15726), string.Empty, null);

            _client1 = new ActiveInvocatorClient("c1");
            _client1.EnvelopeReceivedEvent += new MessageBusClient.EnvelopeUpdateDelegate(client_EnvelopeReceivedEvent);

            _bus.AddClient(_client1);

            _client2 = new ActiveInvocatorClient("c2");
            _client2.EnvelopeReceivedEvent += new MessageBusClient.EnvelopeUpdateDelegate(client_EnvelopeReceivedEvent2);

            _proxy.AddClient(_client2);

            // Some time for the TCP to connect.
            Thread.Sleep(2000);
        }

        [TestFixtureTearDown]
        public void UnInit()
        {
            _bus.Dispose();
            _bus = null;

            _proxy.Dispose();
            _proxy = null;

            ApplicationLifetimeHelper.SetApplicationClosing();
        }

        [Test]
        public void CombinedTest([Values(10000, 35000, 100000, 1000000)] int length)
        {
            _sendFinishedEvent.Reset();
            _sendFinishedEvent2.Reset();

            _targetLength = length;
            _received = 0;
            _received2 = 0;

            bool errored = false;
            Envelope lope;
            for (int i = 0; i < length; i++)
            {
                lope = new Envelope() { Message = i };
                if (_proxy.Send(_client2.Id, _client1.Id, lope, null, true) != OutcomeEnum.Success)
                {
                    errored = true;
                    Console.WriteLine("Failed to send 1 - " + i);
                }

                if (_bus.Send(_client1.Id, _client2.Id, lope, null, true) != OutcomeEnum.Success)
                {
                    errored = true;
                    Console.WriteLine("Failed to send 2 - " + i);
                }
            }

            if (errored == false)
            {
                _sendFinishedEvent.WaitOne();
                _sendFinishedEvent2.WaitOne();
            }
        }

        void client_EnvelopeReceivedEvent(MessageBusClient stub, Envelope envelope)
        {
            if (Interlocked.Increment(ref _received) >= _targetLength)
            {
                Console.WriteLine(stub.Id.Name + " executed " + _received.ToString());
                _sendFinishedEvent.Set();
            }

        }

        void client_EnvelopeReceivedEvent2(MessageBusClient stub, Envelope envelope)
        {
            if (Interlocked.Increment(ref _received2) >= _targetLength)
            {
                Console.WriteLine(stub.Id.Name + " executed " + _received2.ToString());
                _sendFinishedEvent2.Set();
            }

        }
    }
}
