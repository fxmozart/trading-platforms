// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Diagnostics;

//namespace VariousTests
//{
//    /// <summary>
//    /// Test with multiple threads.
//    /// </summary>
//    public abstract class MultiThreadTest
//    {
//        bool _result = true;
//        FormTesting _form;

//        bool _running = true;
//        /// <summary>
//        /// 
//        /// </summary>
//        public bool Running
//        {
//            get { return _running; }
//        }
       
//        long _stepId = 0;
//        /// <summary>
//        /// 
//        /// </summary>
//        public long StepsPerformed
//        {
//            get { return _stepId; }
//        }

//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public MultiThreadTest()
//        {
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public bool Run(FormTesting form, int threadCount, TimeSpan period)
//        {
//            _form = form;
//            _running = true;
//            _result = true;
//            _stepId = 0;

//            OnTestStart();
//            for (int i = 0; i < threadCount; i++)
//            {
//                ThreadPool.QueueUserWorkItem(ExecutionCallback);
//            }

//            Thread.Sleep(period);
//            _running = false;

//            OnTestEnd();

//            return _result;
//        }

//        void ExecutionCallback(object state)
//        {
//            try
//            {
//                while (_running)
//                {
//                    OnExecuteStep(_form, Interlocked.Increment(ref _stepId));
//                }
//            }
//            catch (Exception ex)
//            {
//                _form.ReportMain(GeneralHelper.GetExceptionMessage(ex));
//                _result = false;
//            }
//        }

//        public virtual void Update(FormTesting form)
//        {
//        }

//        protected virtual void OnTestStart()
//        {
//        }

//        protected virtual void OnTestEnd()
//        {
//        }

//        protected abstract void OnExecuteStep(FormTesting form, long stepId);
//    }
//}
