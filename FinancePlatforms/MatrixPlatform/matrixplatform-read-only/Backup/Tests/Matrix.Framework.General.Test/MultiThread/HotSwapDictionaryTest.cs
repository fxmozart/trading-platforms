// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using Matrix.Common.Core.Collections;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.MultiThread
{
    /// <summary>
    /// 
    /// </summary>
    public class HotSwapDictionaryTest : MultiThreadTest
    {
        HotSwapDictionary<long, long> _instance = new HotSwapDictionary<long, long>();

        long _desiredItemsCount = 10;
        /// <summary>
        /// 
        /// </summary>
        public long DesiredItemsCount
        {
            get { return _desiredItemsCount; }
            set { _desiredItemsCount = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public HotSwapDictionaryTest()
        {
        }

        protected override void OnTestStart()
        {
            _instance.Clear();
            _instance.Add(89, 91);
        }

        protected override void OnTestEnd()
        {
            _instance.Clear();
        }

        public override void Update(FormTesting form)
        {
            form.ReportSecondary(_instance.Count + " items in list");
        }

        protected override void OnExecuteStep(FormTesting form, long stepId)
        {
            double x = 0;
            //lock (this)
            {
                foreach (long value in _instance.Values)
                {
                    x += value;
                }
            }

            
            HotSwapDictionary<long, long> instance = _instance;
            if (instance.ContainsKey(89))
            {
                long t = instance[89];
            }

            if (stepId % 1200 == 0)
            {
                if (_instance.Count <= _desiredItemsCount)
                {
                    //lock (_list)
                    {
                        _instance.Add(stepId, 71);
                    }
                }
            }
            else if (stepId % 1200 == 60)
            {
                if (_instance.Count >= _desiredItemsCount)
                {
                    foreach(long value in _instance.Values)
                    {
                        _instance.Remove(value);
                    }
                }
            }
        }
    }
}
