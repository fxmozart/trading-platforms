// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.TestFramework;
using System.Drawing;

namespace Matrix.Framework.General.Test.Speed
{
    public class Drawing2DMatrixTest : SpeedTest
    {
        bool _useDefault = false;
        public bool UseDefault
        {
            get { return _useDefault; }
            set { _useDefault = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Drawing2DMatrixTest()
            : base(false)
        {

        }

        public override void Update(FormTesting form)
        {

        }

        protected static void TransformPoint(System.Drawing.Drawing2D.Matrix matrix, ref PointF point)
        {
            // Formula.
            // p.x = mat[0][0] * pt.x + mat[0][1] * pt.y + mat[0][2];
            // p.y = mat[1][0] * pt.x + mat[1][1] * pt.y + mat[1][2];

            float[] elements = matrix.Elements;
            // *Important* stores the value, otherwise changed in first calculation.
            float x = point.X; 
            point.X = elements[0] * point.X + elements[2] * point.Y + elements[4];
            point.Y = elements[1] * x + elements[3] * point.Y + elements[5];
        }

        /// <summary>
        /// Only the scale and rotate.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="point"></param>
        protected static void TransformVector(System.Drawing.Drawing2D.Matrix matrix, ref PointF point)
        {
            // Formula.
            // p.x = mat[0][0] * pt.x + mat[0][1] * pt.y;
            // p.y = mat[1][0] * pt.x + mat[1][1] * pt.y;

            float[] elements = matrix.Elements;
            // *Important* stores the value, otherwise changed in first calculation.
            float x = point.X;
            point.X = elements[0] * point.X + elements[2] * point.Y;
            point.Y = elements[1] * x + elements[3] * point.Y;
        }

        public override bool OnRun(FormTesting form, int count)
        {
            //System.Drawing.Drawing2D.Matrix matrix = new System.Drawing.Drawing2D.Matrix(14.412f, 0.561f, 0, -791273.2f, 8.45129f, 150298.5f);
            System.Drawing.Drawing2D.Matrix matrix = new System.Drawing.Drawing2D.Matrix();
            matrix.Rotate(30);
            matrix.Translate(30, 70);

            System.Drawing.PointF point = new System.Drawing.PointF(123.612f, 6123.12312f);

            //TransformPoint(matrix, ref point);

            {// Verification - Points...
                PointF[] points = new System.Drawing.PointF[] { point };
                matrix.TransformPoints(points);
                System.Drawing.PointF p1 = points[0];

                System.Drawing.PointF p2 = point;
                TransformPoint(matrix, ref p2);

                // A rough compare
                if ((int)p1.X != (int)p2.X || (int)p1.Y != (int)p2.Y)
                {
                    throw new Exception("Fail.");
                }
            }

            {// Verification - Vectors...
                PointF[] points = new System.Drawing.PointF[] { point };
                matrix.TransformVectors(points);
                System.Drawing.PointF p1 = points[0];

                System.Drawing.PointF p2 = point;
                TransformVector(matrix, ref p2);

                // A rough compare
                if ((int)p1.X != (int)p2.X || (int)p1.Y != (int)p2.Y)
                {
                    throw new Exception("Fail.");
                }
            }

            if (_useDefault)
            {
                for (int i = 0; i < count; i++)
                {
                    matrix.TransformPoints(new System.Drawing.PointF[] { point });
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    TransformPoint(matrix, ref point);
                }
            }


            return true;
        }
    }
}
