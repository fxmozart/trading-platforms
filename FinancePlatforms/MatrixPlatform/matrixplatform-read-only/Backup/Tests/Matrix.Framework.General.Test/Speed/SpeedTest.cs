// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;

//namespace VariousTests
//{
//    /// <summary>
//    /// Test base class.
//    /// </summary>
//    public abstract class SpeedTest
//    {
//        protected int _executed = 0;
//        /// <summary>
//        /// 
//        /// </summary>
//        public int Executed
//        {
//            get { return _executed; }
//        }

//        protected int Count { get; set; }

//        protected AutoResetEvent ExecutionFinishedEvent { get; set; }

//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public SpeedTest(bool waitsForExecutionFinishSignal)
//        {
//            if (waitsForExecutionFinishSignal)
//            {
//                ExecutionFinishedEvent = new AutoResetEvent(false);
//            }
//        }

//        public abstract bool OnRun(FormTesting form, int count);
//        public abstract void Update(FormTesting form);
        
//        /// <summary>
//        /// 
//        /// </summary>
//        public bool Run(FormTesting form, int count)
//        {
//            form.ReportSecondary(string.Empty);
//            form.ReportSecondary("------------");

//            Count = count;
//            _executed = 0;
//            bool result = OnRun(form, count);

//            if (ExecutionFinishedEvent != null)
//            {
//                ExecutionFinishedEvent.WaitOne();
//            }

//            return result;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        protected int IncrementExecuted()
//        {
//            int result = Interlocked.Increment(ref _executed);
//            if (_executed >= Count)
//            {
//                SignalTestComplete();
//            }

//            return result;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        protected void SignalTestComplete()
//        {
//            ExecutionFinishedEvent.Set();
//        }


//        public virtual void OnTestFinished(FormTesting form)
//        {

//        }
//    }
//}
