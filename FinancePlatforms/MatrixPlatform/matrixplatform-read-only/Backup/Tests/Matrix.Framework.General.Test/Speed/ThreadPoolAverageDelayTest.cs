// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using Matrix.Common.Core;
using Matrix.Common.Extended;
using Matrix.Common.Extended.ThreadPools;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.Speed
{
    /// <summary>
    /// The current implementation gives around (0.015) ms average delay 
    /// of run to execution, so fast enough (as tested in Debug).
    /// 
    /// The old pool was about this:
    /// Launching trough the ThreadPool gives a delay of about 0.05ms or 1/20,000 of a second
    /// if the thread pool is not overloaded and working properly.
    /// </summary>
    public class ThreadPoolAverageDelayTest : SpeedTest
    {
        ThreadPoolFastEx _threadPool = new ThreadPoolFastEx("TestPool");

        long _lastDelaySpotted = 0;

        FastInvokeHelper.FastInvokeHandlerDelegate _singleFastInvokeDelegate;

        int _interTestIntervalMilliseconds = 50;
        /// <summary>
        /// 
        /// </summary>
        public int InterTestIntervalMilliseconds
        {
            get { return _interTestIntervalMilliseconds; }
            set { _interTestIntervalMilliseconds = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ThreadPoolAverageDelayTest()
            : base(false)
        {
            GeneralHelper.GenericDelegate<long> delegateInstance = new GeneralHelper.GenericDelegate<long>(Execute);
            _singleFastInvokeDelegate = _threadPool.GetMessageHandler(delegateInstance.Method);

            _threadPool.MinimumThreadsCount = 2;
            _threadPool.MaximumThreadsCount = 2;
        }

        public override void Update(FormTesting form)
        {
            
            form.ReportSecondary(_lastDelaySpotted.ToString() + " is " + GeneralHelper.TicksToMilliseconds(_lastDelaySpotted).ToString());
        }

        void Execute(long state)
        {
            Interlocked.Exchange(ref _lastDelaySpotted, ApplicationLifetimeHelper.ApplicationStopwatchTicks - state);
        }

        public override bool OnRun(FormTesting form, int count)
        {
            for (_executed = 0; _executed < count; _executed++)
            {
                Thread.Sleep(_interTestIntervalMilliseconds);
                long tick = ApplicationLifetimeHelper.ApplicationStopwatchTicks;

                _threadPool.QueueFastDelegate(this, _singleFastInvokeDelegate, tick);
            }

            return true;
        }
    }
}
