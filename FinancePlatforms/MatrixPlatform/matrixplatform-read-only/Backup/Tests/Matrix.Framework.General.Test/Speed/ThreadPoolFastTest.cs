// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Matrix.Common.Core;
using Matrix.Common.Extended;
using Matrix.Common.Extended.ThreadPools;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.Speed
{
    /// <summary>
    /// 
    /// </summary>
    public class ThreadPoolFastTest : SpeedTest
    {
        ThreadPoolFastEx _pool = new ThreadPoolFastEx("tp test");

        int _maximumTotalThreadsAllowed = 5;

        public int MaximumTotalThreadsAllowed
        {
            get { return _maximumTotalThreadsAllowed; }
            set { _maximumTotalThreadsAllowed = value; }
        }

        int _minimumThreadsCount = 1;

        public int MinimumThreadsCount
        {
            get { return _minimumThreadsCount; }
            set { _minimumThreadsCount = value; }
        }

        FastInvokeHelper.FastInvokeHandlerDelegate _singleFastInvokeDelegate;
        FastInvokeHelper.FastInvokeHandlerDelegate _singleFastInvokeDelegateX;
        WaitCallback simpleDelegate;

        public ThreadPoolFastTest() : base(true)
        {
            GeneralHelper.GenericDelegate<object> delegateInstance = new GeneralHelper.GenericDelegate<object>(Execute);
            _singleFastInvokeDelegate = _pool.GetMessageHandler(delegateInstance.Method);
            simpleDelegate = new WaitCallback(Execute);
            _singleFastInvokeDelegateX = FastInvokeHelper.GetMethodInvoker(simpleDelegate.Method, true, true);
        }

        void Execute(ThreadPoolFastEx pool, object param)
        {
            Execute(param);
        }

        void Execute(object param)
        {
            Interlocked.Increment(ref _executed);

            //lock (param)
            {// This lock somehow improves the speed, since it obviously syncs the threads better, so test can go up to 3.5 mil with it
                // and only 2.5 mil without it.

            }

            if (_executed == Count)
            {
                SignalTestComplete();
                //_sw.Stop();

                //double perSecond = (((double)1000 / _sw.ElapsedMilliseconds) * (double)count);
                //Report(_received.ToString() + ", " + _executed.ToString() + ", " + _sw.ElapsedMilliseconds.ToString() + " or " + perSecond.ToString("## ### ###.#") + " per second;");
                //Report(">>>");

                //Report2(string.Format("executed [{0}] / pend [{1}] / started [{2}] / awaken [{3}]", _executed, _pool._queue.Count, _pool.TotalThreadsStarted, _pool.TotalThreadsAwakens));
                //Report2(">>>");
            }
        }

        public override void Update(FormTesting form)
        {
            
        }

        public override bool OnRun(FormTesting form, int count)
        {
            _executed = 0;

            _pool.TotalThreadsAwakens = 0;

            //ThreadPoolFastEx.CallbackDelegateSingle single = new ThreadPoolFastEx.CallbackDelegateSingle(Execute);
            //MethodInfo _singleMethodInfo = single.Method;

            object[] predefinedObjects = new object[] { _pool };

            for (int i = 0; i < count && ApplicationLifetimeHelper.ApplicationClosing == false; i++)
            {
                // Stage Direct, 28 mil
                // Execute(_pool);

                // Stage 0.1, 22.2 mil
                // _singleSimpleFastInvokeDelegate(this, _pool);

                // Stage 0.2, 21.7 mil
                //_singleFastInvokeDelegateX(this, predefinedObjects);

                // Stage 0.3, 16 mil
                // _singleFastInvokeDelegate(this, new object[] { _pool, _pool });

                // Stage 1, 12 mil (now 17.2)
                //TargetInfo targetInfo = new TargetInfo(string.Empty, this, _singleSimpleFastInvokeDelegate, i);
                //targetInfo.Invoke();

                // Stage 2, 8 mil only target info
                //ThreadPoolEx.TargetInfo targetInfo = 
                // new ThreadPoolEx.TargetInfo(string.Empty, this, _singleFastInvokeDelegate, _pool, i);

                // Stage 2.1, 8.2 m
                //targetInfo.Invoke();

                // Stage 2.2, 
                //_pool.QeueueTargetInfo(targetInfo);

                // Stage 2.2, 0.8 - 3.5 mil, depending on conditions and thread config
                _pool.QueueFastDelegate(this, _singleFastInvokeDelegate, _pool);

                // Stage 3.
                //_pool.Queue(new GeneralHelper.GenericDelegate<object>(Execute), _pool);

                // System, 1.2 to 1.8 mil
                // ThreadPool.QueueUserWorkItem(Execute, _pool);
            }

            return true;
        }
    }
}
