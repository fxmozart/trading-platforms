// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Reflection;
using System.Diagnostics;
using Matrix.Framework.TestFramework;

namespace Matrix.Framework.General.Test.Speed
{
    /// <summary>
    /// Result 37K in current configuration.
    /// </summary>
    public class GetCallingMethodSpeedTest : SpeedTest
    {
        int stackPop = 2;

        /// <summary>
        /// 
        /// </summary>
        public GetCallingMethodSpeedTest()
            : base(false)
        {
        }

        public MethodBase Get()
        {
            //MethodInfo a;
            //a.gethan

            StackTrace _callStack = new StackTrace(); // The call stack  
            if (stackPop >= _callStack.FrameCount)
            {
                return null;
            }

            StackFrame frame = _callStack.GetFrame(stackPop); // The frame that called me.
            return frame.GetMethod(); // The baseMethod that called me.
        }

        public override bool OnRun(FormTesting form, int count)
        {
            for (_executed = 0; _executed < count; _executed++)
            {
                MethodBase method = Get();
            }

            return true;
        }

        public override void Update(FormTesting form)
        {
            
        }
    }
}
