// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Net;
using System.Threading;
using Matrix.Common.Core.Serialization;
using Matrix.Common.Diagnostics;
using Matrix.Common.Diagnostics.TracerCore.Items;
using Matrix.Common.Diagnostics.TracerCore.Sinks;
using Matrix.Common.Sockets.Common;
using Matrix.Common.Sockets.Core;
using NUnit.Framework;

namespace Matrix.Common.UnitTest.SocketTransport
{
    /// <summary>
    /// Test the operation of the socket library.
    /// </summary>
    [TestFixture]
    public class SocketStarTest
    {
        SocketMessageClient _messageClient = null;
        SocketMessageServer _messageServer = null;

        int _receivedItems = 0;

        int _messagesTargetCount = 0;

        ManualResetEvent _finishedEvent = new ManualResetEvent(false);

        /// <summary>
        /// Constructor.
        /// </summary>
        public SocketStarTest()
        {
            _messageServer = new SocketMessageServer(new BinarySerializer());
            _messageServer.ClientConnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(_messageServer_ClientConnectedEvent);
            _messageServer.ClientDisconnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(_messageServer_ClientDisconnectedEvent);
            _messageServer.Monitor.MinimumTracePriority = TracerItem.PriorityEnum.High;

            _messageClient = new SocketMessageClient(new BinarySerializer());
            _messageClient.Monitor.MinimumTracePriority = TracerItem.PriorityEnum.High;
        }

        void _messageServer_ClientDisconnectedEvent(SocketMessageServer server, SocketCommunicator client)
        {
            Console.WriteLine("Client [" + client.Id + "] disconnected.");
        }

        void _messageServer_ClientConnectedEvent(SocketMessageServer server, SocketCommunicator client)
        {
            Console.WriteLine("Client [" + client.Id + "] connected.");
            client.MessageReceivedEvent += new SocketCommunicator.MessageUpdateDelegate(client_MessageReceivedEvent);
        }

        void client_MessageReceivedEvent(SocketCommunicator helper, object message)
        {
            //if (((TestMessage)message).Int32Value % 1000 == 0)
            //{
            //    //Console.WriteLine(((TestMessage)message).Int32Value.ToString());
            //}

            if (Interlocked.Increment(ref _receivedItems) == _messagesTargetCount)
            {
                _finishedEvent.Set();
            }
        }

        FileTracerItemSink _sink;
        [TestFixtureSetUp]
        public void Init()
        {
            _sink = new FileTracerItemSink(SystemMonitor.Tracer, @"c:\temp.log");
            SystemMonitor.Tracer.Add(_sink);

            _messageServer.Start(new IPEndPoint(IPAddress.Loopback, 6312));
            _messageClient.ConnectAsync(new IPEndPoint(IPAddress.Loopback, 6312));
            
            Thread.Sleep(50);
        }

        [TestFixtureTearDown]
        public void UnInit()
        {
            _messageClient.Dispose();
            _messageServer.Dispose();

            SystemMonitor.Tracer.Remove(_sink);
            _sink.Dispose();
        }

        [Test]
        public void SendMessages([Values(5, 10000, 35000, 50000, 100000)] int messagesTargetCount)
        {
            _messagesTargetCount = messagesTargetCount;
            _receivedItems = 0;

            for (int i = 1; i <= _messagesTargetCount; i++)
            {
                //_messageClient.SendAsync(i);
                _messageClient.SendAsync(new TestMessage() { Int32Value = i, StringValue = "Test" }, null);
            }

            _finishedEvent.WaitOne();

        }

        //void MessageClient_ClientDisconnected(object sender, SocketEventArgs e)
        //{
        //    Console.WriteLine("Client disconnected: {0}", e.SocketError);
        //}

        //void MessageClient_ClientConnected(object sender, SocketEventArgs e)
        //{
        //    Console.WriteLine("Client connected: {0}", e.SocketError);

        //    ////for (int i = 0; i < 5; i++)
        //    //{
        //    //    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object target)
        //    //    {
        //    //        Send();
        //    //    }));
        //    //}
        //}

        //void MessageServer_MessageReceived(object sender, ClientMessageEventArgs e)
        //{
        //    var message = e.Message as TestMessage;
        //    _messageServer.SendMessageAsync(e.ClientId, message);

        //    if (message != null && message.Int32Value == _messagesTargetCount)
        //    {
        //        _finishedEvent.Set();

        //        Console.WriteLine("Message received: {0}, {1}", e.ClientId, e.Message);
        //    }
        //}

        //void MessageServer_ClientDisconnected(object sender, ClientStatusChangedEventArgs e)
        //{
        //    Console.WriteLine("Client disconnected: {0}", e.ClientId);
        //}

        //void MessageServer_ClientConnected(object sender, ClientStatusChangedEventArgs e)
        //{
        //    Console.WriteLine("Client connected: {0}", e.ClientId);
        //}

    }
}
