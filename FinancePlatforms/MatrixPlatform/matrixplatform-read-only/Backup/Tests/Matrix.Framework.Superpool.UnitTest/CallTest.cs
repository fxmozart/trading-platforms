// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using System.Threading;
using Matrix.Common.Core.Serialization;

namespace Matrix.Framework.SuperPool.UnitTest
{


    /// <summary>
    /// Test fixture class, tests various types of super pool calls.
    /// Tests are conducted with 2 local clients on a super pool.
    /// </summary>
    [TestFixture]
    public class CallTest
    {
        /// <summary>
        /// The tests are executed on a bunch of implementors, configured
        /// differently to make sure all tests run on various configurations.
        /// </summary>
        List<CallTestImplementor> _implementors = new List<CallTestImplementor>();

        CallTestImplementor _referenceImplementor;
        CallTestImplementor _binaryImplementor;

        /// <summary>
        /// Constructor.
        /// </summary>
        public CallTest()
        {
            // Load the implementors.
            _referenceImplementor = CreateDefaultImplementor();
            _implementors.Add(_referenceImplementor);
            
            _binaryImplementor = CreateBinaryLocalImplementor();
            _implementors.Add(_binaryImplementor);
            //_implementors.Add(CreateJSonLocalImplementor());
        }

        /// <summary>
        /// Implementor works on local referencing model.
        /// </summary>
        CallTestImplementor CreateDefaultImplementor()
        {
            Matrix.Framework.SuperPool.Core.SuperPool pool = new Matrix.Framework.SuperPool.Core.SuperPool("DefaultImplementor.Pool");
            
            CallTestImplementor implementor = new CallTestImplementor();
            pool.AddClient(implementor.Client1);
            pool.AddClient(implementor.Client2);
            implementor.Disposables.Add(pool);

            return implementor;
        }

        /// <summary>
        /// Implementor works on local referencing model.
        /// </summary>
        CallTestImplementor CreateBinaryLocalImplementor()
        {
            Matrix.Framework.MessageBus.Core.MessageBus bus = new Matrix.Framework.MessageBus.Core.MessageBus("BinaryLocalImplementor.Pool", new BinarySerializer());
            Matrix.Framework.SuperPool.Core.SuperPool pool = new Matrix.Framework.SuperPool.Core.SuperPool(bus);
            
            CallTestImplementor implementor = new CallTestImplementor();
            pool.AddClient(implementor.Client1);
            pool.AddClient(implementor.Client2);
            implementor.Disposables.Add(pool);

            implementor.Client1.EnvelopeDuplicationMode = Matrix.Framework.MessageBus.Core.Envelope.DuplicationModeEnum.DuplicateBoth;
            implementor.Client1.EnvelopeMultiReceiverDuplicationMode = Matrix.Framework.MessageBus.Core.Envelope.DuplicationModeEnum.DuplicateBoth;

            implementor.Client2.EnvelopeDuplicationMode = Matrix.Framework.MessageBus.Core.Envelope.DuplicationModeEnum.DuplicateBoth;
            implementor.Client2.EnvelopeMultiReceiverDuplicationMode = Matrix.Framework.MessageBus.Core.Envelope.DuplicationModeEnum.DuplicateBoth;

            return implementor;
        }

        ///// <summary>
        ///// Implementor works on local referencing model.
        ///// </summary>
        //CallTestImplementor CreateJSonLocalImplementor()
        //{
        //    MessageBus.MessageBus bus = new MessageBus.MessageBus("JSonLocalImplementor.Pool", new JSonSerializer());
        //    MessageSuperPool pool = new MessageSuperPool(bus);

        //    CallTestImplementor implementor = new CallTestImplementor();
        //    pool.AddClient(implementor.Client1);
        //    pool.AddClient(implementor.Client2);
        //    implementor.Disposables.Add(pool);

        //    implementor.Client1.DefaultEnvelopeDuplicationMode = MessageBus.Envelope.DuplicationModeEnum.DuplicateBoth;
        //    implementor.Client2.DefaultEnvelopeDuplicationMode = MessageBus.Envelope.DuplicationModeEnum.DuplicateBoth;

        //    return implementor;
        //}

        [TestFixtureSetUp]
        public void Init()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.Initialize();
            }
        }

        [TestFixtureTearDown]
        public void UnInit()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.Uninit();
            }
        }

        [Test]
        public void SimpleCallTestReference([Values(10000, 100000)] int length)
        {
            _referenceImplementor.SimpleCallTest(length);
        }

        [Test]
        public void SimpleCallTestBinarySerialization([Values(10000, 100000)] int length)
        {
            _binaryImplementor.SimpleCallTest(length);
        }

        [Test]
        public void VariableCallTest([Values(100)] int length)
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.VariableCallTest(length);
            }
        }

        [Test]
        public void RefCallTest()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.RefCallTest();
            }
        }

        [Test]
        public void OutCallTest()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.OutCallTest();
            }
        }

        [Test]
        public void AsyncResultCallTest()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.AsyncResultCallTest();
            }
        }

        [Test]
        public void LongTest_AsyncTimeoutResultCallTest()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.AsyncTimeoutResultCallTest();
            }
        }

        [Test]
        public void AsyncTimeoutResultCallTestException()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.AsyncTimeoutResultCallTestException();
            }
        }

        [Test]
        public void CallConfirmedTest()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.ConfirmedCallTest();
            }
        }

        /// <summary>
        /// This test is only runed on default implementor.
        /// </summary>
        [Test]
        public void DirectCall([Values(10000, 100000, 1000000)] int length)
        {
            _referenceImplementor.DirectCallTest(length);
        }

        [Test]
        public void CallFirst()
        {
            foreach (CallTestImplementor implementor in _implementors)
            {
                implementor.CallFirst();
            }
        }

    }
}

