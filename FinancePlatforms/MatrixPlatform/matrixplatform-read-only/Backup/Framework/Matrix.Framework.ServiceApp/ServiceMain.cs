// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Reflection;
using System.ServiceProcess;

namespace Matrix.Framework.ServiceApp
{
    /// <summary>
    /// Main service class.
    /// </summary>
    public partial class ServiceMain : ServiceBase
    {
        object _syncRoot = new object();
        //volatile NitroPlatform _platform = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public ServiceMain()
        {
            InitializeComponent();
            //ServiceName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        public void ConsoleStart(string[] args)
        {
            OnStart(args);
        }
        
        public void ConsoleStop()
        {
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            //PlatformSettings settings = AssemblySettings.LoadOrDefault<PlatformSettings>(Assembly.GetExecutingAssembly());
            //if (_platform == null)
            //{// Not 100% thread safe, but no big errors could occur from it.
            //    SystemMonitor.AssignTracer(new NetworkTracer(settings.TracerServerPort));
            //}

            //lock (_syncRoot)
            //{
            //    if (_platform != null)
            //    {
            //        return;
            //    }
                
            //    _platform = new NitroPlatform();
            //}

            //if (_platform.Initialize(settings).IsFailure)
            //{
            //    base.Stop();
            //}
        }

        protected override void OnStop()
        {
            //NitroPlatform platform;
            
            //lock (_syncRoot)
            //{
            //    platform = _platform;
            //    _platform = null;
            //    if (platform == null)
            //    {
            //        return;
            //    }
            //}

            //platform.UnInitialize();
            //platform.Settings.Save(Assembly.GetExecutingAssembly());

            //platform.Dispose();
        }
    }
}
