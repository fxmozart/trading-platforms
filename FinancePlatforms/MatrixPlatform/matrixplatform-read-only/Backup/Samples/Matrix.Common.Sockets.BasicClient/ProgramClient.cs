// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System.Windows.Forms;
using Matrix.Common.Diagnostics;
using Matrix.Common.Diagnostics.TracerCore;

namespace Matrix.Common.Sockets.BasicClient
{
    public class ProgramClient
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Main()
        {
            SystemMonitor.AssignTracer(new Tracer());
            Application.Run(new FormClient());
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public static void MainConsole()
        //{
        //    Console.WriteLine("Press a key to connect to server.");

        //    Console.ReadKey();

        //    MessageClient messageClient = new MessageClient();
        //    messageClient.ConnectedEvent += new AsyncCommunicator.HelperUpdateDelegate(Helper_ConnectedEvent);
        //    messageClient.DisconnectedEvent += new AsyncCommunicator.HelperUpdateDelegate(Helper_DisconnectedEvent);

        //    messageClient.ConnectAsync(new IPEndPoint(IPAddress.Loopback, 6312));

        //    Console.WriteLine("Connecting to server...");
        //    Console.ReadKey();

        //    Console.WriteLine("Sending data...");

        //    // Send data.
        //    for (int i = 0; i < 10; i++)
        //    {
        //        messageClient.SendAsync(new byte[1024 * 1024]);
        //    }

        //    Console.WriteLine("Press key to disconnect...");
        //    Console.ReadKey();

        //    messageClient.DisconnectAsync();
        //    //Console.ReadKey();
        //}

        //static void Helper_ConnectedEvent(AsyncCommunicator helper)
        //{
        //    Console.WriteLine("Client connected.");
        //}

        //static void Helper_DisconnectedEvent(AsyncCommunicator helper)
        //{
        //    Console.WriteLine("Client disconnected.");
        //}

    }
}
