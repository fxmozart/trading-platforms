// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using Matrix.Framework.SuperPool.Core;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.MessageBus.Core;

namespace Matrix.Framework.SuperPool.Demonstration
{
    [SuperPoolInterface]
    public interface ISomeInterface
    {
        void ReceiveSomeInfo(string info);
        void DoSomeWork();
    }

    public class MyComponent : ISomeInterface
    {
        public SuperPoolClient Client { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MyComponent()
        {
            Client = new SuperPoolClient("MyClient", this);
        }

        /// <summary>
        /// Send a request to "other" to do some work.
        /// </summary>
        public void RequestSomeWork(ClientId otherId)
        {
            Client.Call<ISomeInterface>(otherId).DoSomeWork();
        }

        #region ISomeInterface

        public void ReceiveSomeInfo(string info)
        {
            // ... someone send us some info.
        }

        public void DoSomeWork()
        {
            // ... doing work.
        }

        #endregion
    }

    public class MainClass
    {
        public void ShowMe()
        {
            // Create the pool.
            Matrix.Framework.SuperPool.Core.SuperPool pool = new Matrix.Framework.SuperPool.Core.SuperPool("MyPool");

            // Create component 1 and 2.
            MyComponent component1 = new MyComponent();
            MyComponent component2 = new MyComponent();

            // Add them both to the pool.
            pool.AddClient(component1.Client);
            pool.AddClient(component2.Client);

            // Request some work:
            component1.RequestSomeWork(component2.Client.Id);

            // Or we can also do it directly like this (although the previous is often a more suitable aproach)
            component1.Client.Call<ISomeInterface>(component2.Client.Id).DoSomeWork();
        }
    }
}
