// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Matrix.Common.Extended;
using Matrix.Framework.MessageBus.Clients.ExecutionStrategies;
using Matrix.Framework.MessageBus.Core;
using Matrix.Framework.SuperPool.Clients;
using Matrix.Framework.SuperPool.Core;

namespace Matrix.Framework.SuperPool.Demonstration
{
    /// <summary>
    /// This class contains code that demonstrates how to assign a custom execution strategy.
    /// </summary>
    public class CustomExecutionStrategyDemonstration
    {
        public class CustomExecutionStrategy : ExecutionStrategy
        {
            protected override void OnExecute(Envelope envelope)
            {
                // Process the incoming request, we will simply execute in on default thread pool.
                WaitCallback del = delegate(object state)
                {
                    base.Client.PerformExecution((Envelope)envelope);
                };

                ThreadPool.QueueUserWorkItem(del, envelope);
            }
        }

        /// <summary>
        /// Creates a new client, assigns it with a custom execution strategy and adds it to the super pool.
        /// </summary>
        /// <param name="superPool"></param>
        public void Demonstrate(Matrix.Framework.SuperPool.Core.SuperPool superPool)
        {
            SuperPoolClient client = new SuperPoolClient("Client", this);
            client.SetupExecutionStrategy(new CustomExecutionStrategy());

            superPool.AddClient(client);
        }
    }
}
