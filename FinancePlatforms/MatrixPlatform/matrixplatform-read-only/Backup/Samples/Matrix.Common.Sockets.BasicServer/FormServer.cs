// -----------------------------------------------------------------------------
// This source file is part of Matrix Platform
// 	(Universal .NET Software Development Platform)
// For the latest info, see http://www.matrixplatform.com
// 
// Copyright (c) 2009-2010, Ingenious Ltd
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------------------------------------------
using System;
using System.Windows.Forms;
using System.Net;
using Matrix.Common.Diagnostics;
using Matrix.Common.Sockets.Core;
using Matrix.Common.Core.Serialization;
using Matrix.Common.Sockets.Common;

namespace Matrix.Common.Sockets.BasicServer
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormServer : Form
    {
        SocketMessageServer _server;

        IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, SocketMessageServer.DefaultPort);

        /// <summary>
        /// 
        /// </summary>
        public FormServer()
        {
            InitializeComponent();

            _server = new SocketMessageServer(new BinarySerializer());
            _server.ClientConnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(messageServer_ClientConnectedEvent);
            _server.ClientDisconnectedEvent += new SocketMessageServer.ServerClientUpdateDelegate(messageServer_ClientDisconnectedEvent);

            //_server = new SocketMessageServer(base.Serializer);
            //_server.Start(new IPEndPoint(IPAddress.Any, port.Value));

            _server.ClientMessageReceivedEvent += new SocketMessageServer.MessageUpdateDelegate(_server_ClientMessageReceivedEvent);
            _server.ClientAsyncMessageSendEvent += new SocketMessageServer.AsyncMessageSendUpdateDelegate(_server_ClientAsyncMessageSendEvent);
        }

        void _server_ClientAsyncMessageSendEvent(SocketMessageServer server, SocketCommunicatorEx client, SocketCommunicator.AsyncMessageSendInfo info)
        {
            
        }

        void _server_ClientMessageReceivedEvent(SocketMessageServer server, SocketCommunicatorEx client, object message)
        {
            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SystemMonitor.AssignTracer(new Matrix.Common.Diagnostics.TracerCore.Tracer());
            this.tracerControl1.Tracer = SystemMonitor.Tracer;

            _server.Start(endPoint);
        }

        void messageServer_ClientConnectedEvent(SocketMessageServer server, SocketCommunicator client)
        {
            SystemMonitor.Info("+ Client connected [" + client.Id.ToString() + "]  event raised.");
            client.MessageReceivedEvent += new SocketCommunicator.MessageUpdateDelegate(client_MessageReceivedEvent);
        }

        void messageServer_ClientDisconnectedEvent(SocketMessageServer server, SocketCommunicator client)
        {
            SystemMonitor.Info("- Client disconnected [" + client.Id.ToString() + "] event raised.");
        }

        void client_MessageReceivedEvent(SocketCommunicator client, object message)
        {
            if (message is byte[])
            {
                SystemMonitor.Info(">> Client [" + client.Id.ToString() + "] received [" + message.ToString() + ", " + ((byte[])(message)).Length +" bytes]");
            }
            else
            {
                SystemMonitor.Info(">> Client [" + client.Id.ToString() + "] received [" + message.ToString() + "]");
            }
        }

        private void toolStripButtonStart_Click(object sender, EventArgs e)
        {
            _server.Start(endPoint);
        }

        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            _server.Stop(null);
        }

    }
}
